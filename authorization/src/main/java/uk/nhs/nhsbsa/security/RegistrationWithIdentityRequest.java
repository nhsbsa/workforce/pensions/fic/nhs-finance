package uk.nhs.nhsbsa.security;

import com.nhsbsa.security.RegistrationRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by Mark Lishman on 16/1/2017.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationWithIdentityRequest {

    @Valid
    @NotNull
    private RegistrationRequest registrationRequest;

    @Valid
    @NotNull
    private IdentityRequest identityRequest;
}
