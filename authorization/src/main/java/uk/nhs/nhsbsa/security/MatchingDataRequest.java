package uk.nhs.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by peterwhitehead on 29/03/2017.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MatchingDataRequest {
    @NotNull
    private String uuid;
}
