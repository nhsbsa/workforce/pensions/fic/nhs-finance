package uk.nhs.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdentityRequest {

    @NotNull
    private long sdNumber;

    @NotNull
    private String nino;

    @NotNull
    private LocalDate dateOfBirth;

}
