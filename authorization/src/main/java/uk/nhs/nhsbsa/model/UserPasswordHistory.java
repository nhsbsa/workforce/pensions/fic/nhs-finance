package uk.nhs.nhsbsa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by nathulse on 30/10/2017.
 * User Password History for authorization
 */

@Builder
@Data
@Entity
@Table(name = "user_password_history")
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
public class UserPasswordHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "uph_id", insertable = false, updatable = false)
    private Long id;

    private Long userId;
    private String name;
    private String hash;

}
