package uk.nhs.nhsbsa.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import uk.nhs.nhsbsa.model.ResetEmailToken;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by nataliehulse on 11/12/2017.
 */

@Repository
public interface ResetEmailTokenRepository extends CrudRepository<ResetEmailToken, Long> {

    ResetEmailToken findByToken(String token);

    Set<ResetEmailToken> findByUserId(Long userId);

    @Transactional
    int deleteByCreatedDateBefore(LocalDateTime expiryDate);
}
