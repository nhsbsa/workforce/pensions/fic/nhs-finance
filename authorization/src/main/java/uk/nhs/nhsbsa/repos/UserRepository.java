package uk.nhs.nhsbsa.repos;

import com.nhsbsa.model.User;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jeffreya on 16/08/2016.
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByNameAndApplicationsName(String userName, String appName);
    User findByUuid(String uuid);
    User findById(Long userId);
    List<User> findByRole(String role);
}
