package uk.nhs.nhsbsa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(HTTPSConfig.class)
public class AppConfig {
}
