package uk.nhs.nhsbsa.controllers;

import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.model.User;
import com.nhsbsa.security.RegistrationRequest;
import uk.nhs.nhsbsa.service.UserRegistrationService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.nhs.nhsbsa.rest.RestAuthentication;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by Mark Lishman on 4/1/2017.
 */

@Slf4j
@RequestMapping
@RestController
public class RegisterController {


    private static final String REGISTER_ENDPOINT = "/register";
    public static final String OAUTH2_TOKEN = "OAUTH2-TOKEN";

    private final UserRegistrationService userRegistrationService;

    private static final String APPLICATION_ID_HEADER_ERROR_MESSAGE = "ERROR - No application id in header";
    private static final String REGISTER_USER_ERROR_MESSAGE = "ERROR - Failed to register user";
    @Autowired
    public RegisterController(final UserRegistrationService userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
    }

    @PostMapping("/register")
    @ApiOperation(value = "default", notes = "default")
	  public ResponseEntity<AuthenticationResponse> register(@RequestBody @Valid final RegistrationRequest registrationRequest,
                                                           final BindingResult bindingResult,
                                                           final HttpServletRequest httpServletRequest) {
        try {
            log.info("ENTER - {}", REGISTER_ENDPOINT);

            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final String applicationId = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_ID);
            if (applicationId == null) {
                log.error(APPLICATION_ID_HEADER_ERROR_MESSAGE);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final User validUser = userRegistrationService.register(applicationId, registrationRequest);
            if (validUser == null) {
                log.error(REGISTER_USER_ERROR_MESSAGE);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final AuthenticationResponse authenticationResponse = getAuthenticationResponse(validUser);
            return ResponseEntity.ok(authenticationResponse);
        } finally {
            log.info("EXIT - {}", REGISTER_ENDPOINT);
        }
    }

    private AuthenticationResponse getAuthenticationResponse(User validUser) {
        return AuthenticationResponse
                        .builder()
                        .uuid(validUser.getUuid())
                        .token(OAUTH2_TOKEN)
                        .build();
    }
}