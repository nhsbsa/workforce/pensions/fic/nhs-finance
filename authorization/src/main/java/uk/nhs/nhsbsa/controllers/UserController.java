package uk.nhs.nhsbsa.controllers;

import com.nhsbsa.model.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uk.nhs.nhsbsa.rest.RestAuthentication;
import uk.nhs.nhsbsa.service.UserAuthenticationService;
import uk.nhs.nhsbsa.service.UserService;

@Slf4j
@RestController
public class UserController {

  private final UserService userService;
  private final UserAuthenticationService userAuthenticationService;
  private final Integer waitForErrorMillis;

  @Autowired
  public UserController(final UserService userService,
            final UserAuthenticationService userAuthenticationService,
            final @Value("${waitForErrorMillis}") Integer waitForErrorMillis) {
    this.userService = userService;
    this.userAuthenticationService = userAuthenticationService;
    this.waitForErrorMillis = waitForErrorMillis;
  }

  @GetMapping(value = "/user/{uuid}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ApiOperation(value = "default", notes = "default")
  public HttpEntity<User> user(
      @ApiParam(name = "uuid", value = "User's identifier") @PathVariable String uuid
  ) throws InterruptedException {

      log.info("Retrieving user by uuid = {}", uuid);
      User userByUuid = userService.getUserByUuid(uuid);

      if (userByUuid != null) {
          return ResponseEntity.ok(userByUuid);
      } else {
          Thread.sleep(waitForErrorMillis);
          return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
      }
  }

  @GetMapping(value = "/user/email/{email}/")
  @ApiOperation(value = "default", notes = "default")
  public HttpEntity<User> getUserByEmail(
      @ApiParam(name = "email", value = "User's mail identifier") @PathVariable String email,
      final HttpServletRequest httpServletRequest
  ) throws InterruptedException {

    log.info("Retrieving user by email = {}", email);
    final String applicationId = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_ID);
    if (applicationId == null) {
      log.error("ERROR - No application id in header");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    User userByEmail = userAuthenticationService.retrieveUserByValidEmail(applicationId, email);
    return ResponseEntity.ok(userByEmail);

  }
  
  @GetMapping(value = {"/user","/user/"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ApiOperation(value = "Query for users.", notes = "Request the users with the supplied parameters.")
  public HttpEntity<List<User>> getUsersByOptionalParam(
	        @ApiParam(value = "Role of users.")
	        @RequestParam(name = "role", required = true) String role)  {

      log.info("Retrieving users by role = {}", role);
      List<User> users = userService.getUsersByRole(role);

      if (users!=null) {
          return ResponseEntity.ok(users);
      } 
          
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
  }

  @PostMapping("/user/delete-user/{uuid}")
  @ApiOperation(value = "default", notes = "default")
  public HttpEntity<User> deleteUserByUuid(
      @ApiParam(value = "Delete user.")
      @PathVariable String uuid) {
    try {
      log.info("Start Delete user uuid = {]", uuid);

      return ResponseEntity.ok(userService.deleteUserByUuid(uuid));

    } finally {
      log.info("User Deleted = {]", uuid);
    }
  }
}