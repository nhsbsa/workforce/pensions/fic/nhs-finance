package uk.nhs.nhsbsa.controllers;


import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import com.nhsbsa.security.LoginRequest;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.nhs.nhsbsa.rest.RestAuthentication;
import uk.nhs.nhsbsa.service.UserAuthenticationService;

/**
 * Created by jeffreya on 16/08/2016.
 * RestAuthentication
 */

@Slf4j
@RequestMapping
@RestController
public class AuthenticationController {

    private final UserAuthenticationService userAuthenticationService;

    @Autowired
    public AuthenticationController(final UserAuthenticationService userAuthenticationService) {
        this.userAuthenticationService = userAuthenticationService;
    }

    @PostMapping("/authenticate")
    @ApiOperation(value = "default", notes = "default")
    public HttpEntity<AuthenticationResponse> authenticate(@RequestBody @Valid final LoginRequest loginRequest,
                                                           final BindingResult bindingResult,
                                                           final HttpServletRequest httpServletRequest) {
        try {
            log.info("ENTER - {}", "/authenticate");

            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final String applicationId = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_ID);
            if (applicationId == null) {
                log.error("ERROR - No application id in header");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final AuthenticationResponse response = userAuthenticationService.authenticateUser(applicationId, loginRequest.getUsername(), loginRequest.getPassword());

            if (!AuthenticationStatus.AUTH_SUCCESS.equals(response.getAuthenticationStatus())){
                log.error("ERROR - Invalid or locked user "+loginRequest.getUsername());
                return ResponseEntity.ok(response);
            }

            log.info("User authenticated: " + response.getUuid());
            return ResponseEntity.ok(response);
        } finally {
            log.info("EXIT - {}", "/authenticate");
        }
    }
}