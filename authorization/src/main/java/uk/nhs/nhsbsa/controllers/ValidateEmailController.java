package uk.nhs.nhsbsa.controllers;


import com.nhsbsa.model.ForgottenPassword;
import com.nhsbsa.model.User;
import com.nhsbsa.security.ResetEmailTokenResponse;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.nhs.nhsbsa.model.ResetEmailToken;
import uk.nhs.nhsbsa.rest.RestAuthentication;
import uk.nhs.nhsbsa.security.ValidateEmailResponse;
import uk.nhs.nhsbsa.service.UserAuthenticationService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by nataliehulse on 27/11/2017.
 * RestAuthentication
 */

@Slf4j
@RequestMapping
@RestController
public class ValidateEmailController {

    private final UserAuthenticationService userAuthenticationService;

    private static String logEnter = "ENTER - {}";
    private static String logExit = "EXIT - {}";

    @Autowired
    public ValidateEmailController(final UserAuthenticationService userAuthenticationService) {
        this.userAuthenticationService = userAuthenticationService;
    }

    @PostMapping("/authenticate/validate-email")
    @ApiOperation(value = "default", notes = "default")
    public HttpEntity<ValidateEmailResponse> authenticate(@RequestBody @Valid final ForgottenPassword forgottenPassword,
                                                          final BindingResult bindingResult,
                                                          final HttpServletRequest httpServletRequest) {
        try {
            log.info(logEnter, "/validate-email");

            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final String applicationId = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_ID);
            if (applicationId == null) {
                log.error("ERROR - No application id in header");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final User validUser = userAuthenticationService.retrieveUserByValidEmail(applicationId, forgottenPassword.getEmail());
            if (validUser == null || validUser.userIsDeleted()) {
                log.error("ERROR - Not a valid email");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            // If CreateEmailToken is passed in as True. Using this flag incase other services want to call this ValidateEmailController without resetting password.
            // Create an Email token
            String emailToken;
            if (forgottenPassword.isCreateEmailToken()) {

                final ResetEmailToken resetEmailToken = userAuthenticationService.generateEmailToken(validUser.getId());
                emailToken = resetEmailToken.getToken();
                log.info("Email Token Created: " + resetEmailToken.getToken());

            } else {
                emailToken = "";
                log.info("Email Token Not Created: ");
            }

            final ValidateEmailResponse validateEmailResponse =
                    ValidateEmailResponse
                            .builder()
                            .uuid(validUser.getUuid())
                            .userName(validUser.getName())
                            .token(emailToken)
                            .build();

            log.info("Email authenticated: " + validUser.getUuid());
            return ResponseEntity.ok(validateEmailResponse);
        } finally {
            log.info(logExit, "/validate-email");
        }
    }

    @PostMapping("/authenticate/verify-token")
    @ApiOperation(value = "default", notes = "default")
    public HttpEntity<ResetEmailTokenResponse> authenticateEmailToken(@RequestBody @Valid final String emailTokenRequest,
                                                                      final BindingResult bindingResult,
                                                                      final HttpServletRequest httpServletRequest) {
        try {
            log.info(logEnter, "/verify-token");

            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final String applicationId = httpServletRequest.getHeader(RestAuthentication.AUTH_APP_ID);
            if (applicationId == null) {
                log.error("ERROR - No application id in header");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            final String userUuid = userAuthenticationService.validateEmailToken(emailTokenRequest);
            if (userUuid == null) {
                log.error("ERROR - Not a valid token");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }

            ResetEmailTokenResponse emailTokenResponse = new ResetEmailTokenResponse();
            emailTokenResponse.setUserUuid(userUuid);

            log.info("Email Token verified: " + userUuid);
            return ResponseEntity.ok(emailTokenResponse);
        } finally {
            log.info(logExit, "/verify-token");
        }
    }

    @PostMapping("/authenticate/delete-token")
    @ApiOperation(value = "default", notes = "default")
    public ResponseEntity<Boolean> deleteEmailToken(@RequestBody @Valid final String emailTokenRequest) {
        try {
            log.info(logEnter, "/delete-token");

            return ResponseEntity.ok(userAuthenticationService.deleteEmailToken(emailTokenRequest));

        } finally {
            log.info(logExit, "/delete-token");
        }
    }
}