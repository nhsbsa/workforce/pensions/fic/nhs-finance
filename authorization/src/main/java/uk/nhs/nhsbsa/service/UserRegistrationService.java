package uk.nhs.nhsbsa.service;

import static java.util.UUID.randomUUID;

import com.nhsbsa.model.AuthorizedApplication;
import com.nhsbsa.model.User;
import com.nhsbsa.security.RegistrationRequest;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.nhs.nhsbsa.repos.ApplicationRepository;
import uk.nhs.nhsbsa.repos.UserRepository;

/**
 * Created by Mark Lishman on 26/09/2016.
 */

@Log4j
@Service
public class UserRegistrationService {

    private final PasswordService passwordService;
    private final ApplicationRepository applicationRepository;
    private final UserRepository userRepository;

    @Autowired
    public UserRegistrationService(
            final PasswordService passwordService,
            final UserRepository userRepository,
            final ApplicationRepository applicationRepository) {
        this.passwordService = passwordService;
        this.userRepository = userRepository;
        this.applicationRepository = applicationRepository;
    }

    public User register(final String applicationName, final RegistrationRequest registrationRequest) {
        final AuthorizedApplication app = applicationRepository.findByName(applicationName);
        if (app == null) {
            return null;
        }

        final String hash = passwordService.generatePasswordHash(registrationRequest.getPassword());
        final User user = User
            .builder()
            .name(registrationRequest.getUsername())
            .hash(hash)
            .uuid(randomUUID().toString())
            .firstLogin(true)
            .role(registrationRequest.getRole())
            .build();
        user.addApplication(app);

        return userRepository.save(user);
    }
}
