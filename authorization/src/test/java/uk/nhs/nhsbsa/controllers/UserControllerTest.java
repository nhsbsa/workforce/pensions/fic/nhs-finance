package uk.nhs.nhsbsa.controllers;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.model.User;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uk.nhs.nhsbsa.rest.RestAuthentication;
import uk.nhs.nhsbsa.service.UserAuthenticationService;
import uk.nhs.nhsbsa.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserControllerTest {

  private static final Long USER_ID = 1L;
  private static final String USER_UUID = "Valid-UUID";
  private static final String USER_HASH = "hash";
  private static final String USER_EMAIL = "email@email.com";
  private static final String USER_ROLE = "ROLE_STANDARD";
  private static final Boolean FIRST_LOGIN = true;
  private static final LocalDate LOCAL_DATE = LocalDate.of(2018,10,1);
  private static Date DELETED_ON = Date.from(LOCAL_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant());

  private static final String USER_URL = "/user/{uuid}";
  private static final String USER_ROLE_URL = "/user";

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private UserService userService;

  @MockBean
  private UserAuthenticationService userAuthenticationService;

  private UserController controller;

  private MockMvc mvc;

  @Before
  public void init(){

    controller = Mockito
        .spy(new UserController(userService, userAuthenticationService, 20));

    mvc = standaloneSetup(controller).build();
  }

  @Test
  public void retrieve_user()  throws Exception {


    String expectedJsonResponseBody = "{\"id\":1"
        + ",\"name\":\"email@email.com\""
        + ",\"hash\":\"hash\""
        + ",\"uuid\":\"Valid-UUID\""
        + ",\"firstLogin\":true"
        + ",\"role\":\"ROLE_STANDARD\""
        + ",\"applications\":null"
        + "}";

    User expectedUser = User.builder()
        .name(USER_EMAIL)
        .hash(USER_HASH)
        .uuid(USER_UUID)
        .firstLogin(FIRST_LOGIN)
        .role(USER_ROLE)
        .id(USER_ID)
        .build();

    when(userService.getUserByUuid(USER_UUID)).thenReturn(expectedUser);

    mvc
        .perform(MockMvcRequestBuilders.get(USER_URL, USER_UUID))
        .andExpect(content().json(expectedJsonResponseBody))
        .andExpect(status().isOk());
  }

  @Test
  public void retrieve_user_returns_not_found()  throws Exception {

    when(userService.getUserByUuid(USER_UUID)).thenReturn(null);

    mvc
        .perform(MockMvcRequestBuilders.get(USER_URL, USER_UUID))
        .andExpect(status().isNotFound());
  }

  @Test
  public void retrieve_user_by_email()  throws Exception {

    String applicationId = "fic";

    String expectedJsonResponseBody = "{\"id\":1,\"name\":\"email@email.com\",\"hash\":\"hash\",\"uuid\":\"Valid-UUID\",\"firstLogin\":true,\"role\":\"ROLE_STANDARD\",\"applications\":null}";

    User expectedUser = User.builder().name(USER_EMAIL).hash(USER_HASH).uuid(USER_UUID).firstLogin(FIRST_LOGIN).role(USER_ROLE).id(USER_ID).build();

    when(userAuthenticationService.retrieveUserByValidEmail(applicationId, USER_EMAIL)).thenReturn(expectedUser);

    mvc
        .perform(MockMvcRequestBuilders.get("/user/email/{email}/", USER_EMAIL).header(
            RestAuthentication.AUTH_APP_ID, applicationId))
        .andExpect(content().json(expectedJsonResponseBody))
        .andExpect(status().isOk());
  }

  @Test
  public void retrieve_user_by_email_without_applicationID_header_attribute()  throws Exception {

    mvc
        .perform(MockMvcRequestBuilders.get("/user/email/{email}/", USER_EMAIL ))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void retrieve_user_role()  throws Exception {


    String expectedJsonResponseBody = "[{\"id\":1"
        + ",\"name\":\"email@email.com\""
        + ",\"hash\":\"hash\""
        + ",\"uuid\":\"Valid-UUID\""
        + ",\"firstLogin\":true"
        + ",\"role\":\"ROLE_STANDARD\""
        + ",\"applications\":null"
        + "}]";

    List<User> expectedUser = Collections.singletonList(User.builder()
        .name(USER_EMAIL)
        .hash(USER_HASH)
        .uuid(USER_UUID)
        .firstLogin(FIRST_LOGIN)
        .role(USER_ROLE)
        .id(USER_ID)
        .build());

    when(userService.getUsersByRole(USER_ROLE)).thenReturn(expectedUser);

    mvc
        .perform(MockMvcRequestBuilders.get(USER_ROLE_URL).param("role", USER_ROLE))
        .andExpect(content().json(expectedJsonResponseBody))
        .andExpect(status().isOk());
  }

  @Test
  public void retrieve_user_role_returns_not_found()  throws Exception {

    when(userService.getUsersByRole(USER_ROLE)).thenReturn(null);

    mvc
        .perform(MockMvcRequestBuilders.get(USER_ROLE_URL).param("role", USER_ROLE))
        .andExpect(status().isNotFound());
  }

  @Test
  public void delete_user_by_uuid()  throws Exception {

    User expectedUser = User.builder().name(USER_EMAIL).hash(USER_HASH).uuid(USER_UUID).role(USER_ROLE).id(USER_ID).deletedOn(DELETED_ON).build();

    when(userService.deleteUserByUuid(USER_UUID)).thenReturn(expectedUser);

    mvc
        .perform(MockMvcRequestBuilders.post("/user/delete-user/{uuid}/", USER_UUID))
        .andExpect(content().string(is(equalTo(validUserResponse()))))
        .andExpect(status().isOk());
  }

  private String validUserResponse() throws JsonProcessingException {
      return objectMapper.writeValueAsString(User.builder()
          .name(USER_EMAIL)
          .hash(USER_HASH)
          .uuid(USER_UUID)
          .role(USER_ROLE)
          .id(USER_ID)
          .deletedOn(DELETED_ON)
          .build());
  }

}
