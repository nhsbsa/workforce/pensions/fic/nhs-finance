package uk.nhs.nhsbsa.service;

import static org.assertj.core.util.DateUtil.now;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.model.User;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import uk.nhs.nhsbsa.repos.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private static final String HASH = "hash";
    private static final String UUID = "user-uuid";
    private static final String ROLE = "user-role";
    private static final Long USER_ID = 1L;
    private static final String NAME = "Testing@Test";

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Before
    public void before() {
        userService = new UserService(userRepository);
    }

    @Test
    public void getUserByUuidTest() {

        final User user = User.builder().id(USER_ID).uuid(UUID).hash(HASH).name(NAME).role(ROLE).firstLogin(false).locked(false).build();
        given(userRepository.findByUuid(UUID)).willReturn(user);

        User response = userService.getUserByUuid(UUID);

        assertThat(response.getId(), is(USER_ID));
        assertThat(response.getUuid(), is(UUID));
        assertThat(response.getHash(), is(HASH));
        assertThat(response.getName(), is(NAME));
        assertThat(response.getRole(), is(ROLE));
        assertThat(response.isFirstLogin(), is(false));
        assertThat(response.isLocked(), is(false));
    }

    @Test
    public void getUsersByRoleTest() {

        final User user = User.builder().id(USER_ID).uuid(UUID).hash(HASH).name(NAME).role(ROLE).firstLogin(false).locked(false).build();
        final List<User> userList = Collections.singletonList(user);
        given(userRepository.findByRole(ROLE)).willReturn(userList);

        List<User> response = userService.getUsersByRole(ROLE);

        assertThat(response.get(0).getId(), is(USER_ID));
        assertThat(response.get(0).getUuid(), is(UUID));
        assertThat(response.get(0).getHash(), is(HASH));
        assertThat(response.get(0).getName(), is(NAME));
        assertThat(response.get(0).getRole(), is(ROLE));
        assertThat(response.get(0).isFirstLogin(), is(false));
        assertThat(response.get(0).isLocked(), is(false));
    }

    @Test
    public void DeletedOnIsSaved() {

        final User user = User.builder().id(USER_ID).uuid(UUID).hash(HASH).name(NAME).build();
        given(userRepository.findByUuid(UUID)).willReturn(user);

        final User savedUser = User.builder().id(USER_ID).uuid(UUID).hash(HASH).name(NAME).deletedOn(now()).build();
        given(userRepository.save(user)).willReturn(savedUser);

        User response = userService.deleteUserByUuid(UUID);

        assertThat(response.getId(), is(USER_ID));
        assertThat(response.getUuid(), is(UUID));
        assertThat(response.getHash(), is(HASH));
        assertNotNull(response.getDeletedOn());
        assertTrue(response.userIsDeleted());
    }

}