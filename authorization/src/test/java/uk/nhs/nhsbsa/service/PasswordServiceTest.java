package uk.nhs.nhsbsa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCrypt;

@RunWith(MockitoJUnitRunner.class)
public class PasswordServiceTest {

  private PasswordService passwordService = new PasswordService();

  private final static String FIRST_PASSWORD = "password1";
  private final static String SECOND_PASSWORD = "password2";
  private final static String salt = BCrypt.gensalt(12);
  private final static String FIRST_HASHED_PASSWORD = BCrypt.hashpw("password1", salt);
  private final static String SECOND_HASHED_PASSWORD = BCrypt.hashpw("password2", salt);


  @Test
  public void isValidPasswordReturnsTrue() {

    boolean result = passwordService.isValidPassword(FIRST_PASSWORD, FIRST_HASHED_PASSWORD);

    assertTrue(result);
  }

  @Test
  public void isValidPasswordReturnsFalse() {

    boolean result = passwordService.isValidPassword(FIRST_PASSWORD, SECOND_HASHED_PASSWORD);

    assertFalse(result);
  }

  @Test
  public void notAValidPasswordReturnsTrue() {

    boolean result = passwordService.notAValidPassword(FIRST_PASSWORD, SECOND_HASHED_PASSWORD);

    assertTrue(result);
  }

  @Test
  public void notAValidPasswordReturnsFalse() {

    boolean result = passwordService.notAValidPassword(SECOND_PASSWORD, SECOND_HASHED_PASSWORD);

    assertFalse(result);
  }

}
