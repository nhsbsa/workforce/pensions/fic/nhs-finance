package com.nhsbsa.controllers;

import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.service.OrganisationService;
import com.nhsbsa.view.OrganisationView;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class OrganisationController {

  private final OrganisationService organisationService;

  @Autowired
  public OrganisationController(final OrganisationService organisationService) {
    this.organisationService = organisationService;
  }

  @GetMapping(value = "/organisation/{eaCode}")
  public ResponseEntity<OrganisationView> getOrganisationByEaCode(
      @PathVariable("eaCode") final String eaCode) {

    log.info("GET organisation with EA code = {}", eaCode);
    OrganisationView org = organisationService.getOrganisationByEaCode(eaCode);

    return ResponseEntity.ok(org);
  }

  @GetMapping(value = "/employer-type/{eaCode}")
  public ResponseEntity<String> getEmployerTypeByEaCode(
      @PathVariable("eaCode") final String eaCode) {

    log.info("GET - searching for employerType with eaCode = {}", eaCode);
    Optional<String> empType  = organisationService.getEmployerTypeByEaCode(eaCode);

    return empType.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());

  }

  @RequestMapping(value = "/organisation", method = RequestMethod.POST)
  public ResponseEntity<OrganisationView> registerOrganisation(
      @RequestBody @Valid final CreateOrganisationRequest createOrganisationRequest) {

    log.info("POST for registering organisation with EA code = {}", createOrganisationRequest.getEas().get(0).getEaCode());
    final OrganisationView organisation = organisationService.registerOrganisation(createOrganisationRequest);

    return ResponseEntity.ok(organisation);
  }

  @RequestMapping(value = "/organisation-no-ea", method = RequestMethod.POST)
  public ResponseEntity<CreateOrganisationResponse> registerOrganisationWithoutEa(
      @RequestBody @Valid final CreateOrganisationRequest createOrganisationRequest) {

    log.info("POST for registering organisation without EA");
    final Optional<CreateOrganisationResponse> organisation = organisationService.registerOrganisationWithoutEa(createOrganisationRequest);

    return organisation.map(u -> ResponseEntity.ok(u)).orElse(ResponseEntity.noContent().build());
  }

  @RequestMapping(value = "/employing-authority", method = RequestMethod.POST)
  public ResponseEntity<OrganisationView> registerEa(
      @RequestBody @Valid final CreateEaRequest createEaRequest) {

    log.info("POST for registering organisation without EA");
    final Optional<OrganisationView> orgEa = organisationService.registerEa(createEaRequest);

    return orgEa.map(u -> ResponseEntity.ok(u)).orElse(ResponseEntity.noContent().build());
  }

  @RequestMapping(value = "/organisation-name", method = RequestMethod.POST)
  public ResponseEntity<String> getOrganisationByName(
      @RequestBody @Valid final CreateOrganisationRequest organisationRequest) {

    log.info("POST for getting Organisation by name");
    final Optional<String> org = organisationService.getOrganisationByName(organisationRequest);

    return org.map(u -> ResponseEntity.ok(u)).orElse(ResponseEntity.noContent().build());
  }

  @GetMapping(value = "/employing-authority/{eaCode}")
  public ResponseEntity<String> getEaByEaCode(
      @PathVariable("eaCode") final String eaCode) {

    log.info("GET - searching for ea with eaCode = {}", eaCode);
    Optional<String> ea  = organisationService.getEaByCode(eaCode);

    return ea.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());

  }

}
