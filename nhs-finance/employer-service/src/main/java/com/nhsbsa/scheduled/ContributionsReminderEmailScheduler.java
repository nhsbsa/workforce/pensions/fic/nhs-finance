package com.nhsbsa.scheduled;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import com.nhsbsa.model.User;
import com.nhsbsa.service.UserService;
import com.nhsbsa.service.email.EmailConfig;
import com.nhsbsa.service.email.EmailService;
import com.nhsbsa.utility.ExampleDateGenerator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@ConditionalOnProperty("activateEmailReminderScheduler")
public class ContributionsReminderEmailScheduler {

  private final EmailService emailService;
  private final UserService userService;
  private EmailConfig reminderEmailConfig;
  private final ExampleDateGenerator exampleDateGenerator;

  @Value("${bsa.appBaseAddress}")
  private String appBaseAddress;
  @Value("${bsa.contributions.reminder.email.subject}")
  String reminderEmailSubject;
  @Value("${bsa.contributions.reminder.email.link}")
  String reminderFicBaseUrl;

  @Autowired
  public ContributionsReminderEmailScheduler(
      final EmailService emailService,
      final UserService userService,
      final ExampleDateGenerator exampleDateGenerator, 
      final EmailConfig reminderEmailConfig) {
    this.emailService = emailService;
    this.userService = userService;
    this.exampleDateGenerator = exampleDateGenerator;
    this.reminderEmailConfig = reminderEmailConfig;
  }

  @Scheduled(cron = "${contributionsReminderEmail.cron}")
  public void run() {
    this.sendRemindersToEmployerAdmins();
  }

  public void sendRemindersToEmployerAdmins() {
    log.info("Starting sending reminder emails to employer Admins");
    List<User> admins = userService.getAllEmployerAdminsUsers();

    if (admins.isEmpty()) {
      log.info("No employer admins found.");
    }

    for (User admin : admins) {

        Map<String, String> personalisation = new HashMap<>();
        personalisation.put("contributionMonth", exampleDateGenerator.generateFormattedDeadlineMonth());
        personalisation.put("lastPaymentDate", exampleDateGenerator.generateFormattedEmailReminderDeadlineDate());

        LocalTime deadlineHour = LocalTime.of(exampleDateGenerator.getDeadlineHour(), 0);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ha");

        personalisation.put("deadlineHour", deadlineHour.format(dtf));
        personalisation.put("ficBaseUrl", reminderFicBaseUrl);
        personalisation.put("appBaseAddress", appBaseAddress);

        this.emailService.sendEmail(
            Arrays.asList(admin.getName()),
            reminderEmailSubject, 
            reminderEmailConfig, 
            personalisation);
      }
    }
}