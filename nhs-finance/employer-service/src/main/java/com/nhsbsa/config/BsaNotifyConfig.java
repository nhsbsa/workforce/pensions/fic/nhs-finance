package com.nhsbsa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import uk.nhs.nhsbsa.notify.client.NotificationConfig;

@Configuration
@Import(NotificationConfig.class)
@ComponentScan(basePackages = "uk/nhs/nhsbsa/notify/client")
public class BsaNotifyConfig {
	@Bean
	public ClientHttpRequestFactory clientHttpRequestFactory() {
		return new HttpComponentsClientHttpRequestFactory();
	}
}
