package com.nhsbsa.service.email.impl;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import com.nhsbsa.service.email.EmailConfig;
import com.nhsbsa.service.email.EmailContentService;
import com.nhsbsa.service.email.EmailService;

import lombok.extern.slf4j.Slf4j;
import uk.nhs.nhsbsa.notify.api.v1.model.Notification;
import uk.nhs.nhsbsa.notify.api.v1.service.INotificationService;

@Slf4j
@Profile("email-provider-NHSMail")
@Service(value = "bsaNotifyV1Service") //V1 uses NHS Mail
public class BSANotifyV1Service implements EmailService{

  private INotificationService notificationClient;

  private EmailContentService emailContentService;

  @Autowired
  public BSANotifyV1Service(INotificationService notificationClient,
      EmailContentService emailContentService) {
    this.notificationClient = notificationClient;
    this.emailContentService = emailContentService;
  }

  private Notification createNotification(List<String> toEmails, String fromEmail,
      String emailSubject, String htmlMessage) {
    Notification notification = new Notification();
    notification.setId(RandomStringUtils.randomAlphabetic(15));
    notification.setTo(toEmails);
    notification.setFrom(fromEmail);
    notification.setSubject(emailSubject);
    notification.setHtmlMessage(htmlMessage);

    return notification;
  }

  @Override
  public void sendEmail(List<String> toEmails, String emailSubject, EmailConfig emailConfig, Map<String, String> personalisation) {

    String htmlMessage = emailContentService.createEmailContent(
        emailConfig.getTemplateName(),
        personalisation);

    Notification notification = createNotification(
        toEmails, 
        emailConfig.getFromEmail(),
        emailSubject, 
        htmlMessage);

    log.info("ID [{}]: Sending '{}' email to BSA Notify for: {}", notification.getId(), emailSubject, toEmails);

    notificationClient.send(notification);
  }

}
