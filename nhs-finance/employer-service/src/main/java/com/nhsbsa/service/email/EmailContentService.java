package com.nhsbsa.service.email;

import java.util.Map;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service(value = "emailContentService")
public class EmailContentService {

  private TemplateEngine templateEngine;

  @Autowired
  public EmailContentService(TemplateEngine templateEngine) {
    this.templateEngine = templateEngine;
  }

  public String createEmailContent(String templateName, Map<String, String> personalisation) {
    Context context = new Context();

    for (Entry<String, String> entry : personalisation.entrySet()) {
      context.setVariable(entry.getKey(), entry.getValue());
    }

    return templateEngine.process(templateName, context);
  }

}
