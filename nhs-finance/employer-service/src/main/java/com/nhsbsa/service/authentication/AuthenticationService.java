package com.nhsbsa.service.authentication;


import com.nhsbsa.security.LoginRequest;

public interface AuthenticationService<T> {

    T authenticateUser(final LoginRequest loginRequest);

}
