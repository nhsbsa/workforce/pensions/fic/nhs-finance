package com.nhsbsa.service.email;

import com.nhsbsa.model.StandardEmployerUser;
import com.nhsbsa.security.EmailRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service(value = "userAdminEmailService")
public class UserAdminEmailService {

    private EmailService emailService;

    private EmailConfig resetEmailConfig;

    private EmailConfig standardUserCreatedConfig;
        
    @Value("${bsa.appBaseAddress}")
    private String appBaseAddress;

    @Value("${bsa.resetpassword.email.subject}") 
    private String resetEmailSubject;
    @Value("${bsa.resetpassword.email.setPasswordUrl}") 
    private String setPasswordUrl;

    @Value("${bsa.createStandardUser.email.subject}")
    private String standardEmailSubject;
    @Value("${bsa.createStandardUser.email.linkUrl}")
    private String standardLinkUrl;
   
    @Autowired
    public UserAdminEmailService(final EmailConfig resetEmailConfig,
                                final EmailConfig standardUserCreatedConfig,
                                final EmailService emailService) {
        this.resetEmailConfig = resetEmailConfig;
        this.standardUserCreatedConfig = standardUserCreatedConfig;
        this.emailService = emailService;
    }

  public void sendPasswordResetEmail(String email, String token) {

    Map<String, String> personalisation = new HashMap<>();
    personalisation.put("setPasswordUrl", setPasswordUrl + token);
    personalisation.put("appBaseAddress", appBaseAddress);

    emailService.sendEmail(Arrays.asList(email), resetEmailSubject, resetEmailConfig, personalisation);
  }

  public void sendStandardUserCreatedEmail(EmailRequest emailRequest) {

    Map<String, String> personalisation = new HashMap<>();
    personalisation.put("standardLinkUrl", standardLinkUrl);
    personalisation.put("appBaseAddress", appBaseAddress);
    personalisation.put("userName", emailRequest.getEmail());
    personalisation.put("createdBy", emailRequest.getCreatedBy());

    emailService.sendEmail(Arrays.asList(emailRequest.getEmail()), standardEmailSubject, standardUserCreatedConfig, personalisation);
  }
}
