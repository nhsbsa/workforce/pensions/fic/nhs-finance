package com.nhsbsa.service.email;

import java.util.List;
import java.util.Map;

public interface EmailService {
    
    void sendEmail(List<String> toEmails, String emailSubject, EmailConfig emailConfig, Map<String, String> personalisation);

}