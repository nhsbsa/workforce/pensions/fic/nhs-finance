package com.nhsbsa.service.email.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import com.nhsbsa.service.email.EmailConfig;
import com.nhsbsa.service.email.EmailContentService;
import uk.nhs.nhsbsa.notify.api.v1.model.Notification;
import uk.nhs.nhsbsa.notify.api.v1.service.INotificationService;

@RunWith(SpringRunner.class)
public class BSANotifyV1ServiceTest {

	@Mock
	private INotificationService notificationClient;

	@Mock
	private EmailContentService emailContentService;
	
	private BSANotifyV1Service service;
	
	private List<String> toEmails = Arrays.asList("toAddress1@","toAddress2@");
	private String fromEmail = "fromEmail@";
	private String emailSubject = "Unit Test Email";
	private String emailTemplate = "test.template";
	private String dummyTemplate = "<HTML></HTML>";

	private EmailConfig emailConfig = new EmailConfig(fromEmail,emailTemplate);
	
	private Map<String, String> personalisation = new HashMap<>();
	
	@Before 
	public void setUp(){
	  service = new BSANotifyV1Service(notificationClient, emailContentService);
	}
	
	@Test
	public void whenGivenValidEmailDetailsNotificationClientSendIsCalledWithANotification() {
	    
		service.sendEmail(toEmails, emailSubject, emailConfig, personalisation);
		
	    verify(emailContentService, times(1)).createEmailContent(emailTemplate,personalisation);
		verify(notificationClient, times(1)).send(any(Notification.class));
	}

	@Test
	public void whenGivenValidEmailDetailsNotificationClientSendIsCalledWithANotificationPopulatedWithDetails() {
		
		ArgumentCaptor<Notification> argument = ArgumentCaptor.forClass(Notification.class);
		
		doReturn(dummyTemplate).when(emailContentService).createEmailContent(emailTemplate,personalisation);
		
		service.sendEmail(toEmails, emailSubject, emailConfig, personalisation);

		verify(notificationClient).send(argument.capture());
		
		assertEquals(toEmails, argument.getValue().getTo());
		assertEquals(fromEmail, argument.getValue().getFrom());
		assertEquals(emailSubject, argument.getValue().getSubject());
		assertEquals(dummyTemplate, argument.getValue().getHtmlMessage());
	}
	
	@Test
	public void whenGivenValidEmailDetailsNotificationClientSendIsCalledWithANotificationWithARandomId() {
		
		ArgumentCaptor<Notification> argument = ArgumentCaptor.forClass(Notification.class);
        service.sendEmail(toEmails, emailSubject, emailConfig, personalisation);

		verify(notificationClient).send(argument.capture());
		assertNotNull(argument.getValue().getId());

	}
}
