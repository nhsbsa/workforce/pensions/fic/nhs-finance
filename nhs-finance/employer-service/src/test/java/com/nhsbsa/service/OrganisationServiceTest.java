package com.nhsbsa.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.service.authentication.impl.OrganisationDto;
import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class OrganisationServiceTest {

  private final String EXPECTED_EA_CODE = "123456";
  private final String ORG_ID = "org-id";
  private final String ORG_NAME = "org-name";
  private final UUID ORG_UUID = UUID.randomUUID();

  @Mock
  private RestTemplate restTemplate;

  private OrganisationService service;

  @Before
  public void before() {
    service = new OrganisationService(restTemplate, "http://employer-details-service:1234/employer-context");
  }
  @Test
  public void check_that_getOrganisationByEaCode_method_does_not_modify_URI_request() {

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/organisation/ea-code/" + EXPECTED_EA_CODE);

    given(restTemplate.getForObject(URI_request, OrganisationView.class)).willReturn(null);

    service.getOrganisationByEaCode(EXPECTED_EA_CODE);

    verify(restTemplate, times(1)).getForObject(URI_request, OrganisationView.class);

  }

  @Test
  public void check_that_getOrganisationById_returns_response() {
    OrganisationDto organisationDto = OrganisationDto.builder()
        .accountName(ORG_NAME)
        .identifier(ORG_UUID)
        .build();

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/organisation/" + ORG_ID);

    given(restTemplate.getForObject(URI_request, OrganisationDto.class)).willReturn(organisationDto);

    OrganisationDto response = service.getOrganisationByID(ORG_ID);

    assertEquals(response.getAccountName(), ORG_NAME);
    assertEquals(response.getIdentifier(), ORG_UUID);
    verify(restTemplate, times(1)).getForObject(URI_request, OrganisationDto.class);

  }

  @Test
  public void check_that_registerOrganisation_returns_response() {
    CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
        .name(ORG_NAME)
        .eas(Collections.emptyList())
        .build();

    OrganisationView organisationView = OrganisationView.builder()
        .name(ORG_NAME)
        .eas(Collections.emptyList())
        .identifier(ORG_UUID)
        .build();

    final String orgUri = "http://employer-details-service:1234/employer-context/organisation";

    given(restTemplate.postForObject(orgUri, orgRequest, OrganisationView.class)).willReturn(organisationView);

    OrganisationView response = service.registerOrganisation(orgRequest);

    assertEquals(response.getName(), ORG_NAME);
    assertEquals(response.getIdentifier(), ORG_UUID);
    assertThat(response.getEas().size(), is(0) );
    verify(restTemplate, times(1)).postForObject(orgUri, orgRequest, OrganisationView.class);

  }

  @Test
  public void check_that_getEmployerTypeByEaCode_method_returns_a_value() {

    String empType = EmployerTypes.STAFF.name();

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/employer-type/" + EXPECTED_EA_CODE);

    given(restTemplate.getForObject(URI_request, String.class)).willReturn(empType);

    Optional<String> result = service.getEmployerTypeByEaCode(EXPECTED_EA_CODE);

    assertThat(result)
        .isEqualTo(Optional.of(empType));

    verify(restTemplate, times(1)).getForObject(URI_request, String.class);

  }

  @Test
  public void check_that_invalid_ea_for_getEmployerTypeByEaCode_method_returns_null() {

    String empType = EmployerTypes.STAFF.name();

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/employer-type/" + EXPECTED_EA_CODE);

    given(restTemplate.getForObject(URI_request, String.class)).willReturn(null);

    Optional<String> result = service.getEmployerTypeByEaCode(EXPECTED_EA_CODE);

    assertFalse(result.isPresent());

    verify(restTemplate, times(1)).getForObject(URI_request, String.class);

  }

  @Test
  public void check_that_registerOrganisationWithoutEa_method_returns_a_value() {

    String orgName = "New Organisation";
    String orgUuid = "org-uuid";

    CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .eas(Collections.emptyList())
        .build();

    CreateOrganisationResponse orgResponse = CreateOrganisationResponse.builder()
        .name(orgName)
        .uuid(orgUuid)
        .build();

    String uri = "http://employer-details-service:1234/employer-context/organisation-no-ea";

    given(restTemplate.postForObject(uri, orgRequest, CreateOrganisationResponse.class)).willReturn(orgResponse);

    Optional<CreateOrganisationResponse> result = service.registerOrganisationWithoutEa(orgRequest);

    assertTrue(result.isPresent());
    assertEquals(result.get().getName(), orgName);
    assertEquals(result.get().getUuid(), orgUuid);

    verify(restTemplate, times(1)).postForObject(uri, orgRequest, CreateOrganisationResponse.class);

  }

  @Test
  public void check_that_registerOrganisationWithoutEa_method_returns_empty() {

    String orgName = "New Organisation";
    String orgUuid = "org-uuid";

    CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .eas(Collections.emptyList())
        .build();

    String uri = "http://employer-details-service:1234/employer-context/organisation-no-ea";

    given(restTemplate.postForObject(uri, orgRequest, CreateOrganisationResponse.class)).willReturn(null);

    Optional<CreateOrganisationResponse> result = service.registerOrganisationWithoutEa(orgRequest);

    assertFalse(result.isPresent());

    verify(restTemplate, times(1)).postForObject(uri, orgRequest, CreateOrganisationResponse.class);

  }

  @Test
  public void check_that_registerEa_method_returns_a_value() {

    String orgName = "Organisation Name";
    UUID orgUuid = UUID.randomUUID();
    String eaName = "New EA Name";
    String eaCode = "EA1234";
    String employerType = "STAFF";

    CreateEaRequest eaRequest = CreateEaRequest.builder()
        .organisationUuid(orgUuid.toString())
        .eaName(eaName)
        .eaCode(eaCode)
        .employerType(employerType)
        .build();

    EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .name(eaName)
        .eaCode(eaCode)
        .employerType(employerType)
        .build();

    OrganisationView orgResponse = OrganisationView.builder()
        .name(orgName)
        .identifier(orgUuid)
        .eas(Collections.singletonList(eaView))
        .build();

    String uri = "http://employer-details-service:1234/employer-context/employing-authority";

    given(restTemplate.postForObject(uri, eaRequest, OrganisationView.class)).willReturn(orgResponse);

    Optional<OrganisationView> result = service.registerEa(eaRequest);

    assertTrue(result.isPresent());
    assertEquals(result.get().getName(), orgName);
    assertEquals(result.get().getIdentifier(), orgUuid);
    assertEquals(result.get().getEas().get(0), eaView);

    verify(restTemplate, times(1)).postForObject(uri, eaRequest, OrganisationView.class);

  }

  @Test
  public void check_that_registerEa_method_returns_empty() {

    String orgName = "Organisation Name";
    UUID orgUuid = UUID.randomUUID();
    String eaName = "New EA Name";
    String eaCode = "EA1234";
    String employerType = "STAFF";

    CreateEaRequest eaRequest = CreateEaRequest.builder()
        .organisationUuid(orgUuid.toString())
        .eaName(eaName)
        .eaCode(eaCode)
        .employerType(employerType)
        .build();

    String uri = "http://employer-details-service:1234/employer-context/employing-authority";

    given(restTemplate.postForObject(uri, eaRequest, OrganisationView.class)).willReturn(null);

    Optional<OrganisationView> result = service.registerEa(eaRequest);

    assertFalse(result.isPresent());

    verify(restTemplate, times(1)).postForObject(uri, eaRequest, OrganisationView.class);

  }

  @Test
  public void check_that_getOrganisationByName_method_returns_a_value() {

    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name("Org Name")
        .build();

    String URI_request = "http://employer-details-service:1234/employer-context/organisation-name";

    given(restTemplate.postForObject(URI_request, organisationRequest, String.class)).willReturn("Org Name");

    Optional<String> result = service.getOrganisationByName(organisationRequest);

    assertThat(result)
        .isEqualTo(Optional.of(organisationRequest.getName()));

    verify(restTemplate, times(1)).postForObject(URI_request, organisationRequest, String.class);

  }

  @Test
  public void check_that_getOrganisationByName_method_returns_null() {

    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name("Org Name")
        .build();

    String URI_request = "http://employer-details-service:1234/employer-context/organisation-name";

    given(restTemplate.postForObject(URI_request, organisationRequest, String.class)).willReturn(null);

    Optional<String> result = service.getOrganisationByName(organisationRequest);

    assertFalse(result.isPresent());

    verify(restTemplate, times(1)).postForObject(URI_request, organisationRequest, String.class);

  }

  @Test
  public void check_that_getEaByCode_method_returns_a_value() {

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/employing-authority/" + EXPECTED_EA_CODE);

    given(restTemplate.getForObject(URI_request, String.class)).willReturn(EXPECTED_EA_CODE);

    Optional<String> result = service.getEaByCode(EXPECTED_EA_CODE);

    assertThat(result)
        .isEqualTo(Optional.of(EXPECTED_EA_CODE));

    verify(restTemplate, times(1)).getForObject(URI_request, String.class);

  }

  @Test
  public void check_that_getEaByCode_method_returns_null() {

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/employing-authority/" + EXPECTED_EA_CODE);

    given(restTemplate.getForObject(URI_request, String.class)).willReturn(null);

    Optional<String> result = service.getEaByCode(EXPECTED_EA_CODE);

    assertFalse(result.isPresent());

    verify(restTemplate, times(1)).getForObject(URI_request, String.class);

  }
}