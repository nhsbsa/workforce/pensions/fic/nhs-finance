package com.nhsbsa.service.authentication.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class FinanceUserAuthenticationServiceTest {

  private final EmployingAuthorityView EMPLOYING_AUTHORITY = EmployingAuthorityView.builder().eaCode("EA12345").name("").employerType("STAFF").build();
  private final List<EmployingAuthorityView> EMPLOYING_AUTHORITY_LIST = Collections.singletonList(EMPLOYING_AUTHORITY);
  private final OrganisationView ORGANISATION = OrganisationView.builder().identifier(UUID.randomUUID()).name("TEST ORG").eas(EMPLOYING_AUTHORITY_LIST).build();
  private final List<OrganisationView> ORGANISATION_LIST = Collections.singletonList(ORGANISATION);

  private FinanceUserAuthenticationService service;

  @Mock
  private RestTemplate restTemplate;

  @Before
  public void before() {
    service = new FinanceUserAuthenticationService(
        restTemplate,
        "http://employer-details-service-url:1234/finance-information-collection/employer-details"
    );
  }

  @Test
  public void given_a_valid_login_request_when_user_is_authenticated_then_finance_user_is_returned() {
    final UUID userUuid = UUID.randomUUID();
    final UUID orgUuid = UUID.randomUUID();

    final LoginRequest loginRequest = LoginRequest
        .builder()
        .username("user-name")
        .uuid(userUuid.toString())
        .build();

    final UserDto userDto = UserDto
        .builder()
        .identifier(userUuid)
        .firstName("first-name")
        .lastName("last-name")
        .emailAddress("contact-email")
        .contactNumber("123456789")
        .organisations(ORGANISATION_LIST)
        .build();
    final URI userUri = URI.create("http://employer-details-service-url:1234/finance-information-collection/employer-details/user/" + userUuid.toString());
    given(restTemplate.getForObject(userUri, UserDto.class)).willReturn(userDto);

    final OrganisationDto organisationView = OrganisationDto
        .builder()
        .eas(EMPLOYING_AUTHORITY_LIST)
        .build();
    final URI organisationUri = URI
        .create("http://employer-details-service-url:1234/finance-information-collection/employer-details/organisation/" + orgUuid.toString());
    given(restTemplate.getForObject(organisationUri, OrganisationDto.class))
        .willReturn(organisationView);

    final FinanceUser financeUser = service.authenticateUser(loginRequest);

    assertThat(financeUser.getUsername(), is(equalTo("user-name")));
    assertThat(financeUser.getFirstName(), is(equalTo("first-name")));
    assertThat(financeUser.getLastName(), is(equalTo("last-name")));
    assertThat(financeUser.getContactEmail(), is(equalTo("contact-email")));
    assertThat(financeUser.getContactTelephone(), is(equalTo("123456789")));
    assertThat(financeUser.getUuid(), is(equalTo(userUuid.toString())));
    assertThat(financeUser.getRole(), is(equalTo(UserRoles.ROLE_STANDARD.name())));
    assertThat(financeUser.getOrganisations().get(0).getName(), is(equalTo("TEST ORG")));
    assertThat(financeUser.getOrganisations().get(0).getEas().get(0).getEaCode(), is(equalTo("EA12345")));
  }


}