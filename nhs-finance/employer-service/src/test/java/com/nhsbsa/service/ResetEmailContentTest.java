package com.nhsbsa.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.EvaluateXPathMatcher.hasXPath;
import java.util.HashMap;
import java.util.Map;
import com.nhsbsa.service.email.EmailContentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;

import uk.nhs.nhsbsa.notify.api.v1.model.Notification;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ThymeleafAutoConfiguration.class})
public class ResetEmailContentTest {

    private final String WEB_APP_ADDRESS = "http://localhost:8080/employer-website";
    private final String RESET_PASS_LINK = "http://localhost:8080/employer-website/reset-password";
    
    private Map<String, String> personalisation = new HashMap<String, String>() {{
      put("setPasswordUrl", RESET_PASS_LINK);
      put("appBaseAddress", WEB_APP_ADDRESS);
    }};
    
    private final String TEMPLATE_NAME = "email-fic-reset-pw";
    
    private EmailContentService emailContentService;
    
    @Autowired
    private TemplateEngine templateEngine;
    
    @Before
    public void setUp() {
      emailContentService = new EmailContentService(templateEngine);
    }

    @Test
    public void urlIsPopulated() {

        String content = emailContentService.createEmailContent(TEMPLATE_NAME, personalisation);

        Notification notification = new Notification();
        notification.setHtmlMessage(content);

        String htmlMessage = notification.getHtmlMessage();

        assertThat(htmlMessage, hasXPath("//a[@id='reset-url']/@href", equalTo(RESET_PASS_LINK)));
    }

    @Test
    public void BSABannerIsPresent() {

        String content = emailContentService.createEmailContent(TEMPLATE_NAME, personalisation);

        Notification notification = new Notification();
        notification.setHtmlMessage(content);

        String htmlMessage = notification.getHtmlMessage();

        assertThat(htmlMessage, hasXPath("//img[@id='bsa-banner']/@src", equalTo("http://localhost:8080/employer-website/images/email-banner.png")));
    }


}