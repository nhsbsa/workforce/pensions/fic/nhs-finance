package com.nhsbsa.scheduled;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.service.UserService;
import com.nhsbsa.service.email.EmailConfig;
import com.nhsbsa.service.email.EmailService;
import com.nhsbsa.utility.ExampleDateGenerator;

@RunWith(SpringRunner.class)
public class ContributionsReminderEmailSchedulerTest {

    @Mock
    private EmailService emailService;
    
    @Mock
    private UserService userService;
    
    @Mock
    private ExampleDateGenerator exampleDateGenerator;
    
    private ContributionsReminderEmailScheduler scheduler;
  
    private final String FIC_REMINDER_LINK = "http://localhost:8080/employer-website";
    private final String APP_BASE_ADDRESS = "http://localhost:8080/employer-website";
    private final String FROM_EMAIL = "from@example.com";
    private final String EMAIL_SUBJECT = "Test email subject";
    private final String EMAIL_TEMPLATE = "test_template";
        
    private Map<String, String> personalisation = new HashMap<String, String>() {{
      put("ficBaseUrl", FIC_REMINDER_LINK);
      put("appBaseAddress", APP_BASE_ADDRESS);
    }};
    
    private EmailConfig emailConfig  = new EmailConfig(FROM_EMAIL, EMAIL_TEMPLATE);
    
    @Before
    public void setUp() throws Exception{
      scheduler = new ContributionsReminderEmailScheduler(emailService, userService, exampleDateGenerator, emailConfig);
      
      FieldUtils.writeDeclaredField(scheduler, "reminderEmailSubject", EMAIL_SUBJECT, true);
      FieldUtils.writeDeclaredField(scheduler, "appBaseAddress", APP_BASE_ADDRESS, true);
      FieldUtils.writeDeclaredField(scheduler, "reminderFicBaseUrl", FIC_REMINDER_LINK, true);
    }
    

    @Test
    public void should_call_send_email_when_there_are_admins() {
      
      User user1 = User.builder().name("admin1@email.com").build();
      User user2 = User.builder().name("admin2@email.com").build();
      User user3 = User.builder().name("admin3@email.com").build();
      
      when(userService.getAllEmployerAdminsUsers()).thenReturn(Arrays.asList(user1, user2, user3));

      String exampleDeadlineMonth = "July 2018";
      String exampleDealineDate = "15 August 2018";
      int exampleDeadlineHour = 13;

      when(exampleDateGenerator.generateFormattedDeadlineMonth()).thenReturn(exampleDeadlineMonth);
      when(exampleDateGenerator.generateFormattedEmailReminderDeadlineDate()).thenReturn(exampleDealineDate);
      when(exampleDateGenerator.getDeadlineHour()).thenReturn(exampleDeadlineHour);

      personalisation.put("contributionMonth", exampleDeadlineMonth);
      personalisation.put("lastPaymentDate", exampleDealineDate);
      personalisation.put("deadlineHour", "1PM");

      scheduler.sendRemindersToEmployerAdmins();
      
      verify(emailService, Mockito.times(1)).sendEmail(Arrays.asList(user1.getName()), EMAIL_SUBJECT, emailConfig, personalisation);
      verify(emailService, Mockito.times(1)).sendEmail(Arrays.asList(user2.getName()), EMAIL_SUBJECT, emailConfig, personalisation);
      verify(emailService, Mockito.times(1)).sendEmail(Arrays.asList(user3.getName()), EMAIL_SUBJECT, emailConfig, personalisation);
    }
    
    @Test
    public void should_not_call_send_email_when_no_admins() {
      when(userService.getAllEmployerAdminsUsers()).thenReturn(Collections.emptyList());

      scheduler.sendRemindersToEmployerAdmins();
      
      verify(emailService, Mockito.times(0)).sendEmail(anyListOf(String.class), any(String.class), any(EmailConfig.class), anyMap());
    }
    
}