package com.nhsbsa.controllers;

import com.nhsbsa.controllers.finance.SetPasswordController;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.SetPassword;
import com.nhsbsa.service.authentication.AuthorizationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.view.InternalResourceView;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by nataliehulse on 12/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SetPasswordControllerTest {

    public static final String SET_PASS_VIEW_NAME = "setpassword";

    private MockMvc mockMvc;

    @Mock
    private AuthorizationService authorizationService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        FinanceUser user = mock(FinanceUser.class);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);

        mockMvc = MockMvcBuilders
                .standaloneSetup(new SetPasswordController(authorizationService))
                .setSingleView(new InternalResourceView("/WEB-INF/templates/setpassword.html"))
                .build();
    }

    @Test
    public void setPasswordPageIsReturned() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/set-password"))
                .andExpect(handler().methodName("showSetPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(SET_PASS_VIEW_NAME))
                .andExpect(model().attributeExists("setPassword"));
    }

    @Test
    public void successfulSaveRedirectsUserToPasswordUpdatedPage() throws Exception {

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.post("/set-password")
                .param("newPassword", "Password12345")
                .param("passwordConfirm", "Password12345"))
                .andExpect(handler().methodName("postSetPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(model().attributeErrorCount("setPassword", 0))
                .andExpect(view().name("redirect:/passwordUpdated"));

        verify(authorizationService, times(1)).savePassword(Mockito.any(SetPassword.class));
    }

    @Test
    public void postDataWithErrors() throws Exception {

        final MockHttpSession session = new MockHttpSession();

        mockMvc.perform(MockMvcRequestBuilders.post("/set-password")
                .param("newPassword", "123456")
                .param("passwordConfirm", "123456")
                .session(session))
                .andExpect(handler().handlerType(SetPasswordController.class))
                .andExpect(handler().methodName("postSetPasswordPage"))
                .andExpect(status().isOk())
                .andExpect(view().name(is(equalTo("setpassword"))))
                .andExpect(model().attributeErrorCount("setPassword", 1))
                .andExpect(model().attributeHasFieldErrorCode("setPassword", "newPassword", "ValidatePassword"))
                .andExpect(model().attribute("setPassword", is(instanceOf(SetPassword.class))));
    }

}
