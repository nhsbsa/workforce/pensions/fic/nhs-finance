package com.nhsbsa.controllers.finance;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.EmailResponse;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.service.authentication.UserRegistrationService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CreateStandardEmployerControllerTest {

  @MockBean
  private UserRegistrationService userRegistrationService;

  @MockBean
  private AuthorizationService authorizationService;

  @MockBean
  private FinanceService financeService;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  private static EmployingAuthorityView EA = EmployingAuthorityView.builder().eaCode("EA1234").name("").employerType("").build();
  private static List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA);
  private static OrganisationView ORG = OrganisationView.builder().identifier(null).name("").eas(EA_LIST).build();
  private static List<OrganisationView> ORG_LIST = Collections.singletonList(ORG);

  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();

  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-standard-employer"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-standard-employer"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_master_role_then_requested_create_standard_employer_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-standard-employer"))
        .andExpect(handler().methodName("showCreateStandardEmployerPage"))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andReturn();
  }


  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_admin_role_with_finance_user_ok_then_requested_create_standard_employer_page_is_returned()
      throws Exception {

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();

    CreateStandardEmployerController controller = Mockito
        .spy(new CreateStandardEmployerController(userRegistrationService, authorizationService,financeService));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/createStandardEmployer.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.get("/create-standard-employer"))
        .andExpect(handler().methodName("showCreateStandardEmployerPage"))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_blank_first_name_then_page_is_returned_with_1_error()
      throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/create-standard-employer")
                .param("firstName", "")
                .param("lastName", "LastName")
                .param("email", "valid.email@email.com")
                .param("firstPassword", "ValidPassword123")
                .with(csrf()))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andExpect(model().errorCount(1))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_invalid_last_name_then_page_is_returned_with_1_error()
      throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/create-standard-employer")
                .param("firstName", "Name")
                .param("lastName", "Invalid1")
                .param("email", "valid.email@email.com")
                .param("firstPassword", "ValidPassword123")
                .with(csrf()))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andExpect(model().errorCount(1))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_invalid_email_then_page_is_returned_with_1_error()
      throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/create-standard-employer")
                .param("firstName", "first")
                .param("lastName", "last")
                .param("email", "invalidEmail")
                .param("firstPassword", "ValidPassword123")
                .with(csrf()))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andExpect(model().errorCount(1))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_invalid_password_then_page_is_returned_with_1_error()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-standard-employer")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("email", "valid@email.com")
            .param("firstPassword", "INVALID_PASSWORD")
            .with(csrf()))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andExpect(model().errorCount(1))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_invalid_fields_then_page_is_returned_with_4_errors()
      throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/create-standard-employer")
                .param("firstName", "Invalid1")
                .param("lastName", "Invalid2")
                .param("email", "INVALID_EMAIL")
                .param("firstPassword", "INVALID_PASSWORD").with(csrf()))
        .andExpect(model().attributeExists("standardEmployerUser"))
        .andExpect(model().errorCount(4))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_user_that_already_exists_then_page_is_returned_with_userExists_attribute()
      throws Exception {

    String validUUID = "VALID-UUID";
    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");
    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = User.builder().uuid(validUUID).build();
    EmailResponse emailResponse = EmailResponse.builder().name("Test's").build();

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(mockedUser));
    when(financeService.getFinanceUserByUuid(validUUID)).thenReturn(Optional.of(new FinanceUser()));
    when(financeService.sendStandardUserCreatedEmail(any())).thenReturn(Optional.of(emailResponse));

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();

    CreateStandardEmployerController controller = Mockito
        .spy(new CreateStandardEmployerController(userRegistrationService, authorizationService, financeService));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/createStandardEmployer.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-standard-employer")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("email", "valid@email.com")
            .param("firstPassword", "validPassword123")
            .with(csrf()))
        .andExpect(model().attributeExists("userExists"))
        .andReturn();
  }


  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_user_that_does_not_exist_but_email_not_sent_then_forwarded_page_is_returned()
      throws Exception {

    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");
    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid("VALID-UUID");

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(userRegistrationService.createAuthenticationUser(any())).thenReturn(Optional.of(mockedAuthenticationResponse));
    when(userRegistrationService.createEmployerUser(any()))
        .thenReturn((Optional.of(mockedResponse)));
    when(financeService.sendStandardUserCreatedEmail(any())).thenReturn(Optional.empty());

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();

    CreateStandardEmployerController controller = Mockito
        .spy(new CreateStandardEmployerController(userRegistrationService, authorizationService, financeService));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/createStandardEmployer.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-standard-employer")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("email", "valid@email.com")
            .param("firstPassword", "validPassword123")
            .with(csrf()))
        .andExpect((view().name("forward:/employer-created")))
        .andExpect(model().attribute("firstName", is("")))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_with_authentication_user_that_exists_but_finance_user_not_then_forwarded_page_is_returned()
      throws Exception {

    String validUUID = "VALID-UUID";
    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");
    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = User.builder().uuid(validUUID).build();
    EmailResponse emailResponse = EmailResponse.builder().name("Test's").build();

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(mockedUser));
    when(financeService.getFinanceUserByUuid(validUUID)).thenReturn(Optional.empty());
    when(userRegistrationService.createEmployerUser(any()))
        .thenReturn((Optional.of(mockedResponse)));
    when(financeService.sendStandardUserCreatedEmail(any())).thenReturn(Optional.of(emailResponse));

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();

    CreateStandardEmployerController controller = Mockito
        .spy(new CreateStandardEmployerController(userRegistrationService, authorizationService, financeService));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/createStandardEmployer.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-standard-employer")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("email", "valid@email.com")
            .param("firstPassword", "validPassword123")
            .with(csrf()))
        .andExpect((view().name("forward:/employer-created")))
        .andExpect(model().attribute("firstName", is(emailResponse.getName())))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_and_userRegistrationService__createEmployerUser_returns_null_then_500_response_is_sent()
      throws Exception {

    String validUUID = "VALID-UUID";
    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = User.builder().uuid(validUUID).build();

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(mockedUser));
    when(financeService.getFinanceUserByUuid(validUUID)).thenReturn(Optional.empty());
    when(userRegistrationService.createEmployerUser(any()))
        .thenReturn((Optional.empty()));

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();

    CreateStandardEmployerController controller = Mockito
        .spy(new CreateStandardEmployerController(userRegistrationService, authorizationService, financeService));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/createStandardEmployer.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-standard-employer")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("email", "valid@email.com")
            .param("firstPassword", "validPassword123")
            .with(csrf()))
        .andExpect((status().is5xxServerError()))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_post_and_userRegistrationService_createAuthenticationUser_returns_null_then_500_response_is_sent()
      throws Exception {

    String validUUID = "VALID-UUID";
    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());

    when(userRegistrationService.createAuthenticationUser(any())).thenReturn(Optional.empty());

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();

    CreateStandardEmployerController controller = Mockito
        .spy(new CreateStandardEmployerController(userRegistrationService, authorizationService, financeService));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/createStandardEmployer.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-standard-employer")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("email", "valid@email.com")
            .param("firstPassword", "validPassword123")
            .with(csrf()))
        .andExpect((status().is5xxServerError()))
        .andReturn();
  }
}
