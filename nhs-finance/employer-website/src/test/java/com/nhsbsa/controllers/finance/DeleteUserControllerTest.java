package com.nhsbsa.controllers.finance;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.authentication.AuthorizationService;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class DeleteUserControllerTest {

  @MockBean
  private AuthorizationService authorizationService;

  @MockBean
  private FinanceService financeService;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;


  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/confirm-delete-user/{uuid}", "user-uuid"))
        .andExpect(status().isForbidden())
      .andReturn();
}

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/confirm-delete-user/{uuid}", "user-uuid"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_admin_role_then_requested_delete_account_page_is_returned()
      throws Exception {

    FinanceUser user = FinanceUser.builder()
        .firstName("tom")
        .uuid("user-uuid")
        .build();
    when(financeService.getFinanceUserByUuid("user-uuid")).thenReturn(Optional.of(user));

    mockMvc.perform(MockMvcRequestBuilders.get("/confirm-delete-user/{uuid}", "user-uuid"))
        .andExpect(handler().methodName("showDeleteUserPage"))
        .andExpect(model().attributeExists("deleteUser"))
        .andExpect(model().attribute("userName", "Tom's"))
        .andExpect(model().attribute("deleteUser", hasProperty("uuid", is("user-uuid"))));

  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_binding_errors_then_requested_delete_account_page_is_returned_with_errors()
      throws Exception {

    FinanceUser user = FinanceUser.builder()
        .firstName("tom")
        .uuid("user-uuid")
        .build();
    when(financeService.getFinanceUserByUuid("user-uuid")).thenReturn(Optional.of(user));

    DeleteUserController controller = Mockito
        .spy(new DeleteUserController(authorizationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/confirm-delete-user.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/confirm-delete-user")
        .param("deleteUserYesNo", "")
        .param("uuid","user-uuid" ))
        .andExpect(handler().methodName("postDeleteUserPage"))
        .andExpect(model().attributeExists("deleteUser"))
        .andExpect(model().attribute("userName", "Tom's"))
        .andExpect(model().attribute("deleteUser", hasProperty("uuid", is("user-uuid"))))
        .andExpect(model().attributeErrorCount("deleteUser", 1))
        .andExpect(model().attributeHasFieldErrorCode("deleteUser", "deleteUserYesNo",
            "NotBlank"));

    verify(financeService, times(1)).getFinanceUserByUuid(Mockito.any(String.class));
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_y_then_page_posted_and_redirected_to_account_deleted()
      throws Exception {

    FinanceUser financeUser = FinanceUser.builder()
        .firstName("tom")
        .uuid("user-uuid")
        .build();
    when(financeService.getFinanceUserByUuid("user-uuid")).thenReturn(Optional.of(financeUser));

    User user = User.builder()
        .uuid("user-uuid")
        .build();
    when(authorizationService.deleteUser("user-uuid")).thenReturn(Optional.of(user));

    DeleteUserController controller = Mockito
        .spy(new DeleteUserController(authorizationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/confirm-delete-user.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/confirm-delete-user")
        .param("deleteUserYesNo", "Y")
        .param("uuid","user-uuid" ))
        .andExpect(handler().methodName("postDeleteUserPage"))
        .andExpect(model().attributeExists("deleteUser"))
        .andExpect(model().attribute("userName", "Tom's"))
        .andExpect(model().attribute("deleteUser", hasProperty("uuid", is("user-uuid"))))
        .andExpect((view().name("account-deleted")));

    verify(financeService, times(1)).getFinanceUserByUuid(Mockito.any(String.class));
    verify(authorizationService, times(1)).deleteUser(Mockito.any(String.class));
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_n_then_page_posted_and_redirected_to_current_users()
      throws Exception {

    FinanceUser financeUser = FinanceUser.builder()
        .firstName("tom")
        .uuid("user-uuid")
        .build();
    when(financeService.getFinanceUserByUuid("user-uuid")).thenReturn(Optional.of(financeUser));

    DeleteUserController controller = Mockito
        .spy(new DeleteUserController(authorizationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/confirm-delete-user.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/confirm-delete-user")
        .param("deleteUserYesNo", "N")
        .param("uuid","user-uuid" ))
        .andExpect(handler().methodName("postDeleteUserPage"))
        .andExpect((view().name("redirect:/current-users")));

    verify(financeService, times(1)).getFinanceUserByUuid(Mockito.any(String.class));
    verify(authorizationService, times(0)).deleteUser(Mockito.any(String.class));
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_and_user_not_present_then_500_response_is_sent()
      throws Exception {

    FinanceUser financeUser = FinanceUser.builder()
        .firstName("tom")
        .uuid("user-uuid")
        .build();
    when(financeService.getFinanceUserByUuid("user-uuid")).thenReturn(Optional.of(financeUser));

    when(authorizationService.deleteUser("user-uuid")).thenReturn(Optional.empty());

    DeleteUserController controller = Mockito
        .spy(new DeleteUserController(authorizationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/confirm-delete-user.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/confirm-delete-user")
        .param("deleteUserYesNo", "Y")
        .param("uuid","user-uuid" ))
        .andExpect(handler().methodName("postDeleteUserPage"))
        .andExpect((status().is5xxServerError()));

    verify(financeService, times(1)).getFinanceUserByUuid(Mockito.any(String.class));
    verify(authorizationService, times(1)).deleteUser(Mockito.any(String.class));
  }


}
