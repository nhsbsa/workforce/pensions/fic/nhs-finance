package com.nhsbsa.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.controllers.finance.StartController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
public class StartControllerTest {

  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    StartController controller = new StartController();

    mockMvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/start.html"))
        .build();
  }

  @Test
  public void given_start_endpoint_requested_then_start_page_returned()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/start"))
        .andExpect(status().isOk())
        .andExpect(view().name("start"));
  }

  @Test
  public void given_slash_endpoint_requested_then_start_page_returned()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/"))
        .andExpect(status().isOk())
        .andExpect(view().name("start"));
  }
}
