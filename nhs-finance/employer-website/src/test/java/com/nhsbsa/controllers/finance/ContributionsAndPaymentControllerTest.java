package com.nhsbsa.controllers.finance;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.object.IsCompatibleType.typeCompatibleWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.exceptions.AlreadySubmittedException;
import com.nhsbsa.exceptions.GlobalExceptionHandler;
import com.nhsbsa.service.RequestForTransferService;

import com.nhsbsa.view.RequestForTransferView;
import java.util.Date;
import java.util.Optional;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ContributionsAndPaymentControllerTest {

    private MockMvc mockMvc;
    private RequestForTransferService mockService;

    private static RequestForTransferView EXPECTED_RFT;
    private static RequestForTransferView EXPECTED_SUBMITTED_RFT;

    private static final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";
    private static final String INVALID_UUID = "invalid";

    private ContributionsAndPaymentController controller;

    @BeforeClass
    public static void classSetUp() throws Exception {
        EXPECTED_RFT = new RequestForTransferView();
        EXPECTED_RFT.setRftUuid(EXPECTED_UUID);

        EXPECTED_SUBMITTED_RFT = new RequestForTransferView();
        EXPECTED_SUBMITTED_RFT.setRftUuid(EXPECTED_UUID);
        EXPECTED_SUBMITTED_RFT.setSubmitDate(new Date());
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockService = mock(RequestForTransferService.class);
        controller = Mockito.spy(new ContributionsAndPaymentController(
                mockService));

        mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/summary.html"))
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();
    }

    @WithMockUser
    @Test
    public void given_endpoint_requested_when_valid_rft_uuid_provided_but_already_submitted_then_login_redirection_response_is_returned()
            throws Exception {
        when(mockService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
                .thenReturn(Optional.of(EXPECTED_SUBMITTED_RFT));

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/contributionsandpayment/{rftUuid}", EXPECTED_UUID))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/login"))
                .andReturn();

        assertThat(mvcResult.getResolvedException().getClass(),
                typeCompatibleWith(AlreadySubmittedException.class));
    }

    @WithMockUser
    @Test
    public void check_that_endpoint_post_request_exists_for_false_adjustmentRequired_value()
            throws Exception {

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.post("/contributionsandpayment/{rftUuid}", EXPECTED_UUID)
                        .param("adjustmentsRequired", "0")
                        .param("adjustment.adjustmentContributionDate.contributionMonth", "November"))
                .andReturn();

        verify(controller, times(1)).saveContributionPayment(any(String.class), any(RequestForTransferView.class), any(
                BindingResult.class));
    }

    @WithMockUser
    @Test
    public void check_that_endpoint_post_request_exists_for_true_adjustmentRequired_value()
            throws Exception {

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.post("/contributionsandpayment/{rftUuid}", EXPECTED_UUID)
                        .param("adjustmentsRequired", "1")
                        .param("adjustment.adjustmentContributionDate.contributionMonth", "November"))
                .andReturn();

        verify(controller, times(1)).saveContributionPayment(any(String.class), any(RequestForTransferView.class), any(
                BindingResult.class));
    }

    @WithMockUser
    @Test
    public void check_that_endpoint_post_request_exists_for_null_adjustmentRequired_value()
            throws Exception {

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.post("/contributionsandpayment/{rftUuid}", EXPECTED_UUID)
                        .param("adjustment.adjustmentContributionDate.contributionMonth", "November"))
                .andReturn();

        verify(controller, times(1)).saveContributionPayment(any(String.class), any(RequestForTransferView.class), any(
                BindingResult.class));
    }


    @WithMockUser
    @Test
    public void given_endpoint_requested_when_invalid_rft_uuid_provided_404_redirection_response_is_returned()
            throws Exception {
        when(mockService.getOptionalRequestForTransferByRftUuid(INVALID_UUID))
                .thenReturn(Optional.empty());

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/contributionsandpayment/{rftUuid}", INVALID_UUID))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }

    @Test
    public void postPageWithAdjustmentDateBeforeContributionDateReturnsError() throws Exception {

        final MockHttpSession session = new MockHttpSession();

        mockMvc.perform(MockMvcRequestBuilders.post("/contributionsandpayment/{rftUuid}", EXPECTED_UUID)
                .param("totalPensionablePay", "10000")
                .param("employeeContributions", "1000")
                .param("employerContributions", "9000")
                .param("totalDebitAmount", "10030")
                .param("contributionDate.contributionMonth", "November")
                .param("contributionDate.contributionYear", "2017")
                .param("adjustmentsRequired", "1")
                .param("adjustment.adjustmentContributionDate.contributionMonth", "March")
                .param("adjustment.adjustmentContributionDate.contributionYear", "2018")
                .param("adjustment.employeeContributions", "10")
                .param("adjustment.employerContributions", "20"))
                // .session(session))
                .andExpect(handler().handlerType(ContributionsAndPaymentController.class))
                .andExpect(handler().methodName("saveContributionPaymentWithAdjustment"))
                .andExpect(status().isOk())
                .andExpect(view().name(is(equalTo("contributionsandpayment"))))
                .andExpect(model().attributeErrorCount("rft", 1))
                .andExpect(model().attributeHasFieldErrorCode("rft", "adjustmentDateBeforeContributionDate", "AssertTrue"));
    }


    @Test
    public void postPageWithEmployerContributionErrorReturnsDynamicError() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/contributionsandpayment/{rftUuid}", EXPECTED_UUID)
            .param("totalPensionablePay", "100")
            .param("employeeContributions", "5")
            .param("employerContributions", "14.32")
            .param("totalDebitAmount", "19.32")
            .param("contributionDate.contributionMonth", "November")
            .param("contributionDate.contributionYear", "2017")
            .param("adjustmentsRequired", "0")
            .param("adjustment.adjustmentContributionDate.contributionMonth", "")
            .param("adjustment.adjustmentContributionDate.contributionYear", ""))
            .andExpect(hasBindingError(
                "rft", "Enter an employer contribution that''s £14.38 or more (14.38% of the total pensionable pay)")
            );
    }

    // Method required to get the BindingResult actual error message 
    private ResultMatcher hasBindingError(String attribute, String expectedMessage) {
        return result -> {
            BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + attribute);
            bindingResult.getAllErrors()
                .stream()
                .filter(oe -> attribute.equals(oe.getObjectName()))
                .forEach(oe -> assertEquals(
                    "Expected default message", expectedMessage, oe.getDefaultMessage())
                );
        };
    }





}
