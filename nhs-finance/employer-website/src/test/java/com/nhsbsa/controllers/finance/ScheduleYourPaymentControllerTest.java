package com.nhsbsa.controllers.finance;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.utility.ExampleDateGenerator;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import com.nhsbsa.view.RequestForTransferView;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class ScheduleYourPaymentControllerTest {

  @Value("${paymentdate.excluded.dates:01/01,07/05,28/05,27/07,25/12,26/12}")
  List<String> excludedDates;

  private MockMvc mockMvc;

  private static RequestForTransferView EXPECTED_RFT;

  private static RequestForTransferService mockService;

  @Autowired
  private WebApplicationContext wac;

  private static final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

  private static final String UNEXPECTED_UUID = "7a8068d9-d1bc-5a0b-a6c5-76947fe910b7";

  private static final String EXPECTED_EACODE = "EA1234";

  private static final DateTimeFormatter DAY_FORMAT = DateTimeFormatter.ofPattern("d");
  private static final DateTimeFormatter MONTH_FORMAT = DateTimeFormatter.ofPattern("MMMM");
  private static final DateTimeFormatter YEAR_FORMAT = DateTimeFormatter.ofPattern("yyyy");

  private static EmployingAuthorityView EA = EmployingAuthorityView.builder().eaCode("EA1234").name("").employerType("").build();
  private static List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA);
  private static OrganisationView ORG = OrganisationView.builder().identifier(null).name("").eas(EA_LIST).build();
  private static List<OrganisationView> ORG_LIST = Collections.singletonList(ORG);

  @BeforeClass
  public static void classSetUp() throws Exception {
    EXPECTED_RFT = new RequestForTransferView();
    EXPECTED_RFT.setRftUuid(EXPECTED_UUID);
    EXPECTED_RFT.setContributionDate(new ContributionDate("January", 2015));
    EXPECTED_RFT.setEaCode("EA1234");
  }

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    mockService = mock(RequestForTransferService.class);
    when(mockService.getOptionalRequestForTransferByRftUuid(EXPECTED_UUID))
        .thenReturn(Optional.of(EXPECTED_RFT));
    when(mockService.getRequestForTransferByRftUuid(EXPECTED_UUID))
        .thenReturn(EXPECTED_RFT);
    when(mockService.getOptionalRequestForTransferByRftUuid(UNEXPECTED_UUID))
        .thenReturn(Optional.empty());
    when(mockService.saveRequestForTransfer(Mockito.any(RequestForTransferView.class)))
        .then(returnsFirstArg());

    ScheduleYourPaymentController controller = new ScheduleYourPaymentController(mockService, new ExampleDateGenerator());

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();
    mockMvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_when_no_rft_uuid_provided_then_a_new_rft_instance_is_used()
      throws Exception {
    FinanceUser user = FinanceUser.builder().role(UserRoles.ROLE_ADMIN.name())
        .build();

    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));

    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    mockMvc.perform(MockMvcRequestBuilders.get("/scheduleyourpayment"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.USER_ROLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.EA_CODE))
        .andExpect(model().attribute("rft", hasProperty("rftUuid", nullValue())));
  }

  @Test(expected = Exception.class)
  public void given_endpoint_requested_when_invalid_rft_uuid_provided_then_an_error_is_returned()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/scheduleyourpayment/{rftUuid}", "MISSING-UUID"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.EA_CODE))
        .andExpect(model().attribute("rft", hasProperty("rftUuid", nullValue())));
  }

  @Test
  public void given_endpoint_requested_when_not_existing_rft_uuid_provided_then_a_404_response_is_returned()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
        .get("/scheduleyourpayment/{rftUuid}", UNEXPECTED_UUID))
        .andExpect(status().is(404));
  }

  @Test
  @WithMockUser
  public void given_endpoint_requested_when_valid_rft_uuid_provided_then_existing_rft_instance_is_used()
      throws Exception {
    FinanceUser user = FinanceUser.builder().role(UserRoles.ROLE_ADMIN.name()).build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    mockMvc.perform(MockMvcRequestBuilders.get("/scheduleyourpayment/{rftUuid}", EXPECTED_UUID))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.USER_ROLE_ATTR_NAME))
        .andExpect(model().attribute(ScheduleYourPaymentController.EA_CODE, EXPECTED_RFT.getEaCode()))
        .andExpect(model().attribute("rft", EXPECTED_RFT))
        .andExpect(model().attribute("rft", hasProperty("payAsSoonAsPossible")));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_ea_code_provided_then_ea_code_is_set()
      throws Exception {
    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.get("/scheduleyourpayment/ea/{eaCode}", EXPECTED_EACODE))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.USER_ROLE_ATTR_NAME))
        .andExpect(model().attribute(ScheduleYourPaymentController.EA_CODE, EXPECTED_EACODE))
        .andExpect(model().attribute("rft", hasProperty("rftUuid", nullValue())));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_empty_current_user_then_404_response_is_sent()
      throws Exception {

    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.empty());

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.get("/scheduleyourpayment/ea/{eaCode}", EXPECTED_EACODE))
        .andExpect(status().is(404));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_invalid_ea_code_then_404_response_is_sent()
      throws Exception {
    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.get("/scheduleyourpayment/ea/{eaCode}", "EA5555"))
        .andExpect(status().is(404));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_new_rft_and_forwardTo_is_default_when_post_form_then_url_is_conts_and_payments_page()
      throws Exception {
    LocalDate date = getNextValidDate();

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    when(mockService.saveRequestForTransfer(Mockito.any(RequestForTransferView.class)))
        .thenReturn(EXPECTED_RFT);

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", "")
        .param("forwardTo", "")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(view().name("redirect:/contributionsandpayment/" + EXPECTED_UUID));

    verify(mockService, times(1)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_existing_rft_and_forwardTo_is_default_when_post_form_then_url_is_conts_and_payments_page()
      throws Exception {
    LocalDate date = getNextValidDate();

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(view().name("redirect:/contributionsandpayment/" + EXPECTED_UUID));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_existing_rft_when_forwarding_to_default_when_post_form_then_rft_is_both_loaded_and_saved()
      throws Exception {
    LocalDate date = getNextValidDate();

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"));

    verify(mockService, times(1)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(1)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_empty_current_user_when_post_form_then_404_response_is_returned()
      throws Exception {
    LocalDate date = getNextValidDate();

    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.empty());

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().is(404));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_invalid_eacode_when_post_form_then_404_response_is_returned()
      throws Exception {
    LocalDate date = getNextValidDate();

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", "EA2222")
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().is(404));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void given_validation_errors_produced_when_page_submitted_for_existing_rft()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("contributionDate.contributionMonth", "")
        .param("contributionDate.contributionYear", "")
        .param("rftUuid", EXPECTED_UUID)
        .param("transferDate.days", "")
        .param("transferDate.month", "")
        .param("transferDate.year", "")
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 2))
        .andExpect(model().attributeHasFieldErrorCode("rft", "contributionDate",
            "ContributionDateValid"))
        .andExpect(model().attributeHasFieldErrorCode("rft", "transferDate",
            "TransferFormDateNotBlank"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class))));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void given_validation_errors_produced_when_page_submitted_for_new_rft()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", "")
        .param("forwardTo", "")
        .param("contributionDate.contributionMonth", "")
        .param("contributionDate.contributionYear", "")
        .param("transferDate.days", "")
        .param("transferDate.month", "")
        .param("transferDate.year", "")
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 2))
        .andExpect(model().attributeHasFieldErrorCode("rft", "contributionDate",
            "ContributionDateValid"))
        .andExpect(model().attributeHasFieldErrorCode("rft", "transferDate",
            "TransferFormDateNotBlank"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class))));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void given_not_blank_validation_errors_on_contribution_date_when_page_submitted()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", "")
        .param("forwardTo", "")
        .param("contributionDate.contributionMonth", "")
        .param("contributionDate.contributionYear", "")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 1))
        .andExpect(model().attributeHasFieldErrorCode("rft", "contributionDate",
            "ContributionDateValid"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class))));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }


  @Test
  public void given_incorrect_date_on_contribution_date_field_when_page_submitted()
      throws Exception {
   LocalDate date = getNextValidDate();
   MvcResult result =  mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
       .param("rftUuid", EXPECTED_UUID)
       .param("forwardTo", "")
        .param("contributionDate.contributionMonth", "13")
        .param("contributionDate.contributionYear", "1999")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 1))
        .andExpect(model().attributeHasFieldErrorCode("rft", "contributionDate",
            "ContributionDateValid"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class)))).andReturn();

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void given_not_blank_validation_errors_on_transfer_date_when_page_submitted()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("contributionDate.contributionMonth", date.format(MONTH_FORMAT))
        .param("contributionDate.contributionYear",  date.format(YEAR_FORMAT))
        .param("transferDate.days", "")
        .param("transferDate.month", "")
        .param("transferDate.year", "")
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 1))
        .andExpect(model().attributeHasFieldErrorCode("rft", "transferDate",
            "TransferFormDateNotBlank"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class))));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void given_invalid_date_on_transfer_date_field_when_page_submitted()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("contributionDate.contributionMonth", date.format(MONTH_FORMAT))
        .param("contributionDate.contributionYear",  date.format(YEAR_FORMAT))
        .param("transferDate.days", "")
        .param("transferDate.month", "2018")
        .param("transferDate.year", "")
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 1))
        .andExpect(model().attributeHasFieldErrorCode("rft", "transferDate",
            "TransferFormDateNotBlank"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class))));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void given_incorrect_date_on_transfer_date_field_when_page_submitted()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("rftUuid", EXPECTED_UUID)
        .param("forwardTo", "")
        .param("contributionDate.contributionMonth", date.format(MONTH_FORMAT))
        .param("contributionDate.contributionYear",  date.format(YEAR_FORMAT))
        .param("transferDate.days", "31")
        .param("transferDate.month", "2")
        .param("transferDate.year", "1999")
        .param("isGp", "0")
        .param("payAsSoonAsPossible", "N"))
        .andExpect(status().isOk())
        .andExpect(view().name("scheduleyourpayment"))
        .andExpect(model().attributeExists("rft"))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.PAYMENT_MONTH_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeExists(ScheduleYourPaymentController.HINT_DATE_EXAMPLE_ATTR_NAME))
        .andExpect(model().attributeErrorCount("rft", 1))
        .andExpect(model().attributeHasFieldErrorCode("rft", "transferDate",
            "ValidFormDate"))
        .andExpect(
            model().attribute("rft", is(instanceOf(RequestForTransferView.class))));

    verify(mockService, times(0)).getRequestForTransferByRftUuid(Mockito.anyString());
    verify(mockService, times(0)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_existing_rft_and_forwardTo_is_summary_page_when_post_form_then_url_is_summary_page()
      throws Exception {
    LocalDate date = getNextValidDate();

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("forwardTo", "summary")
        .param("rftUuid", EXPECTED_UUID)
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "Y"))
        .andExpect(view().name("redirect:/summary/" + EXPECTED_UUID));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_existing_rft_when_forwarding_to_summary_page_then_rft_is_updated()
      throws Exception {
    LocalDate date = getNextValidDate();

    FinanceUser user = FinanceUser.builder().organisations(ORG_LIST).role(UserRoles.ROLE_ADMIN.name())
        .build();
    ScheduleYourPaymentController controller = Mockito
        .spy(new ScheduleYourPaymentController(mockService, new ExampleDateGenerator()));
    when(controller.getCurrentUser()).thenReturn(Optional.of(user));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/scheduleyourpayment.html"))
        .build();

    mvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("forwardTo", "summary")
        .param("rftUuid", EXPECTED_UUID)
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"));

    verify(mockService, Mockito.times(1)).getRequestForTransferByRftUuid(EXPECTED_UUID);
    verify(mockService, Mockito.times(1))
        .saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test(expected = Exception.class)
  public void given_non_existing_rft_when_forwarding_to_summary_page_then_exception_is_raised()
      throws Exception {
    LocalDate date = getNextValidDate();
    mockMvc.perform(MockMvcRequestBuilders.post("/scheduleyourpayment/{eaCode}", EXPECTED_EACODE)
        .param("forwardTo", "summary")
        .param("rftUuid", "88888888-4444-4444-4444-CCCCCCCCCCCC")
        .param("transferDate.days", date.format(DAY_FORMAT))
        .param("transferDate.month", date.format(MONTH_FORMAT))
        .param("transferDate.year", date.format(YEAR_FORMAT))
        .param("isGp", "0")
        .param("contributionDate.contributionMonth", "January")
        .param("contributionDate.contributionYear", "2016")
        .param("payAsSoonAsPossible", "N"));

    verify(mockService, Mockito.times(1))
        .getRequestForTransferByRftUuid("88888888-4444-4444-4444-CCCCCCCCCCCC");
    verify(mockService, Mockito.times(1))
        .saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  //RFT transfer date validation won't allow a date on the weekend or bank holiday,
  //so we need to ensure a test date that doesn't breach these requirements.
	private LocalDate getNextValidDate() {

		LocalDate localDate = LocalDate.now();
		localDate = localDate.plusDays(3);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM");

		//If we're hitting a weekend or excluded date then move to next day.
		while (DayOfWeek.SATURDAY.equals(localDate.getDayOfWeek())
				|| DayOfWeek.SUNDAY.equals(localDate.getDayOfWeek())
				|| excludedDates.contains(localDate.format(formatter))) {
			localDate = localDate.plusDays(1);
		}

		return localDate;
	}
}
