package com.nhsbsa.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.EmployerTypes;

import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RequestForTransferServiceTest.class)
@WithMockUser
public class RequestForTransferServiceTest {

    private static final String RFT_UUID = "123";
    private static final String EA_CODE = "EA1111";
    private RequestForTransferService requestForTransferService;

    private RestTemplate restTemplate;
    private BackendApiUriService backendApiUriService;

    @Before
    public void before() {
        restTemplate = Mockito.mock(RestTemplate.class);

        backendApiUriService = new BackendApiUriService(
                "http",
                "host",
                "8080",
                "/employer-service-context"
        );

        requestForTransferService = new RequestForTransferService(
                backendApiUriService,
                restTemplate
        );
    }

    @Test
    public void getRequestForTransferByRftUuid() {
      RequestForTransferView expectedRft = new RequestForTransferView();
        given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + RFT_UUID,
            RequestForTransferView.class)).willReturn(expectedRft);

        RequestForTransferView actualRft = requestForTransferService
                .getRequestForTransferByRftUuid(RFT_UUID);

        assertThat(actualRft, is(sameInstance((expectedRft))));
    }

    @Test
    public void saveRequestForTransfer() {
      RequestForTransferView newRft = new RequestForTransferView();
      newRft.setEaCode("EA1234");
      RequestForTransferView expectedRft = new RequestForTransferView();

      String emptype = EmployerTypes.GP.name();
      given(restTemplate.getForObject("http://host:8080/employer-service-context/employer-type/"+ newRft.getEaCode(),
          String.class)).willReturn(emptype);

      given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/requestfortransfer", newRft,
          RequestForTransferView.class)).willReturn(expectedRft);

      RequestForTransferView actualRft = requestForTransferService.saveRequestForTransfer(newRft);

      assertThat(actualRft, is(sameInstance((expectedRft))));
    }

    @Test
    public void saveRequestForTransferSetsEaCode() {

      RequestForTransferView newRft = new RequestForTransferView();
      newRft.setEaCode("EA1234");
      RequestForTransferView expectedRft = new RequestForTransferView();
      expectedRft.setEaCode("EA1234");
      expectedRft.setIsGp(true);

      String emptype = EmployerTypes.GP.name();
      given(restTemplate.getForObject("http://host:8080/employer-service-context/employer-type/"+ newRft.getEaCode(),
          String.class)).willReturn(emptype);

      given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/requestfortransfer", newRft,
          RequestForTransferView.class)).willReturn(expectedRft);

      RequestForTransferView actualRft = requestForTransferService.saveRequestForTransfer(newRft);

      assertThat(actualRft.getEaCode(), is(equalTo(("EA1234"))));
      assertTrue(actualRft.getIsGp());
    }

    @Test
    public void saveContributionPayment() {
        RequestForTransferView emptyRft = new RequestForTransferView();
        RequestForTransferView savedRft = new RequestForTransferView();

        given(requestForTransferService.getRequestForTransferByRftUuid(RFT_UUID)).willReturn(savedRft);

        emptyRft.setTotalPensionablePay(BigDecimal.valueOf(5000));
        emptyRft.setEmployeeContributions(BigDecimal.valueOf(50));
        emptyRft.setEmployerContributions(BigDecimal.valueOf(20));
        emptyRft.setEmployeeAddedYears(BigDecimal.valueOf(30));
        emptyRft.setAdditionalPension(BigDecimal.valueOf(25));
        emptyRft.setErrbo(BigDecimal.valueOf(15));
        emptyRft.setTotalDebitAmount(BigDecimal.valueOf(5050));

        RequestForTransferView actualRft = requestForTransferService
                .saveContributionPayment(RFT_UUID, emptyRft);

        assertThat(actualRft.getTotalPensionablePay(), is(equalTo(emptyRft.getTotalPensionablePay())));
        assertThat(actualRft.getEmployeeContributions(),
                is(equalTo(emptyRft.getEmployeeContributions())));
        assertThat(actualRft.getEmployerContributions(),
                is(equalTo(emptyRft.getEmployerContributions())));
        assertThat(actualRft.getEmployeeAddedYears(), is(equalTo(emptyRft.getEmployeeAddedYears())));
        assertThat(actualRft.getAdditionalPension(), is(equalTo(emptyRft.getAdditionalPension())));
        assertThat(actualRft.getErrbo(), is(equalTo(emptyRft.getErrbo())));
        assertThat(actualRft.getTotalDebitAmount(), is(equalTo(emptyRft.getTotalDebitAmount())));

        assertThat(actualRft, is(sameInstance((savedRft))));

        verify(restTemplate).postForObject("http://host:8080/employer-service-context/finance/requestfortransfer", savedRft,
            RequestForTransferView.class);
    }


    @Test
    public void saveContributionAndAdjustmentPayment() {
        RequestForTransferView emptyRft = new RequestForTransferView();
        RequestForTransferView savedRft = new RequestForTransferView();
        List<Adjustment> adjustmentListTemp = new ArrayList<>();

        given(requestForTransferService.getRequestForTransferByRftUuid(RFT_UUID)).willReturn(savedRft);

        emptyRft.setTotalPensionablePay(BigDecimal.valueOf(5000));
        emptyRft.setEmployeeContributions(BigDecimal.valueOf(50));
        emptyRft.setEmployerContributions(BigDecimal.valueOf(20));
        emptyRft.setEmployeeAddedYears(BigDecimal.valueOf(30));
        emptyRft.setAdditionalPension(BigDecimal.valueOf(25));
        emptyRft.setErrbo(BigDecimal.valueOf(15));
        emptyRft.setTotalDebitAmount(BigDecimal.valueOf(5050));

// Create and populate items in an Adjustment object, the first “list” entry (only one for this test but may have numerous)
        Adjustment adjustmentTestIn1 = new Adjustment();
        ContributionDate adjustmentContributionDate = new ContributionDate();
        adjustmentContributionDate.setContributionMonth("July");
        adjustmentContributionDate.setContributionYear(2015);
        adjustmentTestIn1
                .setEmployeeContributions(new BigDecimal(3.1).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1
                .setEmployerContributions(new BigDecimal(5.2).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1
                .setEmployeeAddedYears(new BigDecimal(7.3).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1
                .setAdditionalPension(new BigDecimal(9.5).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1.setErrbo(new BigDecimal(11.7).setScale(2, BigDecimal.ROUND_HALF_UP));

        // Add temp object (Adjustment class) into the list of "Adjustments", can do many times but this is a test for one only
        adjustmentListTemp.add(adjustmentTestIn1);

        emptyRft.setAdjustmentList(adjustmentListTemp);
        RequestForTransferView actualRft = requestForTransferService
                .saveContributionPayment(RFT_UUID, emptyRft);

        assertThat(actualRft.getTotalPensionablePay(), is(equalTo(emptyRft.getTotalPensionablePay())));
        assertThat(actualRft.getEmployeeContributions(),
                is(equalTo(emptyRft.getEmployeeContributions())));
        assertThat(actualRft.getEmployerContributions(),
                is(equalTo(emptyRft.getEmployerContributions())));
        assertThat(actualRft.getEmployeeAddedYears(), is(equalTo(emptyRft.getEmployeeAddedYears())));
        assertThat(actualRft.getAdditionalPension(), is(equalTo(emptyRft.getAdditionalPension())));
        assertThat(actualRft.getErrbo(), is(equalTo(emptyRft.getErrbo())));
        assertThat(actualRft.getTotalDebitAmount(), is(equalTo(emptyRft.getTotalDebitAmount())));
        assertThat(actualRft.getAdjustmentList(), is(equalTo(emptyRft.getAdjustmentList())));

        Iterator<Adjustment> iterEmptyRft = emptyRft.getAdjustmentList().iterator();

        for (Iterator<Adjustment> iterActualRft = actualRft.getAdjustmentList().iterator();
             iterActualRft.hasNext(); ) {
            Adjustment actualRftElement = iterActualRft.next();
            Adjustment emptyRftElement = iterEmptyRft.next();
            assertThat(actualRftElement, is(equalTo(emptyRftElement)));
        }

        assertThat(actualRft, is(sameInstance((savedRft))));

        verify(restTemplate).postForObject("http://host:8080/employer-service-context/finance/requestfortransfer", savedRft,
            RequestForTransferView.class);
    }

    @Test
    public void saveContributionAndMultipleAdjustmentPayments() {
        RequestForTransferView emptyRft = new RequestForTransferView();
        RequestForTransferView savedRft = new RequestForTransferView();
        List<Adjustment> adjustmentListTemp = new ArrayList<>();

        given(requestForTransferService.getRequestForTransferByRftUuid(RFT_UUID)).willReturn(savedRft);

        emptyRft.setTotalPensionablePay(BigDecimal.valueOf(5000));
        emptyRft.setEmployeeContributions(BigDecimal.valueOf(50));
        emptyRft.setEmployerContributions(BigDecimal.valueOf(20));
        emptyRft.setEmployeeAddedYears(BigDecimal.valueOf(30));
        emptyRft.setAdditionalPension(BigDecimal.valueOf(25));
        emptyRft.setErrbo(BigDecimal.valueOf(15));
        emptyRft.setTotalDebitAmount(BigDecimal.valueOf(5050));

// Create and populate items in an Adjustment object, the first “list” entry (first for this test and another added later too)
        Adjustment adjustmentTestIn1 = new Adjustment();
        ContributionDate adjustmentContributionDateTestIn1 = new ContributionDate();
        adjustmentContributionDateTestIn1.setContributionMonth("July");
        adjustmentContributionDateTestIn1.setContributionYear(2015);
        adjustmentTestIn1
                .setEmployeeContributions(new BigDecimal(3.1).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1
                .setEmployerContributions(new BigDecimal(5.2).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1
                .setEmployeeAddedYears(new BigDecimal(7.3).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1
                .setAdditionalPension(new BigDecimal(9.5).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn1.setErrbo(new BigDecimal(11.7).setScale(2, BigDecimal.ROUND_HALF_UP));

        // Add temp object (Adjustment class) into the list of "Adjustments", can do many times so this is the first entry
        adjustmentListTemp.add(adjustmentTestIn1);

        // Create and populate another list set of items in an Adjustment object, the second “list” entry, to prove multiple entries work
        Adjustment adjustmentTestIn2 = new Adjustment();
        ContributionDate adjustmentContributionDateTestIn2 = new ContributionDate();
        adjustmentContributionDateTestIn2.setContributionMonth("September");
        adjustmentContributionDateTestIn2.setContributionYear(2014);
        adjustmentTestIn2
                .setEmployeeContributions(new BigDecimal(23.1).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn2
                .setEmployerContributions(new BigDecimal(25.2).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn2
                .setEmployeeAddedYears(new BigDecimal(27.3).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn2
                .setAdditionalPension(new BigDecimal(29.5).setScale(2, BigDecimal.ROUND_HALF_UP));
        adjustmentTestIn2.setErrbo(new BigDecimal(211.7).setScale(2, BigDecimal.ROUND_HALF_UP));

        adjustmentListTemp.add(adjustmentTestIn2);

        emptyRft.setAdjustmentList(adjustmentListTemp);

        // Expository: only the first Adjustment in a multiple adjustment scenario is persisted.
        // For private beta only a single Adjustment can be recorded against a request for transfer.
        RequestForTransferView actualRft = requestForTransferService
                .saveContributionPayment(RFT_UUID, emptyRft);

        assertThat(actualRft.getTotalPensionablePay(), is(equalTo(emptyRft.getTotalPensionablePay())));
        assertThat(actualRft.getEmployeeContributions(),
                is(equalTo(emptyRft.getEmployeeContributions())));
        assertThat(actualRft.getEmployerContributions(),
                is(equalTo(emptyRft.getEmployerContributions())));
        assertThat(actualRft.getEmployeeAddedYears(), is(equalTo(emptyRft.getEmployeeAddedYears())));
        assertThat(actualRft.getAdditionalPension(), is(equalTo(emptyRft.getAdditionalPension())));
        assertThat(actualRft.getErrbo(), is(equalTo(emptyRft.getErrbo())));
        assertThat(actualRft.getTotalDebitAmount(), is(equalTo(emptyRft.getTotalDebitAmount())));
        // See comment above re: only allowing a single Adjustment
        assertThat(actualRft.getAdjustment(), is(equalTo(emptyRft.getAdjustmentList().get(0))));

        Iterator<Adjustment> iterEmptyRft = emptyRft.getAdjustmentList().iterator();

        // Loop through all the elements of the list (just do one list as should be in sync, if not then will error).
        for (Iterator<Adjustment> iterActualRft = actualRft.getAdjustmentList().iterator();
             iterActualRft.hasNext(); ) {

            Adjustment actualRftListElement = iterActualRft.next();
            Adjustment emptyRftListElement = iterEmptyRft.next();

            // Generic check of the list element itself (has all members in so a quicker way of checking in one go)
            assertThat(actualRftListElement, is(equalTo(emptyRftListElement)));

            // NOTE that you can also do each individual member of the list element you have for being specific as below.
            assertThat(actualRftListElement.getEmployeeContributions(),
                    is(equalTo(emptyRftListElement.getEmployeeContributions())));
            assertThat(actualRftListElement.getEmployerContributions(),
                    is(equalTo(emptyRftListElement.getEmployerContributions())));
            assertThat(actualRftListElement.getEmployeeAddedYears(),
                    is(equalTo(emptyRftListElement.getEmployeeAddedYears())));
            assertThat(actualRftListElement.getAdditionalPension(),
                    is(equalTo(emptyRftListElement.getAdditionalPension())));
        }

        assertThat(actualRft, is(sameInstance((savedRft))));

        verify(restTemplate).postForObject("http://host:8080/employer-service-context/finance/requestfortransfer", savedRft,
            RequestForTransferView.class);
    }


    @Test
    public void happyPathGetRequestForTransferByRftUuidValidating() {
        String uuid = "9bae9b12-698f-457e-a22c-693ac186d952";
        RequestForTransferView expectedRft = new RequestForTransferView();
        expectedRft.setRftUuid(uuid);
        given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + uuid,
              RequestForTransferView.class)).willReturn(expectedRft);

    Optional<RequestForTransferView> actualRft = requestForTransferService
        .getRequestForTransferByRftUuidValidating(uuid);

    assertThat(actualRft.get(), is(sameInstance((expectedRft))));
  }

  @Test
  public void noEntityGetRequestForTransferByRftUuidValidating() {
    String uuid = "7a8068d9-d1bc-5a0b-a6c5-76947fe910b7";
    RequestForTransferView expectedRft = new RequestForTransferView();
    expectedRft.setRftUuid(uuid);
    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + uuid,
        RequestForTransferView.class)).willReturn(null);

    Optional<RequestForTransferView> actualRft = requestForTransferService
        .getRequestForTransferByRftUuidValidating(uuid);

    assertFalse(actualRft.isPresent());
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidUuidGetRequestForTransferByRftUuidValidating() {
    String uuid = "INVALID_UUID";
    RequestForTransferView expectedRft = new RequestForTransferView();
    expectedRft.setRftUuid(uuid);
    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + uuid,
        RequestForTransferView.class)).willReturn(expectedRft);

    Optional<RequestForTransferView> actualRft = requestForTransferService
        .getRequestForTransferByRftUuidValidating(uuid);

    assertThat(actualRft.get(), is(sameInstance((expectedRft))));
  }

  @Test
  public void submitContributionPaymentTest() {
    RequestForTransferView emptyRft = new RequestForTransferView();
    RequestForTransferView savedRft = new RequestForTransferView();

    given(requestForTransferService.getRequestForTransferByRftUuid(RFT_UUID)).willReturn(savedRft);

    Date now = Date.from(Instant.now());
    emptyRft.setSubmitDate(now);

    RequestForTransferView actualRft = requestForTransferService.submitContributionPayment(RFT_UUID);

    assertThat(emptyRft.getSubmitDate().getDate(), equalTo(actualRft.getSubmitDate().getDate()));
    assertThat(emptyRft.getSubmitDate().getHours(), equalTo(actualRft.getSubmitDate().getHours()));
    assertThat(emptyRft.getSubmitDate().getMinutes(), equalTo(actualRft.getSubmitDate().getMinutes()));
    assertThat(actualRft, is(sameInstance((savedRft))));
  }

  @Test
  public void happyPathGetOptionalRequestForTransferByRftUuid() {
    String uuid = "9bae9b12-698f-457e-a22c-693ac186d952";
    RequestForTransferView expectedRft = new RequestForTransferView();
    expectedRft.setRftUuid(uuid);
    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + uuid,
        RequestForTransferView.class)).willReturn(expectedRft);

    Optional<RequestForTransferView> actualRft = requestForTransferService
        .getOptionalRequestForTransferByRftUuid(uuid);

    assertThat(actualRft.get(), is(sameInstance((expectedRft))));
  }

  @Test
  public void noEntityGetOptionalRequestForTransferByRftUuid() {
    String uuid = "7a8068d9-d1bc-5a0b-a6c5-76947fe910b7";
    RequestForTransferView expectedRft = new RequestForTransferView();
    expectedRft.setRftUuid(uuid);
    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + uuid,
        RequestForTransferView.class)).willReturn(null);

    Optional<RequestForTransferView> actualRft = requestForTransferService
        .getOptionalRequestForTransferByRftUuid(uuid);

    assertFalse(actualRft.isPresent());
  }

  @Test
  public void invalidUuidGetOptionalRequestForTransferByRftUuid() {
    String uuid = "INVALID_UUID";
    RequestForTransferView expectedRft = new RequestForTransferView();
    expectedRft.setRftUuid(uuid);
    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/requestfortransfer/" + uuid,
        RequestForTransferView.class)).willReturn(null);

    Optional<RequestForTransferView> actualRft = requestForTransferService
        .getOptionalRequestForTransferByRftUuid(uuid);

    assertFalse(actualRft.isPresent());
  }

  @Test
  public void getSubmitDateByEaCodeReturnsDate() {
    LocalDate expectedSubmitDate = LocalDate.of(2018,8,15);
    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/submitdate/" + EA_CODE,
        LocalDate.class)).willReturn(expectedSubmitDate);

    Optional<LocalDate> actualSubmitDate = requestForTransferService
        .getSubmitDateByEaCode(EA_CODE);


    assertThat(actualSubmitDate.get(), is(sameInstance((expectedSubmitDate))));
  }

  @Test
  public void getSubmitDateByEaCodeReturnsEmpty() {

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/submitdate/" + EA_CODE,
        LocalDate.class)).willReturn(null);

    Optional<LocalDate> actualSubmitDate = requestForTransferService
        .getSubmitDateByEaCode(EA_CODE);

    assertFalse(actualSubmitDate.isPresent());
  }
}