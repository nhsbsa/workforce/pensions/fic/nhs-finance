package com.nhsbsa.controllers.finance;

/**
 * Created by nataliehulse on 23/11/2017.
 */

import com.nhsbsa.model.ForgottenPassword;
import com.nhsbsa.security.ValidateEmailResponse;
import com.nhsbsa.service.authentication.AuthorizationService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Slf4j
public class ForgottenPasswordController {


    private final AuthorizationService authorizationService;

    private static final String FORGOTTEN_PASS_VIEW_NAME = "forgottenpassword";
    private static final String FORGOTTEN_PASS_RESET_VIEW_NAME = "forgottenpasswordreset";

    @Autowired
    public ForgottenPasswordController(
            final AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }


    @RequestMapping(value = "/forgotten-password", method = RequestMethod.GET)
    public ModelAndView showForgottenPasswordPage() {

        log.info("GET - Accessing to Forgotten Password Screen");

        ModelAndView modelAndView = new ModelAndView(FORGOTTEN_PASS_VIEW_NAME);
        modelAndView.addObject("forgottenPassword", new ForgottenPassword());

        return modelAndView;
    }

    @RequestMapping(value = "/forgotten-password-reset", method = RequestMethod.GET)
    public ModelAndView showForgottenPasswordResetPage() {
        log.info("GET - Accessing to Reset Password Screen");
        return new ModelAndView(FORGOTTEN_PASS_RESET_VIEW_NAME);
    }

    @PostMapping("/forgotten-password")
    public String postForgottenPasswordPage(@ModelAttribute @Valid ForgottenPassword forgottenPassword,
                                      final BindingResult bindingResult)  {

        log.info("POST - Forgotten Password");
        if (bindingResult.hasErrors()) {
            return FORGOTTEN_PASS_VIEW_NAME;
        }

        log.info("Forgotten Password - Checking username = {}", forgottenPassword.getEmail());
        ValidateEmailResponse validateEmailResponse = authorizationService.checkUsername(forgottenPassword);

        if (validateEmailResponse != null) {
            log.info("Forgotten Password - Sending email to user = {}", validateEmailResponse.getUserName());
            authorizationService.sendEmail(validateEmailResponse);

        }

        return FORGOTTEN_PASS_RESET_VIEW_NAME;
    }



}
