package com.nhsbsa.controllers.finance;

import com.nhsbsa.exceptions.AlreadySubmittedException;
import com.nhsbsa.view.RequestForTransferView;
import com.nhsbsa.model.validation.AdjustmentNotNullValidationGroup;
import com.nhsbsa.model.validation.AdjustmentValidationGroup;
import com.nhsbsa.model.validation.ContributionsValidationGroup;
import com.nhsbsa.service.RequestForTransferService;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ianfulcher on 16/11/2016.
 */
@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN') or hasRole('STANDARD')")
public class ContributionsAndPaymentController {

  private final RequestForTransferService requestForTransferService;
  private static final String CONTRIBUTIONS_AND_PAYMENT_VIEW = "contributionsandpayment";

  @Autowired
  public ContributionsAndPaymentController(
      final RequestForTransferService requestForTransferService) {
    this.requestForTransferService = requestForTransferService;
  }

  // Loading of the "Contributions and payment" page, use the UUID and get rft details what entered in "Schedule your payment"
  @GetMapping(value = "/contributionsandpayment/{rftUuid}")
  public ModelAndView contributionsandpayment(@PathVariable("rftUuid") final String rftUuid,
      HttpServletResponse response) throws IOException, AlreadySubmittedException {

    log.info(String.format("GET - Accessing to Contributions and Payment screen with rft uuid = %s", rftUuid));

    RequestForTransferView rft;

    Optional<RequestForTransferView> optionalRft = requestForTransferService
        .getOptionalRequestForTransferByRftUuid(rftUuid);
    if (optionalRft.isPresent()) {

      rft = optionalRft.get();

      if (isRequestAlreadySubmitted(rft)) {
        log.error("Request is already submitted");
        throw new AlreadySubmittedException();
      }

      ModelAndView modelAndView = new ModelAndView(CONTRIBUTIONS_AND_PAYMENT_VIEW);
      modelAndView.addObject("rft", rft);
      return modelAndView;
    }

    log.error(String.format("request for transfer with rft uuid = %s not found", rftUuid));
    response.sendError(404);
    return null;

  }

  private boolean isRequestAlreadySubmitted(RequestForTransferView requestForTransfer) {
    return requestForTransfer.getSubmitDate() != null;
  }


  // Click on "Next step" on the "Contributions and payment" page, with Adjustment Required set to YES
  @PostMapping(value = "/contributionsandpayment/{rftUuid}", params = "adjustmentsRequired=1")
  public String saveContributionPaymentWithAdjustment(@PathVariable("rftUuid") final String rftUuid,
      @Validated(value = {ContributionsValidationGroup.class, AdjustmentNotNullValidationGroup.class, AdjustmentValidationGroup.class})
      @ModelAttribute("rft") final RequestForTransferView requestForTransfer,
      final BindingResult bindingResult) {

    log.info(String.format("POST - Contributions and Payment with adjustments required and rft uuid = %s", rftUuid));
    return saveContributionPayment(rftUuid, requestForTransfer, bindingResult);
  }

  // Click on "Next step" on the "Contributions and payment" page, with Adjustment Required set to NO
  @PostMapping(value = "/contributionsandpayment/{rftUuid}", params = {"adjustmentsRequired=0"})
  public String saveContributionPaymentWithoutAdjustment(
      @PathVariable("rftUuid") final String rftUuid,
      @Validated(value = ContributionsValidationGroup.class)
      @ModelAttribute("rft") final RequestForTransferView requestForTransfer,
      final BindingResult bindingResult) {

    log.info("POST - Contributions and Payment with adjustments not required and rft uuid = {}", rftUuid);

    requestForTransfer.removeAdjustment();
    return saveContributionPayment(rftUuid, requestForTransfer, bindingResult);
  }

  // Click on "Next step" on the "Contributions and payment" page, with Adjustment Required set to NULL
  @PostMapping(value = "/contributionsandpayment/{rftUuid}", params = {"!adjustmentsRequired"})
  public String saveContributionPaymentNoAdjustment(@PathVariable("rftUuid") final String rftUuid,
      @Validated(value = ContributionsValidationGroup.class)
      @ModelAttribute("rft") final RequestForTransferView requestForTransfer,
      final BindingResult bindingResult) {
    log.info("POST - Contributions and Payment with no adjustments and rft uuid = {}", rftUuid);
    return saveContributionPaymentWithoutAdjustment(rftUuid, requestForTransfer, bindingResult);
  }

   String saveContributionPayment(final String rftUuid,
      final RequestForTransferView requestForTransfer,
      final BindingResult bindingResult) {

    if (bindingResult.hasErrors()) {
      return CONTRIBUTIONS_AND_PAYMENT_VIEW;
    }

    // All data ok, save what is in "requestForTransfer" into rft and then store in DB
    log.debug("About to save data from Finance 'Contributions and payment' page in the Database");
    requestForTransferService.saveContributionPayment(rftUuid, requestForTransfer);
    log.debug("Data from Finance 'Contributions and payment' page saved ok in the Database");
    return String.format("redirect:/summary/%s", requestForTransfer.getRftUuid());
  }


}
