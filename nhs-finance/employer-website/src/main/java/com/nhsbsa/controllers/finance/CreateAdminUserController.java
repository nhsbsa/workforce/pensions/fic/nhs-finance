package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.AdminUser;
import com.nhsbsa.model.EmployingAuthority;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.RegistrationRequest;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.OrganisationService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.service.authentication.UserRegistrationService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('MASTER')")
public class CreateAdminUserController {

  private static final String CREATE_ADMIN_USER_VIEW_NAME = "admin/create-employer-admin-user";
  private static final String CREATE_EA_VIEW_NAME = "admin/create-ea";

  private static final String CREATE_ADMIN_USER_ENDPOINT = "/create-admin-user";
  private static final String CREATE_EA_ENDPOINT = "/create-ea/{uuid}";

  private static final String DISPLAY_EA_REDIRECT = "redirect:/create-ea/";

  private static final String NEW_USER_MAIL = "userMail";


  private final OrganisationService organisationService;
  private final AuthorizationService authorizationService;
  private final UserRegistrationService userRegistrationService;
  private final FinanceService financeService;

  @Autowired
  public CreateAdminUserController(final OrganisationService organisationService,
      AuthorizationService authorizationService, UserRegistrationService userRegistrationService, FinanceService financeService) {
    this.organisationService = organisationService;
    this.authorizationService = authorizationService;
    this.userRegistrationService = userRegistrationService;
    this.financeService = financeService;
  }

  @GetMapping(value = CREATE_ADMIN_USER_ENDPOINT)
  public ModelAndView showCreateAdminUserPage() {
    log.info("GET - Accessing to Create Multi Ea Admin Employer screen - Step 1");

    ModelAndView modelAndView = new ModelAndView(CREATE_ADMIN_USER_VIEW_NAME);
    modelAndView.addObject("adminUser", new AdminUser());

    return modelAndView;
  }

  @PostMapping(value = CREATE_ADMIN_USER_ENDPOINT)
  public String createAdminEmployer(
      @ModelAttribute @Valid AdminUser adminUser,
      final BindingResult bindingResult, ModelMap model,
      HttpServletResponse httpResponse) throws IOException {

    if (bindingResult.hasErrors()) {
      return CREATE_ADMIN_USER_VIEW_NAME;
    }

    log.info("Create admin user - Step 1 - Retrieving user by email = {}",
        adminUser.getEmail().toLowerCase());

    Optional<User> maybeAuthenticationUser = authorizationService.getUserByEmail(
        adminUser.getEmail().toLowerCase());

    if (maybeAuthenticationUser.isPresent()) {

      String uuid = maybeAuthenticationUser.get().getUuid();

      log.info(
          "Create admin user - Step 1 - Authentication user exists - Retrieving finance user by uuid = {}",
          uuid);

      Optional<FinanceUser> maybeFinanceUser = financeService.getFinanceUserByUuid(uuid);

      if (maybeFinanceUser.isPresent()) {

        model.addAttribute("userExists", true);
        return CREATE_ADMIN_USER_VIEW_NAME;
      } else {

        // Check Organisation doesn't exist
        Optional<String> organisation = organisationService.getOrganisationByName(adminUser.getOrganisationName());
        if (organisation.isPresent()) {
          model.addAttribute("orgExists", true);
          return CREATE_ADMIN_USER_VIEW_NAME;
        }

        log.info(
            "Create admin employer - Step 1 - Authentication user exists - Creating finance user / organisation by uuid = {}",
            uuid);
        adminUser.setUuid(uuid);
        Optional<CreateUserResponse> financeResponse = registerEmployerUser(adminUser);

        if (!financeResponse.isPresent()) {

          httpResponse.sendError(500);
          return null;
        }
        return DISPLAY_EA_REDIRECT + adminUser.getUuid();
      }
    }

    // Check Organisation doesn't exist
    Optional<String> organisation = organisationService.getOrganisationByName(adminUser.getOrganisationName());
    if (organisation.isPresent()) {
      model.addAttribute("orgExists", true);
      return CREATE_ADMIN_USER_VIEW_NAME;
    }

    log.info("Create admin user - Step 1 - registering authentication user");
    Optional<AuthenticationResponse> response = registerAuthenticationUser(adminUser);

    if (!response.isPresent()) {
      httpResponse.sendError(500);
      return null;
    }

    log.info("Create admin employer - Step 1 - registering finance user / organisation");
    adminUser.setUuid(response.get().getUuid());
    Optional<CreateUserResponse> financeResponse = registerEmployerUser(adminUser);

    if (!financeResponse.isPresent()) {
      httpResponse.sendError(500);
      return null;
    }

    return DISPLAY_EA_REDIRECT + adminUser.getUuid();
  }

  @GetMapping(value = CREATE_EA_ENDPOINT)
  public ModelAndView showCreateEaPage(@PathVariable("uuid") final String uuid,
      HttpServletResponse httpResponse) throws IOException {

    log.info("GET - Accessing to Create Multi Ea Admin Employer screen - Step 2");

    Optional<FinanceUser> user = financeService.getFinanceUserByUuid(uuid);

    if(user.isPresent()) {
      List<EmployingAuthorityView> eas = user.map(u -> u.getOrganisations().get(0).getEas())
          .orElse(Collections.emptyList());
      ModelAndView modelAndView = new ModelAndView(CREATE_EA_VIEW_NAME);
      modelAndView.addObject("employingAuthority", new EmployingAuthority());
      modelAndView.addObject("eas", eas);
      modelAndView.addObject(NEW_USER_MAIL, user.get().getUsername());

      return modelAndView;
    }

    httpResponse.sendError(500);
    return null;
  }

  @PostMapping(value = CREATE_EA_ENDPOINT)
  public String createEa(
      @PathVariable("uuid") final String uuid,
      @ModelAttribute @Valid EmployingAuthority ea,
      final BindingResult bindingResult, ModelMap model,
      HttpServletResponse httpResponse) throws IOException {

    log.info("POST - Create Multi Ea Admin Employer screen - Step 2");

    Optional<FinanceUser> user = financeService.getFinanceUserByUuid(uuid);

    List<EmployingAuthorityView> eas = user.map(u -> u.getOrganisations().get(0).getEas())
        .orElse(Collections.emptyList());

    if (bindingResult.hasErrors()) {
      model.addAttribute("eas", eas);
      model.addAttribute(NEW_USER_MAIL, user.map(u -> u.getUsername()).orElse(""));
      return CREATE_EA_VIEW_NAME;
    }

    Optional<String> eaCodeExists = organisationService.getEaByEaCode(ea.getEaCode());
    if(eaCodeExists.isPresent()) {
      model.addAttribute("eas", eas);
      model.addAttribute(NEW_USER_MAIL, user.map(u -> u.getUsername()).orElse(""));
      model.addAttribute("eaExists", true);
      return CREATE_EA_VIEW_NAME;
    }

    if(user.isPresent()) {

      CreateEaRequest eaRequest = CreateEaRequest.builder()
          .eaCode(ea.getEaCode())
          .eaName(ea.getEaName())
          .employerType(ea.getEmployerType())
          .organisationUuid(user.get().getOrganisations().get(0).getIdentifier().toString())
          .build();
      organisationService.registerEa(eaRequest);

      return DISPLAY_EA_REDIRECT + user.get().getUuid();
    }

    httpResponse.sendError(500);
    return null;
  }

  private Optional<AuthenticationResponse> registerAuthenticationUser(AdminUser user) {
    RegistrationRequest registrationRequest = RegistrationRequest.builder()
        .username(user.getEmail().toLowerCase())
        .password(user.getFirstPassword())
        .role(user.getRole().name())
        .build();

    return userRegistrationService.registerAuthenticationUser(registrationRequest);
  }

  private Optional<CreateUserResponse> registerEmployerUser(AdminUser user) {

    Optional<CreateOrganisationResponse> newOrg = organisationService
        .registerOrganisationWithoutEa(user.getOrganisationName());

    if (!newOrg.isPresent()) {
      return Optional.empty();
    }
    List<OrganisationView> organisations = Collections.singletonList(OrganisationView.builder()
        .identifier(UUID.fromString(newOrg.get().getUuid()))
        .name(newOrg.get().getName())
        .build());

    CreateUserRequest createUserRequest = CreateUserRequest.builder()
        .username(user.getEmail().toLowerCase())
        .organisations(organisations)
        .firstName(user.getFirstName())
        .surname(user.getLastName())
        .contactNumber(user.getContactTelephone())
        .uuid(user.getUuid())
        .build();

    return userRegistrationService.createFinanceUser(createUserRequest);
  }




}
