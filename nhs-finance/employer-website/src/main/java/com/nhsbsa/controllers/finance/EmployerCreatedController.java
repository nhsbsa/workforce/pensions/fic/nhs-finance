package com.nhsbsa.controllers.finance;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PreAuthorize("hasRole('ADMIN') or hasRole('MASTER')")
public class EmployerCreatedController {

  private static final String EMPLOYER_CREATED_VIEW_NAME = "employerCreated";

  @PostMapping(value = "/employer-created")
  public ModelAndView showEmployerCreatedPage() {

    return new ModelAndView(EMPLOYER_CREATED_VIEW_NAME);
  }

}
