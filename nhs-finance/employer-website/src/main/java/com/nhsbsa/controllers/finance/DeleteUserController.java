package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.DeleteUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.authentication.AuthorizationService;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN')")
public class DeleteUserController {

  private static final String DELETE_USER_VIEW_NAME = "confirm-delete-user";

  private static final String DELETE_USER_ENDPOINT = "/confirm-delete-user/{uuid}";

  private static final String DELETE_USER_POST_ENDPOINT = "/confirm-delete-user";

  private static final String ACCOUNT_DELETED_ENDPOINT = "account-deleted";

  private static final String USER_NAME_ATTRIBUTE = "userName";

  private final AuthorizationService authorizationService;
  private final FinanceService financeService;

  @Autowired
  public DeleteUserController(AuthorizationService authorizationService, FinanceService financeService) {
    this.authorizationService = authorizationService;
    this.financeService = financeService;
  }

  @GetMapping(value = DELETE_USER_ENDPOINT)
  public ModelAndView showDeleteUserPage(@PathVariable("uuid") final String uuid) {
    log.info("GET - Accessing to Confirm Delete User screen");

    DeleteUser deleteUser = new DeleteUser();
    deleteUser.setUuid(uuid);

    ModelAndView modelAndView = new ModelAndView(DELETE_USER_VIEW_NAME);
    modelAndView.addObject("deleteUser", deleteUser);
    modelAndView.getModelMap().addAttribute(USER_NAME_ATTRIBUTE, financeService.getFinanceUserByUuid(uuid)
        .map(FinanceUser::getFirstName)
        .map(u -> capitalize(u)+"'s")
        .orElse(""));
    return modelAndView;

  }

  @PostMapping(DELETE_USER_POST_ENDPOINT)
  public String postDeleteUserPage(@ModelAttribute @Valid DeleteUser deleteUser,
      final BindingResult bindingResult,
      ModelMap map, HttpServletResponse httpResponse) throws IOException {
    log.info("POST - Confirm Delete User for {}", deleteUser.getUuid());

    String userFirstName = financeService.getFinanceUserByUuid(deleteUser.getUuid())
        .map(FinanceUser::getFirstName)
        .map(u -> capitalize(u) + "'s")
        .orElse("");

    if (bindingResult.hasErrors()) {
      map.addAttribute(USER_NAME_ATTRIBUTE, userFirstName);
      return DELETE_USER_VIEW_NAME;
    }

    if ("Y".equals(deleteUser.getDeleteUserYesNo())) {
      Optional<User> userDeleted = authorizationService.deleteUser(deleteUser.getUuid());

      if (!userDeleted.isPresent()) {
        httpResponse.sendError(500);
        return null;
      }

      map.addAttribute(USER_NAME_ATTRIBUTE, userFirstName);
      return ACCOUNT_DELETED_ENDPOINT;
    }

    return "redirect:/current-users";
  }


  private String capitalize(String s) {
    return WordUtils.capitalizeFully(s, ' ', '-');
  }

}
