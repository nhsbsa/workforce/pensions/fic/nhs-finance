package com.nhsbsa.service.authentication;

import com.nhsbsa.exceptions.LoginAuthenticationException;
import com.nhsbsa.security.LoginRequest;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jeffreya on 24/10/2016.
 *
 */

@Log4j
@Service
public class UserLoginService {


    private final AuthorizationService authorizationService;

    @Autowired
    public UserLoginService(final AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    private void assertCredentialsPresent(String username, String password) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            throw new LoginAuthenticationException();
        }
    }

    private LoginRequest buildAuthenticationRequest(String username, String password) {
        return LoginRequest.builder()
                .username(username)
                .password(password)
                .build();
    }

    public FinanceUserStatusWrapper financeLogin(final String username, final String password) {
        assertCredentialsPresent(username, password);
        final LoginRequest loginRequest = buildAuthenticationRequest(username, password);
        return authorizationService.authenticateAndRetrieveFinanceUser(loginRequest);
    }
}
