package com.nhsbsa.service;

import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@SuppressWarnings("squid:S1075")
public class OrganisationService {

  private static final String GET_ORGANISATION_BY_EACODE_PATH = "/organisation/{eaCode}";

  private static final String REGISTER_ORGANISATION_PATH = "/organisation";

  private static final String REGISTER_ORGANISATION_WITHOUT_EA = "/organisation-no-ea";

  private static final String REGISTER_EA = "/employing-authority";

  private static final String GET_ORGANISATION_BY_NAME = "/organisation-name";

  private static final String GET_EA_BY_EACODE = "/employing-authority/{eaCode}";

  private final RestTemplate ficRestTemplate;
  private final BackendUri getOrganisationByEaCodeUri;
  private final BackendUri registerOrganisationUri;
  private final BackendUri registerOrganisationWithoutEaUri;
  private final BackendUri registerEaUri;
  private final BackendUri getOrganisationByNameUri;
  private final BackendUri getEaByEaCodeUri;

  @Autowired
  public OrganisationService(final BackendApiUriService backendApiUriService,
      final RestTemplate ficRestTemplate) {
    this.ficRestTemplate = ficRestTemplate;
    this.getOrganisationByEaCodeUri = backendApiUriService.path(GET_ORGANISATION_BY_EACODE_PATH);
    this.registerOrganisationUri = backendApiUriService.path(REGISTER_ORGANISATION_PATH);
    this.registerOrganisationWithoutEaUri = backendApiUriService.path(REGISTER_ORGANISATION_WITHOUT_EA);
    this.registerEaUri = backendApiUriService.path(REGISTER_EA);
    this.getOrganisationByNameUri = backendApiUriService.path(GET_ORGANISATION_BY_NAME);
    this.getEaByEaCodeUri = backendApiUriService.path(GET_EA_BY_EACODE);
  }

  public Optional<OrganisationView> getOrganisationByEaCode(final String eaCode) {
    final String uri = getOrganisationByEaCodeUri.params(eaCode);
    return Optional.ofNullable(ficRestTemplate.getForObject(uri, OrganisationView.class));
  }


  public Optional<OrganisationView> registerOrganisation(String eaCode, String accountName, String employerType) {

    Optional<CreateOrganisationRequest> organisationRequest = buildCreateOrganisationRequest(eaCode,
        accountName, employerType);

    if (!organisationRequest.isPresent()) {
      return Optional.empty();
    }

    return Optional
        .ofNullable(ficRestTemplate
            .postForObject(registerOrganisationUri.toUri(), organisationRequest.get(),
                OrganisationView.class));
  }

   Optional<CreateOrganisationRequest> buildCreateOrganisationRequest(String eaCode,
      String accountName, String employerType) {

    if (eaCode == null || accountName == null) {
      return Optional.empty();
    }

    EmployingAuthorityView ea = EmployingAuthorityView.builder().eaCode(eaCode).name(accountName).employerType(employerType).build();
     List<EmployingAuthorityView> employingAuthorities = Collections.singletonList(ea);

    return Optional.of(CreateOrganisationRequest.builder()
        .eas(employingAuthorities)
        .name(accountName)
        .build());
  }

  public Optional<CreateOrganisationResponse> registerOrganisationWithoutEa(String name) {

    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name(name)
        .build();

    return Optional
        .ofNullable(ficRestTemplate
            .postForObject(registerOrganisationWithoutEaUri.toUri(), organisationRequest,
                CreateOrganisationResponse.class));
  }

  public Optional<OrganisationView> registerEa(CreateEaRequest createEaRequest) {

    return Optional
        .ofNullable(ficRestTemplate
            .postForObject(registerEaUri.toUri(), createEaRequest,
                OrganisationView.class));
  }

  public Optional<String> getOrganisationByName(String orgName) {
    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .build();

    return Optional
        .ofNullable(ficRestTemplate
            .postForObject(getOrganisationByNameUri.toUri(), organisationRequest,
                String.class));
  }

  public Optional<String> getEaByEaCode(String eaCode) {
    final String uri = getEaByEaCodeUri.params(eaCode);
    return Optional
        .ofNullable(ficRestTemplate
            .getForObject(uri, String.class));
  }


}
