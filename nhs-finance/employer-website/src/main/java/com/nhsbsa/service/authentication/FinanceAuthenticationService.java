package com.nhsbsa.service.authentication;

import com.nhsbsa.exceptions.LoginAuthenticationException;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 * Created by jeffreya on 19/08/2016.
 * FinanceAuthenticationService
 */

@Log4j
@Service
public class FinanceAuthenticationService {

    private final UserLoginService userLoginService;

    @Autowired
    public FinanceAuthenticationService(final UserLoginService userLoginService) {
        this.userLoginService = userLoginService;
    }

    public Authentication authenticate(final String name, final String password) {
        log.debug("Login request started for user: " + name);
        try {

            final FinanceUserStatusWrapper wrapper = userLoginService.financeLogin(name, password);
            switch (wrapper.getStatus()){

                case ACCOUNT_LOCKED:
                    throw new LockedException("Account locked for user "+name);

                case AUTH_FAILURE:
                    throw new LoginAuthenticationException();

                case AUTH_SUCCESS:
                    final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(name, password, wrapper.getFinanceUser().getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(wrapper.getFinanceUser());
                    return usernamePasswordAuthenticationToken;

                default:
                    throw new LoginAuthenticationException();
            }

        } finally {
            log.debug("Login request ended for user: " + name);
        }
    }
}
