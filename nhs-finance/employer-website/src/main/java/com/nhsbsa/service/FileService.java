package com.nhsbsa.service;

import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class FileService {

  @SuppressWarnings("squid:S1075")
  private static final String GET_FILE_LIST = "/finance/file";

  @SuppressWarnings("squid:S1075")
  private static final String GET_FILE_BY_DATE = "/finance/file/{date}";

  private final RestTemplate ficRestTemplate;
  private final BackendUri getFileListUri;
  private final BackendUri getFileByDateUri;

  @Autowired
  public FileService(final BackendApiUriService backendApiUriService,
      final RestTemplate ficRestTemplate) {
    this.ficRestTemplate = ficRestTemplate;
    this.getFileListUri = backendApiUriService.path(GET_FILE_LIST);
    this.getFileByDateUri = backendApiUriService.path(GET_FILE_BY_DATE);
  }

  public Optional<String[]> getFiles() {

    try {

      return Optional.of(ficRestTemplate.getForObject(getFileListUri.toUri(), String[].class));

    } catch (HttpClientErrorException e) {
      log.error("Could not get file list");
    }
    return Optional.empty();
  }


  public Optional<byte[]> getFile(String fileName) {

    try {

      return Optional
          .ofNullable(ficRestTemplate.getForObject(getFileByDateUri.params(fileName), byte[].class));

    } catch (HttpClientErrorException e) {
      log.error("Could not get file");
    }
    return Optional.empty();
  }
}
