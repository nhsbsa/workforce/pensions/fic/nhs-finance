package com.nhsbsa.interceptors;

import com.nhsbsa.model.FinanceUser;
import java.io.IOException;
import java.util.Optional;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
@Log
public class UserPropagationInterceptor implements ClientHttpRequestInterceptor {

  @Override
  public ClientHttpResponse intercept(
      HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
      throws IOException {

    if(! request.getMethod().matches(HttpMethod.GET.name()) ) {
      Optional.ofNullable(SecurityContextHolder.getContext())
          .map(SecurityContext::getAuthentication)
          .map(Authentication::getDetails)
          .filter(FinanceUser.class::isInstance)
          .map(FinanceUser.class::cast)
          .ifPresent(user -> {
            log.debug("Setting PROPAGATED_UUID with uuid: " + user.getUuid());
            request.getHeaders().add("PROPAGATED_UUID", user.getUuid());
          });
    }
    
    return execution.execute(request, body);
  }

}