package com.nhsbsa.security;

import com.nhsbsa.view.EmployingAuthorityView;
import java.util.List;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrganisationRequest {

  private List<EmployingAuthorityView> eas;

  @NotBlank
  @Size(max = 1000)
  private String name;
}

