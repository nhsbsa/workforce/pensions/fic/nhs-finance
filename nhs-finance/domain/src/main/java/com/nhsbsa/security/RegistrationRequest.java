package com.nhsbsa.security;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jeffreya on 19/08/2016.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String role;
}
