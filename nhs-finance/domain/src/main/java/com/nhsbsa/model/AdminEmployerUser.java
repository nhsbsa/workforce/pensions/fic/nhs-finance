package com.nhsbsa.model;

import com.nhsbsa.model.validation.AdminEmployerValidationGroup;
import com.nhsbsa.model.validation.EACodeFormat;
import com.nhsbsa.model.validation.FullNameFormat;
import com.nhsbsa.model.validation.PhoneNumberFormat;
import com.nhsbsa.view.OrganisationView;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class AdminEmployerUser extends EmployerUser {

  public static final UserRoles role = UserRoles.ROLE_ADMIN;

  private static final String SPLIT_REGEX = " ";

  @FullNameFormat (groups = AdminEmployerValidationGroup.class)
  private String fullName;

  @PhoneNumberFormat (groups = AdminEmployerValidationGroup.class)
  private String contactTelephone;

  @NotEmpty (message="{accountNameFormat.NotValid}", groups = AdminEmployerValidationGroup.class)
  private String accountName;

  @NotEmpty (message="{employerType.NotValid}", groups = AdminEmployerValidationGroup.class)
  private String employerType;

  @EACodeFormat (groups = AdminEmployerValidationGroup.class)
  private String eaCode;

  private List<OrganisationView> organisations;

  @Builder
  public AdminEmployerUser(String email, String firstPassword) {
    this.setEmail(email);
    this.setFirstPassword(firstPassword);
  }

  @Override
  public UserRoles getRole() {
    return role;
  }

  @Override
  public String getFirstName() {
    return fullName.split(SPLIT_REGEX)[0];
  }

  @Override
  public String getLastName() {

    String[] names = fullName.split(SPLIT_REGEX);

    int lastNameIndex = names.length -1;

    return names[lastNameIndex];
  }


}

