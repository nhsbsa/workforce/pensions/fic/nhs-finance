package com.nhsbsa.model;

import java.io.Serializable;
import java.time.YearMonth;
import java.util.Optional;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ContributionDate implements Serializable{

  @Convert(converter = MonthConverter.class)
  private String contributionMonth;

  private Integer contributionYear;
    
  public Optional<YearMonth> toYearMonth(){
    
    MonthNum monthNum = new MonthNum();
    try {
      return Optional.of(YearMonth.of(contributionYear, monthNum.getMonthNumFromName(contributionMonth)));
    } catch (Exception e) {
      return Optional.empty();
    }
  }
}
