package com.nhsbsa.model.validation;

import com.nhsbsa.utility.TransferFormDateFormat;
import com.nhsbsa.view.RequestForTransferView;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

public class IsNotExcludedDateValidator implements ConstraintValidator<IsNotExcludedDate, RequestForTransferView> {

	@Value("#{'${paymentdate.excluded.dates}'.split(',')}")
	private List<String> excludedDates;

	private String bankHoliday;

	private TransferFormDateFormat transferFormDateFormat = new TransferFormDateFormat();

	@Override
	public final void initialize(final IsNotExcludedDate annotation) {
		bankHoliday = annotation.message();
	}

	@Override
	public final boolean isValid(final RequestForTransferView rft, final ConstraintValidatorContext constraintValidatorContext) {

		if ("Y".equals(rft.getPayAsSoonAsPossible())
				|| rft.getTransferDate().getDate() == null
				|| excludedDates == null
				|| excludedDates.isEmpty()) {
			return true;
		}

		if(!transferFormDateFormat.isValidDate(rft.getTransferDate())) {
			return true;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");

		if(excludedDates.contains(sdf.format(rft.getTransferDate().getDate()))) {
			constraintValidatorContext.disableDefaultConstraintViolation();
			constraintValidatorContext.buildConstraintViolationWithTemplate(bankHoliday)
					.addPropertyNode("transferDate")
					.addConstraintViolation();
			return false;
		}

		return true;
	}

}