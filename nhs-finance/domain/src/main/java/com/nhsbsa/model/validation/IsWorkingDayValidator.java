package com.nhsbsa.model.validation;

import com.nhsbsa.utility.TransferFormDateFormat;
import com.nhsbsa.view.RequestForTransferView;
import java.time.DayOfWeek;
import java.time.ZoneId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsWorkingDayValidator implements ConstraintValidator<IsWorkingDay, RequestForTransferView> {

	private String workingDay;

	private TransferFormDateFormat transferFormDateFormat = new TransferFormDateFormat();

	@Override
	public final void initialize(final IsWorkingDay annotation) {
		workingDay = annotation.message();
	}

	@Override
	public boolean isValid(RequestForTransferView rft, ConstraintValidatorContext context) {

		if ("Y".equals(rft.getPayAsSoonAsPossible()) || rft.getTransferDate().getDate() == null
				|| !transferFormDateFormat.isValidDate(rft.getTransferDate())) {
			return true;
		}

		DayOfWeek dayOfWeek = rft.getTransferDate().getDate().toInstant().atZone(ZoneId.systemDefault())
				.toLocalDate().getDayOfWeek();

		if (DayOfWeek.SATURDAY.equals(dayOfWeek) || DayOfWeek.SUNDAY.equals(dayOfWeek)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(workingDay)
					.addPropertyNode("transferDate")
					.addConstraintViolation();
			return false;
		}

		return true;

	}

}