package com.nhsbsa.model.validation;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nataliehulse on 29/09/2017.
 */

public class PasswordFormatValidator implements ConstraintValidator<ValidatePassword, String> {

    private final Pattern regPwPattern;
    private static final String REGEXP_PW_VALIDATION = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{12,})";

    public PasswordFormatValidator() {

        regPwPattern = Pattern.compile(REGEXP_PW_VALIDATION);
    }

    @Override
    public final void initialize(final ValidatePassword password) {
        //nothing needed from annotation
    }

    @Override
    public final boolean isValid(final String password, final ConstraintValidatorContext context) {

        return StringUtils.isNotBlank(password) && isValidCharacters(password);

    }

    /*
     * Checks for valid Password
     * If all ok return true
     * Invalid pattern return false
     */
    private boolean isValidCharacters(String initialPassword) {

        Matcher matcher = regPwPattern.matcher(initialPassword);
        return matcher.matches();
    }

}


