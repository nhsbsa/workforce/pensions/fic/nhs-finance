package com.nhsbsa.model;

import com.nhsbsa.model.validation.EmailFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nataliehulse on 23/11/2017.
 */

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ForgottenPassword {

    @EmailFormat
    private String email;

    private boolean createEmailToken = true;

    public String getEmail() {
        if (email == null) {
            return "";
        }
        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

}

