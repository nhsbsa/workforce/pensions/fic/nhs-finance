package com.nhsbsa.model.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

public class FullNameFormatValidator implements ConstraintValidator<FullNameFormat, String> {

  private final Pattern regFullNamePattern;
  private static final String REGEXP_FULLNAME_VALIDATION = "^[a-zA-Z '-]+$";

  public FullNameFormatValidator() {

    regFullNamePattern = Pattern.compile(REGEXP_FULLNAME_VALIDATION);
  }

  @Override
  public void initialize(FullNameFormat validateFullName) {
    //nothing needed from annotation
  }

  @Override
  public final boolean isValid(final String fullName, final ConstraintValidatorContext context) {

    return StringUtils.isNotBlank(fullName) && areValidCharacters(fullName) && isValidFormat(fullName);

  }

  /*
   * Checks for valid characters
   * If all ok return true
   * Invalid pattern return false
   */
  private boolean areValidCharacters(String fullName) {

    Matcher matcher = regFullNamePattern.matcher(fullName);
    return matcher.matches();
  }

  private boolean isValidFormat(String fullName) {



   return fullName.length() <= 200 && fullName.split(" ").length > 1;

  }

}


