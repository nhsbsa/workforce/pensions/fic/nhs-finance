package com.nhsbsa.model.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by nataliehulse on 29/09/2017.
 */

public class EACodeFormatValidator implements ConstraintValidator<EACodeFormat, String> {

  private final Pattern regEACodePattern;
  private static final String REGEXP_EACODE_VALIDATION = "(((EA)([0-9]){4})|((GP)[A-Z]([0-9]){3}))"; //Example of valid formats: EA1234 or GPA123

  public EACodeFormatValidator() {

    regEACodePattern = Pattern.compile(REGEXP_EACODE_VALIDATION);
  }

  @Override
  public void initialize(EACodeFormat validateEACode) {
    //nothing needed from annotation
  }

  @Override
  public final boolean isValid(final String eaCode, final ConstraintValidatorContext context) {

    return StringUtils.isNotBlank(eaCode) && isValidFormat(eaCode);
  }

  /*
   * Checks for valid Password
   * If all ok return true
   * Invalid pattern return false
   */
  private boolean isValidFormat(String eaCode) {

    Matcher matcher = regEACodePattern.matcher(eaCode);
    return matcher.matches();
  }

}


