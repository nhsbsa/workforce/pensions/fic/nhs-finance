package com.nhsbsa.model;

import com.nhsbsa.model.validation.EACodeFormat;
import com.nhsbsa.view.OrganisationView;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployingAuthority {

  @NotEmpty (message="{eaNameFormat.notValid}")
  private String eaName;

  @NotEmpty (message="{employerType.NotValid}")
  private String employerType;

  @EACodeFormat
  private String eaCode;

  private List<OrganisationView> organisations;


}

