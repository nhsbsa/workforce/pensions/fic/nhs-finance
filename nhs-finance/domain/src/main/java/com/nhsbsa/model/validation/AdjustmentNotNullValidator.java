package com.nhsbsa.model.validation;

import com.nhsbsa.model.Adjustment;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Nat Hulse 01/12/2016
 */
public class AdjustmentNotNullValidator implements
    ConstraintValidator<AdjustmentNotNull, Adjustment> {

  @Override
  public void initialize(AdjustmentNotNull constraint) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public final boolean isValid(final Adjustment adjustment,
      final ConstraintValidatorContext context) {

    // If All of the adjustment values are null return Validation message
    return adjustment.getEmployeeContributions() != null
        || adjustment.getEmployerContributions() != null
        || adjustment.getEmployeeAddedYears() != null
        || adjustment.getAdditionalPension() != null
        || adjustment.getErrbo() != null;
  }
}