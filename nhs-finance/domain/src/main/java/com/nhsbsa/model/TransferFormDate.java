package com.nhsbsa.model;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TransferFormDate extends FormDate implements Serializable {

  @Builder
  private TransferFormDate(final String days, final String month, final String year) {
    super(days, month, year);
  }

}