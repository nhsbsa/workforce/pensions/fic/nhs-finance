package com.nhsbsa.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by jeffreya on 23/09/2016.
 * AuthorizedApplication
 */

@Builder
@Data
@Entity
@Table(name = "application")
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
public class AuthorizedApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "application_id", insertable = false, updatable = false)
    private Long id;

    private String name;
    private String hash;
}
