package com.nhsbsa.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MultiEa {

  private String eaCode;

  private String name;

  private String lastSubmitted;

}