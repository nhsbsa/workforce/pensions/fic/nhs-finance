package com.nhsbsa.model;

import com.nhsbsa.model.validation.NameFormat;
import com.nhsbsa.view.OrganisationView;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
public class StandardEmployerUser extends EmployerUser{

  private static final String DASH_VALUE = "-"; //Default value for FirstName, LastName and ContactTelephone

  public static final UserRoles role =  UserRoles.ROLE_STANDARD;

  @NotBlank(message = "{admin.firstName.notBlank}")
  @NameFormat
  private String firstName;

  @NameFormat(message = "{lastNameFormat.NotValid}")
  private String lastName;

  private String eaCode;

  private List<OrganisationView> organisations;

  @Builder
  public StandardEmployerUser(String firstName, String lastName, String email, String firstPassword){
    this.firstName = firstName;
    this.lastName = lastName;
    this.setEmail(email);
    this.setFirstPassword(firstPassword);
  }

  @Override
  public String getFirstName() {
    return firstName;
  }

  @Override
  public String getLastName() {
    return lastName;
  }

  @Override
  public String getEmployerType() {
    return EmployerTypes.STAFF.name();
  }

  @Override
  public String getContactTelephone() {
    return getDefaultValue() ;
  }

  @Override
  public UserRoles getRole() {
    return role;
  }

  public String getDefaultValue() {
    return DASH_VALUE;
  }

}
