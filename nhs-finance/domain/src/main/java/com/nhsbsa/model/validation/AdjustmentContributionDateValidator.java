package com.nhsbsa.model.validation;

import java.time.YearMonth;
import java.util.Optional;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.assertj.core.util.Strings;
import com.nhsbsa.model.ContributionDate;

public class AdjustmentContributionDateValidator implements
    ConstraintValidator<AdjustmentContributionDateValid, ContributionDate> {

  private String adjustmentContributionDateNotBlank = "{adjustmentContributionDate.not.blank}";

  private String adjustmentContributionDateNotValid = "{adjustmentContributionDate.not.valid}";

  private String adjustmentContributionDateNotInRange = "{adjustmentContributionDate.not.in.range}";

  @Override
  public void initialize(AdjustmentContributionDateValid constraintAnnotation) {
    //Nothing to initialize
  }

  @Override
  public boolean isValid(ContributionDate adjustmentContributionDate,
      ConstraintValidatorContext context) {

    if (containsNullValue(adjustmentContributionDate)) {
      addConstraintViolation(context, adjustmentContributionDateNotBlank);
      return false;
    }

    Optional<YearMonth> maybeYearMonth = adjustmentContributionDate.toYearMonth();
    
    if(!maybeYearMonth.isPresent() || !isValidYear(maybeYearMonth.get().getYear())) {
      addConstraintViolation(context, adjustmentContributionDateNotValid);
      return false;
    }

    if (!maybeYearMonth.get().isBefore(getNow())) {
      addConstraintViolation(context, adjustmentContributionDateNotInRange);
      return false;
    }

    return true;
  }
  
  private boolean containsNullValue(final ContributionDate adjustmentContributionDate) {
    return adjustmentContributionDate == null
        || Strings.isNullOrEmpty(adjustmentContributionDate.getContributionMonth())
        || adjustmentContributionDate.getContributionYear() == null;
  }
  
  private boolean isValidYear(final Integer year) {
    return year >= 2001;
  }

  private void addConstraintViolation(ConstraintValidatorContext context,
      String validationErrorMessage) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(validationErrorMessage).addConstraintViolation();
  }
  
  public YearMonth getNow() {
    return YearMonth.now();
  } 
}
