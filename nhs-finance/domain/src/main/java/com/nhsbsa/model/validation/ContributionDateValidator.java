package com.nhsbsa.model.validation;

import com.nhsbsa.model.ContributionDate;
import java.time.YearMonth;
import java.util.Optional;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.assertj.core.util.Strings;

public class ContributionDateValidator implements
    ConstraintValidator<ContributionDateValid, ContributionDate> {
  
  private int monthsInAdvanceLimit;

  private String contributionDateNotBlank = "{contributionDate.not.blank}";

  private String contributionDateNotValid = "{contributionDate.not.valid}";

  private String contributionDateNotInRange = "{contributionDate.not.in.range}";

  @Override
  public void initialize(ContributionDateValid constraintAnnotation) {
    monthsInAdvanceLimit = constraintAnnotation.monthsInAdvanceLimit();
  }

  @Override
  public boolean isValid(ContributionDate contributionDate, ConstraintValidatorContext context) {
    
    if (containsNullOrEmptyValue(contributionDate)) {
      addConstraintViolation(context, contributionDateNotBlank);
      return false;
    }
    
    Optional<YearMonth> maybeYearMonth = contributionDate.toYearMonth();
    
    if(!maybeYearMonth.isPresent() || !isValidYear(maybeYearMonth.get().getYear())) {
      addConstraintViolation(context, contributionDateNotValid);
      return false;
    }
    
    final YearMonth advanceLimitDate = getNow().plusMonths(monthsInAdvanceLimit+1l);
    
    if (!maybeYearMonth.get().isBefore(advanceLimitDate)) {
      addConstraintViolation(context, contributionDateNotInRange);
      return false;
    }
    
    return true;
  }

  private boolean isValidYear(final Integer year) {
    return year >= 2001;
  }

  private boolean containsNullOrEmptyValue(final ContributionDate contributionDate) {
    return contributionDate == null || Strings.isNullOrEmpty(contributionDate.getContributionMonth())
        || contributionDate.getContributionYear() == null;
  }
  
  private void addConstraintViolation(ConstraintValidatorContext context,
      String validationErrorMessage) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(validationErrorMessage).addConstraintViolation();
  }

  public YearMonth getNow() {
    return YearMonth.now();
  }
}
