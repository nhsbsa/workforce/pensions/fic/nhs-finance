package com.nhsbsa.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Mark Lishman on 07/11/2016.
 */

@Converter
public class TransferFormDateConverter implements AttributeConverter<TransferFormDate, Date> {

  @Override
  public Date convertToDatabaseColumn(TransferFormDate formDate) {
    return formDate.getDate();
  }

  @Override
  public TransferFormDate convertToEntityAttribute(Date date) {
    final TransferFormDate formDate = TransferFormDate.builder().build();
    if (date == null) {
      return formDate;
    }
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    formDate.setDays(String.valueOf(calendar.get(Calendar.DATE)));
    formDate.setMonth(new SimpleDateFormat("MMMM").format(calendar.getTime()));
    formDate.setYear(String.valueOf(calendar.get(Calendar.YEAR)));
    return formDate;
  }
}
