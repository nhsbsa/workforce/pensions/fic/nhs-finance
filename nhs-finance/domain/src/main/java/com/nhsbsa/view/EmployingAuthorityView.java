package com.nhsbsa.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import javax.validation.constraints.Size;
import lombok.Builder;
import org.hibernate.validator.constraints.NotBlank;

@lombok.Value
@Builder(builderClassName = "EmployingAuthorityBuilder")
public final class EmployingAuthorityView {


    @ApiModelProperty(required = true, notes = "Ea Code.")
    @NotBlank
    @Size(max = 50)

    private final String eaCode;

    @ApiModelProperty(required = true, notes = "Ea's name.")
    @Size(max = 1000)
    private final String name;

    @ApiModelProperty(required = true, notes = "Employer Type.")
    @NotBlank
    private final String employerType;

    @JsonCreator
    public static EmployingAuthorityView jsonFactory(@JsonProperty("eaCode") String eaCode,
                                        @JsonProperty("eaName") String name,
                                        @JsonProperty("employerType") String employerType) {
        return EmployingAuthorityView.builder()
                .eaCode(eaCode)
                .name(name)
                .employerType(employerType)
                .build();
    }

    /**
     * Return a new immutable {@code UserView} instance with the identifier
     * provided.
     *
     * @param identifier
     * @return
     */
    public EmployingAuthorityView withIdentifier(UUID identifier) {
        return EmployingAuthorityView.builder()
                .eaCode(this.getEaCode())
                .name(this.getName())
                .employerType(this.getEmployerType())
                .build();
    }
}
