package com.nhsbsa.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.model.validation.AdjustmentDateValidator;
import com.nhsbsa.model.validation.ContributionDateValid;
import com.nhsbsa.model.validation.ContributionsValidationGroup;
import com.nhsbsa.model.validation.DateIsAfterTimeSensitiveDays;
import com.nhsbsa.model.validation.DateLessThan31DaysFromToday;
import com.nhsbsa.model.validation.EmployeeContributionThreshold;
import com.nhsbsa.model.validation.EmployerContributionThreshold;
import com.nhsbsa.model.validation.FirstSchedulePaymentValidationGroup;
import com.nhsbsa.model.validation.IsNotExcludedDate;
import com.nhsbsa.model.validation.IsWorkingDay;
import com.nhsbsa.model.validation.SchedulePaymentValidationGroup;
import com.nhsbsa.model.validation.SecondSchedulePaymentValidationGroup;
import com.nhsbsa.model.validation.TotalDebitAmountValidator;
import com.nhsbsa.model.validation.TransferFormDateNotBlank;
import com.nhsbsa.model.validation.ValidFormDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor // NOSONAR
@EqualsAndHashCode
@TransferFormDateNotBlank(groups = FirstSchedulePaymentValidationGroup.class)
@ValidFormDate(groups = SecondSchedulePaymentValidationGroup.class)
@IsWorkingDay(groups = SchedulePaymentValidationGroup.class)
@DateLessThan31DaysFromToday(groups = SchedulePaymentValidationGroup.class)
@IsNotExcludedDate(groups = SchedulePaymentValidationGroup.class)
@DateIsAfterTimeSensitiveDays(groups = SchedulePaymentValidationGroup.class)
@EmployeeContributionThreshold(groups = ContributionsValidationGroup.class)
@EmployerContributionThreshold(groups = ContributionsValidationGroup.class)
public class RequestForTransferView {

  private Long id;

  private TransferFormDate transferDate;

  private Boolean isGp = false;

  @ContributionDateValid(groups = {FirstSchedulePaymentValidationGroup.class})
  private ContributionDate contributionDate = ContributionDate.builder().build();

  @DecimalMin(value = "1.00", message = "{totalPensionablePay.invalid}")
  @DecimalMax(value = "99999999.99", message = "{totalPensionablePay.invalid}")
  @Digits(integer=10, fraction=2, message = "{totalPensionablePay.invalid}")
  @NotNull(message = "{totalPensionablePay.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal totalPensionablePay;

  @DecimalMin(value = "1.00", message = "{employeeContributions.invalid}")
  @DecimalMax(value = "99999999.99", message = "{employeeContributions.invalid}")
  @Digits(integer=10, fraction=2, message = "{employeeContributions.invalid}")
  @NotNull(message = "{employeeContributions.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal employeeContributions;

  @DecimalMin(value = "1.00", message = "{employerContributions.invalid}")
  @DecimalMax(value = "99999999.99", message = "{employerContributions.invalid}")
  @Digits(integer=10, fraction=2, message = "{employerContributions.invalid}")
  @NotNull(message = "{employerContributions.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal employerContributions;

  @DecimalMin(value = "0.00", message = "{employeeAddedYears.invalid}")
  @DecimalMax(value = "99999999.99", message = "{employeeAddedYears.invalid}")
  @Digits(integer=10, fraction=2, message = "{employeeAddedYears.invalid}")
  private BigDecimal employeeAddedYears;

  @DecimalMin(value = "0.00", message = "{additionalPension.invalid}")
  @DecimalMax(value = "99999999.99", message = "{additionalPension.invalid}")
  @Digits(integer=10, fraction=2, message = "{additionalPension.invalid}")
  private BigDecimal additionalPension;

  @DecimalMin(value = "0.00", message = "{errbo.invalid}")
  @DecimalMax(value = "99999999.99", message = "{errbo.invalid}")
  @Digits(integer=10, fraction=2, message = "{errbo.invalid}")
  private BigDecimal errbo;

  @NotNull(message = "{adjustmentsRequired.notNull}", groups = ContributionsValidationGroup.class)
  private Boolean adjustmentsRequired = false;

  @NotNull(message = "{totalDebitAmount.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal totalDebitAmount;

  /**
   * The date the record was created (the time the user started populating request data)
   */
  private Date receiveDate = new Date();

  private Date submitDate;
  /**
   * The date that the RequestForTransfer entity is received by the finance-facade for inclusion in
   * a csv file for processing. Once this value is non-null, the RFT will no longer be considered
   * for processing by the finance-facade.
   */
  private Date csvProcessedDate;

  private String eaCode;

  private String rftUuid;

  /**
   * Keep track of the page that needs to be shown next, for cases where the normal flow is
   * circumvented, such as when the 'change' navigation on the summary page is used.
   */
  private String forwardTo;

  @NotBlank(message = "{schedulePayment.asap.notBlank}", groups = FirstSchedulePaymentValidationGroup.class)
  private String payAsSoonAsPossible;

  // Where any adjustments are stored, table (see class definition) is "adjustment" and linked on rft_id
  @Valid()
  private List<Adjustment> adjustmentList;

  private Date createdDate;

  private Date modifiedDate;

  private String createdBy;

  private String modifiedBy;

  // Validation constraint implemented at the property level rather than class level
  // so that the error message can be associated with 'Total amount to be debited' field.
  @JsonIgnore
  @AssertTrue(message = "{totalDebitAmount.notMatched}", groups = ContributionsValidationGroup.class)
  public boolean isTotalDebitAmountValid() {
    return TotalDebitAmountValidator.isTotalDebitAmountValid(this);
  }


  @JsonIgnore
  @AssertTrue(message = "{adjustmentDate.invalid}", groups = {ContributionsValidationGroup.class})
  public boolean isAdjustmentDateBeforeContributionDate() {
    return AdjustmentDateValidator.isAdjustmentDateBeforeContributionDate(getAdjustment(), getContributionDate());
  }

  @JsonIgnore
  @AssertTrue(message = "{totalDebitAmount.isPositive}", groups = ContributionsValidationGroup.class)
  public boolean isTotalDebitAmountPositive() {
    return TotalDebitAmountValidator.isTotalDebitAmountPositive(this);
  }

  public Adjustment getAdjustment() {
    if (adjustmentList == null || adjustmentList.isEmpty()) {
      return null;
    }
    return adjustmentList.get(0);
  }

  public void setAdjustment(final Adjustment adjustment) {
    removeAdjustment();
    if (adjustment != null) {
      if (this.adjustmentList == null) {
        adjustmentList = new ArrayList<>();
      }
      adjustmentList.add(adjustment);
    }
  }

  public void removeAdjustment() {
    if (this.adjustmentList != null) {
      this.adjustmentList.clear();
    }
  }
}