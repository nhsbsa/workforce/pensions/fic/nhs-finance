package com.nhsbsa.view;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.MonthNum;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.view.TransferView.TransferViewBuilder;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Converter;

@Converter
public final class TransferViewConverter{

  private TransferViewConverter(){}

  public static TransferView of(RequestForTransferView source) {
    MonthNum monthNum = new MonthNum();

    Date transferDate = source.getTransferDate().getDate();
    LocalDate ld = new java.sql.Date(transferDate.getTime()).toLocalDate();

    ContributionDate contDate = source.getContributionDate();
    int month = monthNum.getMonthNumFromName(contDate.getContributionMonth());
    int year = contDate.getContributionYear() % 100;
    String contributionYearMonth = String.format("%02d%02d", year, month);

    TransferViewBuilder tvBuilder = TransferView.builder()
        .accountCode(source.getEaCode())
        .transactionDate(ld)
        .contributionYearMonth(contributionYearMonth)
        .contributions(source.getEmployeeContributions())
        .employerContributions(source.getEmployerContributions())
        .addedYears(source.getEmployeeAddedYears())
        .additionalPension(source.getAdditionalPension())
        .errbo(source.getErrbo())
        .employerType((source.getIsGp()?"G":"S"));

    Adjustment adjustment = source.getAdjustment();
    if(adjustment != null){
      ContributionDate adjustmentDate = adjustment.getAdjustmentContributionDate();
      month = monthNum.getMonthNumFromName(adjustmentDate.getContributionMonth());
      year = adjustmentDate.getContributionYear() % 100;
      tvBuilder = tvBuilder.adjustmentYearMonth(String.format("%02d%02d", year, month))
          .adjustmentContributions(adjustment.getEmployeeContributions())
          .adjustmentEmployerContributions(adjustment.getEmployerContributions())
          .adjustmentAddedYears(adjustment.getEmployeeAddedYears())
          .adjustmentAdditionalPension(adjustment.getAdditionalPension())
          .adjustmentErrbo(adjustment.getErrbo());
    }

    return tvBuilder.build();
  }
}
