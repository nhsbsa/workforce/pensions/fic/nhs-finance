package com.nhsbsa.audit;

import com.nhsbsa.model.FinanceUser;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class FinanceAuditorProvider implements AuditorAware<String>{

  @Override
  public String getCurrentAuditor() {
    return Optional.ofNullable(SecurityContextHolder.getContext())
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getDetails)
        .filter(FinanceUser.class::isInstance)
        .map(FinanceUser.class::cast)
        .map(FinanceUser::getUuid)
        .orElse("NO_AUTH_CONTEXT_FOUND");
  }
}
