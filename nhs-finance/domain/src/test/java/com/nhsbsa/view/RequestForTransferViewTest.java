package com.nhsbsa.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.validation.FirstSchedulePaymentValidationGroup;
import com.nhsbsa.model.validation.SchedulePaymentValidationSequence;
import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.model.validation.ContributionsValidationGroup;
import com.nhsbsa.model.validation.SchedulePaymentValidationGroup;

public class RequestForTransferViewTest {

  private static Validator validator;
  private RequestForTransferView.RequestForTransferViewBuilder requestForTransferViewBuilder;

  private static final DateTimeFormatter DAY_FORMAT = DateTimeFormatter.ofPattern("d");
  private static final DateTimeFormatter MONTH_FORMAT = DateTimeFormatter.ofPattern("MMMM");
  private static final DateTimeFormatter YEAR_FORMAT = DateTimeFormatter.ofPattern("yyyy");

  @BeforeClass
  public static void setUpValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Before
  public void before() {

    requestForTransferViewBuilder = RequestForTransferView
        .builder()
        .transferDate(tomorrowValidDate())
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20"))
        .contributionDate(ContributionDate.builder().contributionMonth("September").contributionYear(2002).build())
        .adjustmentsRequired(false)
        .totalDebitAmount(new BigDecimal("30"))
        .payAsSoonAsPossible("Y");

  }

  @Test
  public void allDataIsValid() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);

    assertThat(constraintViolations, hasSize(0));
  }

  @Test
  public void invalidTransferDateValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .payAsSoonAsPossible("N")
        .transferDate(
            TransferFormDate
                .builder()
                .days("111")
                .month("12")
                .year("2016")
                .build()
        )
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationSequence.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{validation.validFormDate}"))));
  }

  @Test
  public void invalidTransferDateFormatValidationError() {

    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .payAsSoonAsPossible("N")
        .transferDate(TransferFormDate.builder()
            .days("15")
            .month("May")
            .year("18")
            .build())
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationSequence.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{validation.validFormDate}"))));
  }

  @Test
  public void totalPensionablePayNotEnteredValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(null)
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.notNull}"))));
  }

  @Test
  public void employeeContributionsNotEnteredValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeContributions(null)
        .totalDebitAmount(new BigDecimal("20"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employeeContributions.notNull}"))));
  }

  @Test
  public void employerContributionsNotEnteredValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employerContributions(null)
        .totalDebitAmount(new BigDecimal("10"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employerContributions.notNull}"))));
  }

  @Test
  public void employeeContributionBelowThresholdValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("4.99"))
        .totalDebitAmount(new BigDecimal("24.99"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employee.contribution.threshold}"))));
  }

  @Test
  public void employeeContributionAboveThresholdValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("14.51"))
        .totalDebitAmount(new BigDecimal("34.51"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employee.contribution.threshold}"))));
  }

  @Test
  public void employerContributionBelowThresholdValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("100"))
        .employerContributions(new BigDecimal("13.99"))
        .totalDebitAmount(new BigDecimal("23.99"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()
        ))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employer.contribution.threshold}"))));
  }

  // Total pensionable pay/Employee contributions/Employer contributions/Employee added years/Additional pension/ERRBO are the same style
  // as use the @Digit, @Min and @Max annotation validations, so just used one for this testing, a few examples below.
  @Test
  public void totalPensionablePayBelowMinimumValueError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("0.99"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }
  
  @Test
  public void totalPensionablePayAtMaximumValueNoError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("99999999.99"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));

  }

  @Test
  public void totalPensionablePayAboveMaximumValueError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("100000000"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }
  
  @Test
  public void totalPensionablePayInvalidDecimalValueError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("99.991"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }

  @Test
  public void totalPensionablePayNegativeValueError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("-3.99"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }

  @Test
  public void totalPensionablePayValidAmountValue() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalPensionablePay(new BigDecimal("12.99"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));
  }

  @Test
  public void getAdjustmentReturnsNullIfListIsNull() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .build();

    Adjustment actualAdjustment = requestForTransfer.getAdjustment();

    assertThat(actualAdjustment, is(nullValue()));
  }

  @Test
  public void getAdjustmentReturnsNullIfListIsEmpty() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .adjustmentList(Collections.EMPTY_LIST)
        .build();

    Adjustment actualAdjustment = requestForTransfer.getAdjustment();

    assertThat(actualAdjustment, is(nullValue()));
  }

  @Test
  public void getAdjustmentReturnsFirstItem() {
    final Adjustment expectedAdjustment = Adjustment.builder().build();
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .adjustmentList(Collections.singletonList(expectedAdjustment))
        .build();

    Adjustment actualAdjustment = requestForTransfer.getAdjustment();

    assertThat(actualAdjustment, is(sameInstance(expectedAdjustment)));
  }

  @Test
  public void setAdjustmentWithNullRemovesList() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .build();

    requestForTransfer.setAdjustment(null);

    assertThat(requestForTransfer.getAdjustmentList(), is(nullValue()));
  }

  @Test
  public void setAdjustmentOverwritesListWithValue() {
    final Adjustment adjustment = Adjustment.builder().build();
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .build();

    requestForTransfer.setAdjustment(adjustment);

    assertThat(requestForTransfer.getAdjustmentList(), hasSize(1));
    assertThat(requestForTransfer.getAdjustmentList().get(0), is(sameInstance(adjustment)));
  }
  
	@Test
	public void adjustmentsRequiredShouldDefaultToFalseWhenNotSpecified() {
		/*Use of the lombok builder will correctly initialise the adjustmentRequired Boolean to false
		but Jackson mapping would result in adjustmentRequired being null if no value provided and a 
		NPE in isTotalDebitAmountValid() if adjustmentRequired is null on object creation.*/
		
		final RequestForTransferView requestForTransfer = requestForTransferViewBuilder.build();
		
		assertNotNull(requestForTransfer.getAdjustmentsRequired());
		assertThat(requestForTransfer.getAdjustmentsRequired(), is(false));
	}

	@Test
	public void adjustmentsRequiredShouldBeTrueWhenSetToTrue() {

		final RequestForTransferView requestForTransfer = requestForTransferViewBuilder.build();
		requestForTransfer.setAdjustmentsRequired(true);
		
		assertNotNull(requestForTransfer.getAdjustmentsRequired());
		assertThat(requestForTransfer.getAdjustmentsRequired(), is(true));
	}

  @Test
  public void nullTotalDebitAmountIsValid() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .totalDebitAmount(null)
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(true)));
  }

  @Test
  public void contributionsMatchingTotalDebitAmountIsValid() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("50"))
        .additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70"))
        .totalDebitAmount(new BigDecimal("210"))
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(true)));
  }

  @Test
  public void contributionsNotMatchingTotalDebitAmountIsInvalid() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("50"))
        .additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70"))
        .totalDebitAmount(new BigDecimal("211"))
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(false)));
  }

  @Test
  public void contributionsAndAdjustmentsMatchingTotalDebitAmountIsValid() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("50"))
        .additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70"))
        .adjustmentsRequired(true)
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .employeeContributions(new BigDecimal("1"))
            .employerContributions(new BigDecimal("2"))
            .employeeAddedYears(new BigDecimal("3"))
            .additionalPension(new BigDecimal("4"))
            .errbo(new BigDecimal("5"))
            .build()
        ))
        .totalDebitAmount(new BigDecimal("225"))
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(true)));
  }

  @Test
  public void contributionsAndNegativeAdjustmentsMatchingTotalDebitAmountIsValid() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("50"))
        .additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70"))
        .adjustmentsRequired(true)
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .employeeContributions(new BigDecimal("-1000"))
            .employerContributions(new BigDecimal("2"))
            .employeeAddedYears(new BigDecimal("3"))
            .additionalPension(new BigDecimal("4"))
            .errbo(new BigDecimal("5"))
            .build()
        ))
        .totalDebitAmount(new BigDecimal("-776"))
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(true)));
  }

  @Test
  public void contributionsAndAdjustmentsMatchingTotalDebitAmountIsInvalid() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("50"))
        .additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .employeeContributions(new BigDecimal("1"))
            .employerContributions(new BigDecimal("2"))
            .employeeAddedYears(new BigDecimal("3"))
            .additionalPension(new BigDecimal("4"))
            .errbo(new BigDecimal("5"))
            .build()
        ))
        .totalDebitAmount(new BigDecimal("226"))
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(false)));
  }

  @Test
  public void contributionsMatchingTotalDebitAmountIsValidRegardlessOfDecimalPlaces() {
    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("50"))
        .additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70"))
        .totalDebitAmount(new BigDecimal("210.00"))
        .build();

    assertThat(requestForTransfer.isTotalDebitAmountValid(), is(equalTo(true)));
  }

  @Test
  public void isAdjustmentDateBeforeContributionDateTest() {
    ContributionDate adjustmentContributionDate = new ContributionDate();
    ContributionDate contributionDate = new ContributionDate();
    contributionDate.setContributionMonth("march");
    contributionDate.setContributionYear(2010);
    adjustmentContributionDate.setContributionMonth("july");
    adjustmentContributionDate.setContributionYear(2010);

    final RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .contributionDate(contributionDate)
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(adjustmentContributionDate)
            .build()
        ))
        .build();

    assertThat(requestForTransfer.isAdjustmentDateBeforeContributionDate(), is(equalTo(false)));

  }

  @Test
  public void payAsSoonAsPossibleNotBlankTest() throws Exception {

    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .build();
    requestForTransfer.setPayAsSoonAsPossible("");

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, FirstSchedulePaymentValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{schedulePayment.asap.notBlank}"))));
  }

  @Test
  public void transferFormDateNotBlankValidationError() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .payAsSoonAsPossible("N")
        .transferDate(TransferFormDate.builder().days("").month("").year("").build())
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationSequence.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{formDate.notBlank}"))));
  }

  @Test
  public void isWorkingDayValidationError() {

    LocalDate localDate = LocalDate.now();
    //Jump beyond the initial minimum day validation
    localDate = localDate.plusDays(3);
    //Increment until we get the next non week day.
    while(!DayOfWeek.SATURDAY.equals(localDate.getDayOfWeek())){
      localDate = localDate.plusDays(1);
    }

    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .payAsSoonAsPossible("N")
        .transferDate(TransferFormDate.builder()
            .days(localDate.format(DAY_FORMAT))
            .month(localDate.format(MONTH_FORMAT))
            .year(localDate.format(YEAR_FORMAT))
            .build())
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{transferDate.isWorkingDay}"))));
  }

  @Test
  public void dateLessThan31DaysValidationError() {

    LocalDate plus32Days = LocalDate.now().plusDays(32);
    while(DayOfWeek.SATURDAY.equals(plus32Days.getDayOfWeek()) || DayOfWeek.SUNDAY.equals(plus32Days.getDayOfWeek())){
      plus32Days = plus32Days.plusDays(2);
    }
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .payAsSoonAsPossible("N")
        .transferDate(TransferFormDate.builder()
            .days(plus32Days.format(DAY_FORMAT))
            .month(plus32Days.format(MONTH_FORMAT))
            .year(plus32Days.format(YEAR_FORMAT))
            .build())
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{transferDate.greaterThan31Days}"))));
  }

  @Test
  public void DateIsAfterTimeSensitiveDaysValidationError() throws Exception {

    LocalDate localDate = LocalDate.now();

    //Incase we're running tests at the weekend we don't want a localDate that triggers the workDay validation error
    while(DayOfWeek.SATURDAY.equals(localDate.getDayOfWeek()) || DayOfWeek.SUNDAY.equals(localDate.getDayOfWeek())){
      localDate = localDate.plusDays(1);
    }

    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .payAsSoonAsPossible("N")
        .transferDate(TransferFormDate.builder()
            .days(localDate.format(DAY_FORMAT))
            .month(localDate.format(MONTH_FORMAT))
            .year(localDate.format(YEAR_FORMAT))
            .build())
        .build();

    final Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));

    List<String> violationMessages = constraintViolations.stream().map(ConstraintViolation::getMessage)
        .collect(Collectors.toList());

    if(LocalDateTime.now().getHour()<15) {
      assertThat(violationMessages, hasItem("{transferDate.afterTimeSensitiveDays.before}"));
    } else {
      assertThat(violationMessages, hasItem("{transferDate.afterTimeSensitiveDays.after}"));
    }
  }

  @Test
  public void addedYearsAdditionalPensionErrboAllowZero() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("0"))
        .additionalPension(new BigDecimal("0"))
        .errbo(new BigDecimal("0"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));
  }

  @Test
  public void addedYearsAdditionalPensionErrboAllowLessThan1() {
    RequestForTransferView requestForTransfer = requestForTransferViewBuilder
        .employeeAddedYears(new BigDecimal("0.1"))
        .additionalPension(new BigDecimal("0.5"))
        .errbo(new BigDecimal("0.9"))
        .build();

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));
  }

  private TransferFormDate tomorrowValidDate() {

    TransferFormDate transferFormDate = new TransferFormDate();
    LocalDate localDate = LocalDate.now().plusDays(3);
    while(DayOfWeek.SATURDAY.equals(localDate.getDayOfWeek()) || DayOfWeek.SUNDAY.equals(localDate.getDayOfWeek())){
      localDate = localDate.plusDays(2);
    }
    transferFormDate.setDays(localDate.format(DAY_FORMAT));
    transferFormDate.setMonth(localDate.format(MONTH_FORMAT));
    transferFormDate.setYear(localDate.format(YEAR_FORMAT));

    return transferFormDate;
  }

}
