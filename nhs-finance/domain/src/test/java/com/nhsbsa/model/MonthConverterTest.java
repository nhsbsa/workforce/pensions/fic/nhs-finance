package com.nhsbsa.model;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Created by Stuart Bradley 10/02/2016
 */

@RunWith(Parameterized.class)
public class MonthConverterTest {

  private MonthConverter monthConverter = new MonthConverter();
  private String nullStringMonth;
  private String monthString;
  private Integer monthNumber;
  private Integer nullIntMonth;


  public MonthConverterTest(Integer monthNumber, String monthString) {
    this.monthString = monthString;
    this.monthNumber = monthNumber;
  }

  @Parameterized.Parameters
  public static Collection monthString() {
    return Arrays.asList(new Object[][]{
        {1, "January"},
        {2, "February"},
        {3, "March"},
        {4, "April"},
        {5, "May"},
        {6, "June"},
        {7, "July"},
        {8, "August"},
        {9, "September"},
        {10, "October"},
        {11, "November"},
        {12, "December"}
    });
  }

  @Before
  public void initializeObject() {
    nullStringMonth = null;
    monthConverter = new MonthConverter();
  }

  @Test
  public void convertToDatabaseColumnIfNullExpectReturn0() throws Exception {
    int expectedResult;
    expectedResult = monthConverter.convertToDatabaseColumn(nullStringMonth);
    assertThat(expectedResult, is(0));
  }

  @Test
  public void convertToDatabaseColumnUsingMonthNum() throws Exception {
    assertThat(monthNumber, is(monthConverter.convertToDatabaseColumn(monthString)));
  }

  @Test
  public void convertToEntityAttributeIfNullReturnNull() throws Exception {
    String expectedResult;
    expectedResult = monthConverter.convertToEntityAttribute(nullIntMonth);
    assertThat(expectedResult, is(nullValue()));
  }

  @Test
  public void converToEntituAttributeUsingMonthString() throws Exception {
    assertThat(monthString, is(monthConverter.convertToEntityAttribute(monthNumber)));
  }


}