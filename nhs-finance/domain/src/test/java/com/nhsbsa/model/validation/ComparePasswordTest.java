package com.nhsbsa.model.validation;

import com.nhsbsa.security.SetPassword;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by nataliehulse on 10/10/2017.
 */
public class ComparePasswordTest {

    private Validator comparePasswordValidator;

    @Before
    public void setUp() {
        comparePasswordValidator = new ComparePasswordValidator();
    }

    @Test
    public void supports() {
        assertTrue(comparePasswordValidator.supports(SetPassword.class));
        assertFalse(comparePasswordValidator.supports(Object.class));
    }

    @Test
    public void passwordsMatch() {
        SetPassword setPassword = new SetPassword();
        setPassword.setNewPassword("Testing1");
        setPassword.setPasswordConfirm("Testing1");
        BindException errors = new BindException(setPassword, "setpassword");
        ValidationUtils.invokeValidator(comparePasswordValidator, setPassword, errors);
        assertFalse(errors.hasErrors());
    }

    @Test
    public void passwordsDoNotMatch() {
        SetPassword setPassword = new SetPassword();
        setPassword.setNewPassword("Testing1");
        setPassword.setPasswordConfirm("Testing2");
        BindException errors = new BindException(setPassword, "setpassword");
        ValidationUtils.invokeValidator(comparePasswordValidator, setPassword, errors);
        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount("newPassword"));
        assertEquals("setPassword.passwordsDontMatch", errors.getFieldError("newPassword").getCode());
    }


}





