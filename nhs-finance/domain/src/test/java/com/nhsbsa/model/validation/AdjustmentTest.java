package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.ContributionDate;
import java.math.BigDecimal;
import java.time.Month;
import java.time.YearMonth;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdjustmentTest {

  private static Validator validator;

  private YearMonth now = YearMonth.now();
  private YearMonth nowPlus1Month = now.plusMonths(1);
  private YearMonth nowMinus2Months = now.minusMonths(2);

  private ContributionDate validAdjustmentDate = ContributionDate.builder()
      .contributionMonth(Month.of(nowMinus2Months.getMonthValue()).toString())
      .contributionYear(nowMinus2Months.getYear())
      .build();
  
  private ContributionDate invalidAdjustmentDate = ContributionDate.builder()
      .contributionMonth(Month.of(nowPlus1Month.getMonthValue()).toString())
      .contributionYear(nowPlus1Month.getYear())
      .build();
  
  Adjustment adjustment;
  
  @BeforeClass
  public static void setUpValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Before
  public void before() {
    
    adjustment = Adjustment.builder()
        .adjustmentContributionDate(validAdjustmentDate)
        .build();
  }

  @Test
  public void employeeContributionsAdjustmentsNotEnteredValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("0"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.employeeContributions.notZero}"))));
  }

  @Test
  public void employeeContributionsAdjustmentsNegativeNotValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("-0.50"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
  }

  @Test
  public void employeeContributionsAdjustmentsMaxNegativeNotValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("-99999999.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
  }

  @Test
  public void employeeContributionsAdjustmentsMaxPositiveNotValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("99999999.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
  }

  @Test
  public void employeeContributionsAdjustmentsMaxPositiveWithSignNotValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("+99999999.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
  }

  @Test
  public void employeeContributionsAdjustmentsOverflowMaxNegativeValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("-999999999.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.employeeContributions.invalid}"))));
  }

  @Test
  public void employeeContributionsAdjustmentsOverflowMaxPositiveValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("999999999.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.employeeContributions.invalid}"))));
  }

  @Test
  public void employeeContributionsAdjustmentsOverflowMaxPositiveWithSignValidationError() {
    
    adjustment.setEmployeeContributions(new BigDecimal("+999999999.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.employeeContributions.invalid}"))));
  }

  @Test
  public void employerContributionsAdjustmentsNotEnteredValidationError() {
    
    adjustment.setEmployerContributions(new BigDecimal("57.423"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.employerContributions.invalid}"))));
  }

  @Test
  public void employeeAddedYearsAdjustmentsNotEnteredValidationError() {
 
    adjustment.setEmployeeAddedYears(new BigDecimal("1.5432"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.employeeAddedYears.invalid}"))));
  }

  @Test
  public void additionalPensionAdjustmentsNotEnteredValidationError() {
    
    adjustment.setAdditionalPension(new BigDecimal("12.532"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.additionalPension.invalid}"))));
  }

  @Test
  public void errboAdjustmentsNotEnteredValidationError() {
    
    adjustment.setErrbo(new BigDecimal("0"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.errbo.notZero}"))));
  }

  @Test
  public void errboAdjustmentsFractionalNoValidationError() {
    
    adjustment.setErrbo(new BigDecimal("0.99"));

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
  }
    
  @Test
  public void adjustmentContributionDateNotEnteredValidationError() {
    
    adjustment = Adjustment.builder()
        .employeeContributions(new BigDecimal("-0.50"))
        .build();

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustmentContributionDate.not.blank}"))));
  }
  
  @Test
  public void adjustmentContributionDateInvalidValidationError() {
    
    adjustment = Adjustment.builder()
        .adjustmentContributionDate(invalidAdjustmentDate)
        .employeeContributions(new BigDecimal("-0.50"))
        .build();

    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);
    
    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustmentContributionDate.not.in.range}"))));
  }
  
  private Set<ConstraintViolation<Adjustment>> validateAdjustment(Adjustment adjustment) {
    return validator.validate(adjustment, AdjustmentValidationGroup.class);
  }
}
