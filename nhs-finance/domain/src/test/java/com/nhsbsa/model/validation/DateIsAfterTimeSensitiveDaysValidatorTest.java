package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.view.RequestForTransferView;
import java.time.LocalDateTime;
import java.time.Month;

import javax.validation.ConstraintValidatorContext;

import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DateIsAfterTimeSensitiveDaysValidatorTest {

	DateIsAfterTimeSensitiveDaysValidator validator;
	ConstraintValidatorContext context;

	@Before
	public void setUp() {
		validator = new DateIsAfterTimeSensitiveDaysValidator();

		context = Mockito.mock(ConstraintValidatorContext.class);

		final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder = 
				Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);

		when(context.buildConstraintViolationWithTemplate(Mockito.anyString())).thenReturn(constraintViolationBuilder);

		NodeBuilderCustomizableContext customCtx = mock(NodeBuilderCustomizableContext.class);

		doReturn(customCtx).when(constraintViolationBuilder).addPropertyNode(any());
		doReturn(context).when(customCtx).addConstraintViolation();
	}

	@Test
	public void given_valid_date_before_threshold_when_isValid_then_return_true() throws Exception {

		validator = new DateIsAfterTimeSensitiveDaysValidator() {
			@Override
			public LocalDateTime getDateTime() {
				// Before default threshold of 3pm.
				return LocalDateTime.of(2018, Month.APRIL, 30, 12, 30);
			}
		};

		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("2").month("May").year("2018").build()).build();
		final Boolean value = validator.isValid(rft, context);
		assertTrue(value);
	}

	@Test
	public void given_invalid_date_before_threshold_when_isValid_then_return_false() throws Exception {

		validator = new DateIsAfterTimeSensitiveDaysValidator() {
			@Override
			public LocalDateTime getDateTime() {
				// Before default threshold of 3pm.
				return LocalDateTime.of(2018, Month.APRIL, 30, 12, 30);
			}
		};

		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("1").month("May").year("2018").build()).build();
		final Boolean value = validator.isValid(rft, context);
		assertFalse(value);
	}

	@Test
	public void given_valid_date_after_threshold_when_isValid_then_return_true() throws Exception {

		validator = new DateIsAfterTimeSensitiveDaysValidator() {
			@Override
			public LocalDateTime getDateTime() {
				// After default threshold of 3pm.
				return LocalDateTime.of(2018, Month.APRIL, 30, 17, 30);
			}
		};

		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("3").month("May").year("2018").build()).build();
		final Boolean value = validator.isValid(rft, context);
		assertTrue(value);
	}

	@Test
	public void given_invalid_date_after_threshold_when_isValid_then_return_false() throws Exception {

		validator = new DateIsAfterTimeSensitiveDaysValidator() {
			@Override
			public LocalDateTime getDateTime() {
				// After default threshold of 3pm.
				return LocalDateTime.of(2018, Month.APRIL, 30, 15, 00);
			}
		};

		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("30").month("April").year("2018").build()).build();
		final Boolean value = validator.isValid(rft, context);
		assertFalse(value);
	}

	@Test
	public void given_null_date_when_isValid_then_return_true() {
		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("").month("").year("").build()).build();
		final Boolean value = validator.isValid(rft, context);
		assertThat(value, is(equalTo(true)));
	}

	@Test
	public void given_invalid_date_format_then_return_true() {
		RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
				TransferFormDate.builder().days("15").month("May").year("18").build()).build();
		final Boolean value = validator.isValid(rft, context);
		assertThat(value, is(equalTo(true)));
	}

}
