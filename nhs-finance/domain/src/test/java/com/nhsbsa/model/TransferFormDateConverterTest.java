package com.nhsbsa.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Stuart Bradley on 10/11/2016.
 */

public class TransferFormDateConverterTest {

  private TransferFormDateConverter test;
  private Date date;


  @Before
  public void initializeObject() {
    test = new TransferFormDateConverter();
  }

  @Test
  public void convertToDatabaseColumnTest() throws Exception {
    TransferFormDate transferFormDate = TransferFormDate.builder()
        .days("09")
        .month("February")
        .year("2017")
        .build();
    final Date actualValue = test.convertToDatabaseColumn(transferFormDate);
    DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
    Date expectedValue = formatter.parse("09/02/2017");
    assertThat(actualValue, is(equalTo(expectedValue)));
  }

  @Test
  public void convertToEntityAttributeWithNullDate() throws ParseException {

    TransferFormDate formDate = test.convertToEntityAttribute(null);

    Assert.assertThat(formDate.getDays(), Matchers.is(nullValue()));
    Assert.assertThat(formDate.getMonth(), Matchers.is(nullValue()));
    Assert.assertThat(formDate.getYear(), Matchers.is(nullValue()));
  }

  @Test
  public void convertToEntityAttribute() throws ParseException {
    Date date = new SimpleDateFormat("yyyyMMdd").parse("20161221");

    TransferFormDate formDate = test.convertToEntityAttribute(date);

    Assert.assertThat(formDate.getDays(), Matchers.is(equalTo("21")));
    Assert.assertThat(formDate.getMonth(), Matchers.is(equalTo("December")));
    Assert.assertThat(formDate.getYear(), Matchers.is(equalTo("2016")));
  }
}


