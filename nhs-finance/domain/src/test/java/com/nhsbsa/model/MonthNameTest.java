package com.nhsbsa.model;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Created by nataliehulse on 10/11/2016.
 */


@RunWith(Parameterized.class)
public class MonthNameTest {

  private Integer inputNumber;
  private String expectedResult;
  private final MonthName months = new MonthName();

  public MonthNameTest(Integer inputNumber, String expectedResult) {
    this.inputNumber = inputNumber;
    this.expectedResult = expectedResult;
  }

  @Parameterized.Parameters
  public static Collection monthNumber() {
    return Arrays.asList(new Object[][]{
        {1, "January"},
        {2, "February"},
        {3, "March"},
        {4, "April"},
        {5, "May"},
        {6, "June"},
        {7, "July"},
        {8, "August"},
        {9, "September"},
        {10, "October"},
        {11, "November"},
        {12, "December"}
    });
  }

  @Test
  public void getMonthFromNumberTest() {
    System.out.println("The month number is " + inputNumber + " the expected result is "
        + expectedResult + " the actual result is "
        + months.getMonthNameFromNum(inputNumber));
    assertThat(expectedResult, is(months.getMonthNameFromNum(inputNumber)));
  }

  @Test
  public void IncorrectMonthNoShouldReturnNull() {
    String monthName = months.getMonthNameFromNum(0);
    assertEquals("Should be equal", null, monthName);
  }
}



