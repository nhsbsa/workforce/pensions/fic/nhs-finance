package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by stuartbradley on 27/03/2017.
 */

public class TotalAmountToBeDebitedValidatorTest {

  private TotalAmountToBeDebitedValidator test;

  @Before
  public void before() {
    test = new TotalAmountToBeDebitedValidator();
  }

  @Test
  public void amountValuesEqualsNull() throws Exception {
    final boolean value = test.isValid(null, null);
    assertThat(value, is(true));
  }

  @Test
  public void bigDecimalIsValid() throws Exception {
    final boolean value = test.isValid(BigDecimal.valueOf(12345678.90), null);
    assertThat(value, Is.is(true));
  }

  @Test
  public void bigDecimalTooManyDecimals() throws Exception {
    final boolean value = test.isValid(BigDecimal.valueOf(12345678.901), null);
    assertThat(value, Is.is(false));
  }

  @Test
  public void bigDecimalFormatTooLong() throws Exception {
    final boolean value = test.isValid(BigDecimal.valueOf(123456789999999.01), null);
    assertThat(value, Is.is(false));
  }

  @Test
  public void bigDecimalFormat0Decimal() throws Exception {
    final boolean value = test.isValid(BigDecimal.valueOf(0.12), null);
    assertThat(value, Is.is(true));
  }


}
