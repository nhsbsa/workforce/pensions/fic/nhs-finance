package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by Mark Lishman on 18/11/2016.
 */
public class EmployerContributionThresholdValidatorTest {

  private static final ConstraintValidatorContext NOT_REQUIRED = null;

  private EmployerContributionThresholdValidator validator;

  @Before
  public void before() {
    validator = new EmployerContributionThresholdValidator();
  }

  @Test
  public void isValidReturnsTrueWhenTotalPensionablePayIsNull() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .employerContributions(new BigDecimal("123.45"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsTrueWhenEmployerContributionsIsNull() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("123.45"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsTrueWhenEmployerContributionsIsInsideThreshold() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("100"))
        .employerContributions(new BigDecimal("14.33"))
        .build();

    boolean actual = validator.isValid(rft, NOT_REQUIRED);

    assertThat(actual, is(equalTo(true)));
  }

  @Test
  public void isValidReturnsFalseWhenEmployerContributionsIsOutsideThreshold() {
    RequestForTransferView rft = RequestForTransferView
        .builder()
        .totalPensionablePay(new BigDecimal("100"))
        .employerContributions(new BigDecimal("14.3"))
        .build();

    ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
    HibernateConstraintValidatorContext hibernateContext = Mockito
        .mock(HibernateConstraintValidatorContext.class);
    when(context.unwrap(HibernateConstraintValidatorContext.class)).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(hibernateContext);

    ConstraintViolationBuilder builder = Mockito.mock(ConstraintViolationBuilder.class);
    when(hibernateContext.buildConstraintViolationWithTemplate(anyString())).thenReturn(builder);
    when(builder.addPropertyNode(anyString()))
        .thenReturn(Mockito.mock(NodeBuilderCustomizableContext.class));

    boolean actual = validator.isValid(rft, context);

    assertThat(actual, is(equalTo(false)));
    Mockito.verify(hibernateContext, times(1)).disableDefaultConstraintViolation();
    Mockito.verify(hibernateContext, times(1))
        .addExpressionVariable(Mockito.anyString(), Mockito.anyString());
    Mockito.verify(hibernateContext, times(1))
        .buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate());
    Mockito.verify(hibernateContext
            .buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()),
        times(1))
        .addPropertyNode("employerContributions");
    Mockito.verify(hibernateContext
        .buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
        .addPropertyNode("employerContributions"), times(1))
        .addConstraintViolation();
  }

}