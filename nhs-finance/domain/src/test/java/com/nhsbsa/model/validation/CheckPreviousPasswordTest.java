package com.nhsbsa.model.validation;

import com.nhsbsa.security.SetPassword;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by nataliehulse on 10/10/2017.
 */
public class CheckPreviousPasswordTest {

    private Validator checkPreviousPasswordValidator;

    @Before
    public void setUp() {
        checkPreviousPasswordValidator = new CheckPreviousPasswordValidator();
    }

    @Test
    public void supports() {
        assertTrue(checkPreviousPasswordValidator.supports(SetPassword.class));
        assertFalse(checkPreviousPasswordValidator.supports(Object.class));
    }

    @Test
    public void currentPasswordIsNotSameAsPrevious() {
        SetPassword setPassword = new SetPassword();
        setPassword.setPasswordSameAsPrevious(false);
        BindException errors = new BindException(setPassword, "setpassword");
        ValidationUtils.invokeValidator(checkPreviousPasswordValidator, setPassword, errors);
        assertFalse(errors.hasErrors());
    }

    @Test
    public void currentPasswordIsSameAsPrevious() {
        SetPassword setPassword = new SetPassword();
        setPassword.setPasswordSameAsPrevious(true);
        BindException errors = new BindException(setPassword, "setpassword");
        ValidationUtils.invokeValidator(checkPreviousPasswordValidator, setPassword, errors);
        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getFieldErrorCount("newPassword"));
        assertEquals("setPassword.passwordSameAsPrevious", errors.getFieldError("newPassword").getCode());
    }


}





