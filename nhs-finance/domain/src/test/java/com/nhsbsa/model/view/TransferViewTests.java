package com.nhsbsa.model.view;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.view.RequestForTransferView;
import com.nhsbsa.view.TransferView;
import com.nhsbsa.view.TransferViewConverter;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferViewTests {

    public static final String GP = "G";
    public static final String STAFF = "S";

    @Test
    public void given_minimal_rft_when_used_as_argument_to_of_then_transfer_view_constructed() {
        RequestForTransferView rft = RequestForTransferView.builder()
                .eaCode("DMY0000")
                .isGp(true)
                .contributionDate(
                    ContributionDate.builder().contributionMonth("OCTOBER").contributionYear(2012).build())
                .employeeContributions(new BigDecimal("12.34"))
                .employerContributions(new BigDecimal("56.78"))
                .transferDate(TransferFormDate.builder().days("15").month("MAY").year("2014").build())
                .build();

        TransferView view = TransferViewConverter.of(rft);

        SoftAssertions softly = new SoftAssertions();
        // Mandatory fields
        softly.assertThat(view.getAccountCode()).as("account code").isEqualTo("DMY0000");
        softly.assertThat(view.getTransactionDate()).as("local date").isEqualTo(LocalDate.of(2014, 5, 15));
        softly.assertThat(view.getContributionYearMonth()).as("cont month year").isEqualTo("1210");
        softly.assertThat(view.getContributions()).as("contribs").isEqualTo(new BigDecimal("12.34"));
        softly.assertThat(view.getEmployerContributions()).as("employer contribs").isEqualTo(new BigDecimal("56.78"));
        // Optional contribution fields
        softly.assertThat(view.getAddedYears()).as("added years").isNull();
        softly.assertThat(view.getAdditionalPension()).as("additional pension").isNull();
        softly.assertThat(view.getErrbo()).as("errbo").isNull();
        // Adjustment fields (all optional)
        // If there are no adjustments at all, values are null; otherwise they are zeros
        softly.assertThat(view.getAdjustmentYearMonth()).as("adj month year").isNull();
        softly.assertThat(view.getAdjustmentContributions()).as("adj contributions").isNull();
        softly.assertThat(view.getAdjustmentEmployerContributions()).as("adj employer contributions").isNull();
        softly.assertThat(view.getAdjustmentAddedYears()).as("adj added years").isNull();
        softly.assertThat(view.getAdjustmentAdditionalPension()).as("adj additional pension").isNull();
        softly.assertThat(view.getAdjustmentErrbo()).as("adj errbo").isNull();
        softly.assertAll();

        assertThat(view.getEmployerType()).isEqualTo(GP);
    }

    @Test
    public void given_minimal_rft_with_single_digit_year_when_used_as_argument_to_of_then_transfer_view_constructed() {
        RequestForTransferView rft = RequestForTransferView.builder()
                .eaCode("DMY0000")
                .isGp(false)
                .contributionDate(ContributionDate.builder().contributionMonth("OCTOBER").contributionYear(2008).build())
                .employeeContributions(new BigDecimal("12.34"))
                .employerContributions(new BigDecimal("56.78"))
                .transferDate(TransferFormDate.builder().days("15").month("MAY").year("2014").build())
                .build();

        TransferView view = TransferViewConverter.of(rft);

        SoftAssertions softly = new SoftAssertions();
        // Mandatory fields
        softly.assertThat(view.getAccountCode()).isEqualTo("DMY0000");
        softly.assertThat(view.getTransactionDate()).isEqualTo(LocalDate.of(2014, 5, 15));
        softly.assertThat(view.getContributionYearMonth()).isEqualTo("0810");
        softly.assertThat(view.getContributions()).isEqualTo(new BigDecimal("12.34"));
        softly.assertThat(view.getEmployerContributions()).isEqualTo(new BigDecimal("56.78"));
        // Optional contribution fields
        softly.assertThat(view.getAddedYears()).isNull();
        softly.assertThat(view.getAdditionalPension()).isNull();
        softly.assertThat(view.getErrbo()).isNull();
        // Adjustment fields (all optional)
        // If there are no adjustments at all, values are null; otherwise they are zeros
        softly.assertThat(view.getAdjustmentYearMonth()).isNull();
        softly.assertThat(view.getAdjustmentContributions()).as("adj contributions").isNull();
        softly.assertThat(view.getAdjustmentEmployerContributions()).as("adj employer contributions").isNull();
        softly.assertThat(view.getAdjustmentAddedYears()).as("adj added years").isNull();
        softly.assertThat(view.getAdjustmentAdditionalPension()).as("adj additional pension").isNull();
        softly.assertThat(view.getAdjustmentErrbo()).as("adj errbo").isNull();
        softly.assertAll();

        assertThat(view.getEmployerType()).isEqualTo(STAFF);
    }

    @Test
    public void given_minimal_rft_with_single_digit_month_when_used_as_argument_to_of_then_transfer_view_constructed() {
        RequestForTransferView rft = RequestForTransferView.builder()
                .eaCode("DMY0000")
                .isGp(true)
                .contributionDate(ContributionDate.builder().contributionMonth("MARCH").contributionYear(2013).build())
                .employeeContributions(new BigDecimal("12.34"))
                .employerContributions(new BigDecimal("56.78"))
                .transferDate(TransferFormDate.builder().days("15").month("MAY").year("2014").build())
                .build();

        TransferView view = TransferViewConverter.of(rft);

        SoftAssertions softly = new SoftAssertions();
        // Mandatory fields
        softly.assertThat(view.getAccountCode()).isEqualTo("DMY0000");
        softly.assertThat(view.getTransactionDate()).isEqualTo(LocalDate.of(2014, 5, 15));
        softly.assertThat(view.getContributionYearMonth()).isEqualTo("1303");
        softly.assertThat(view.getContributions()).isEqualTo(new BigDecimal("12.34"));
        softly.assertThat(view.getEmployerContributions()).isEqualTo(new BigDecimal("56.78"));
        // Optional contribution fields
        softly.assertThat(view.getAddedYears()).isNull();
        softly.assertThat(view.getAdditionalPension()).isNull();
        softly.assertThat(view.getErrbo()).isNull();
        // Adjustment fields (all optional)
        // If there are no adjustments at all, values are null; otherwise they are zeros
        softly.assertThat(view.getAdjustmentYearMonth()).isNull();
        softly.assertThat(view.getAdjustmentContributions()).as("adj contributions").isNull();
        softly.assertThat(view.getAdjustmentEmployerContributions()).as("adj employer contributions").isNull();
        softly.assertThat(view.getAdjustmentAddedYears()).as("adj added years").isNull();
        softly.assertThat(view.getAdjustmentAdditionalPension()).as("adj additional pension").isNull();
        softly.assertThat(view.getAdjustmentErrbo()).as("adj errbo").isNull();
        softly.assertAll();

        assertThat(view.getEmployerType()).isEqualTo(GP);
    }

    @Test
    public void given_rft_with_adjustment_when_used_as_argument_to_of_then_transfer_view_constructed() {
        Adjustment adjustment = Adjustment.builder()
                .adjustmentContributionDate(
                    ContributionDate.builder().contributionMonth("FEBRUARY").contributionYear(2001).build())
                .additionalPension(new BigDecimal("1234.56"))
                .build();
        RequestForTransferView rft = RequestForTransferView.builder()
                .eaCode("DMY0000")
                .isGp(true)
                .contributionDate(ContributionDate.builder().contributionMonth("MARCH").contributionYear(2013).build())
                .employeeContributions(new BigDecimal("12.34"))
                .employerContributions(new BigDecimal("56.78"))
                .transferDate(TransferFormDate.builder().days("15").month("MAY").year("2014").build())
                .adjustmentList(Collections.singletonList(adjustment))
                .build();

        TransferView view = TransferViewConverter.of(rft);

        SoftAssertions softly = new SoftAssertions();
        // Mandatory fields
        softly.assertThat(view.getAccountCode()).isEqualTo("DMY0000");
        softly.assertThat(view.getTransactionDate()).isEqualTo(LocalDate.of(2014, 5, 15));
        softly.assertThat(view.getContributionYearMonth()).isEqualTo("1303");
        softly.assertThat(view.getContributions()).isEqualTo(new BigDecimal("12.34"));
        softly.assertThat(view.getEmployerContributions()).isEqualTo(new BigDecimal("56.78"));
        // Optional contribution fields
        softly.assertThat(view.getAddedYears()).isNull();
        softly.assertThat(view.getAdditionalPension()).isNull();
        softly.assertThat(view.getErrbo()).isNull();
        // Adjustment fields (all optional)
        softly.assertThat(view.getAdjustmentYearMonth()).isEqualTo("0102");
        softly.assertThat(view.getAdjustmentContributions()).as("adj contributions").isNull();
        softly.assertThat(view.getAdjustmentEmployerContributions()).as("adj employer contributions").isNull();
        softly.assertThat(view.getAdjustmentAddedYears()).as("adj added years").isNull();
        softly.assertThat(view.getAdjustmentAdditionalPension()).as("adj additional pension").isEqualTo(new BigDecimal("1234.56"));
        softly.assertThat(view.getAdjustmentErrbo()).as("adj errbo").isNull();
        softly.assertAll();

        assertThat(view.getEmployerType()).isEqualTo(GP);
    }
}
