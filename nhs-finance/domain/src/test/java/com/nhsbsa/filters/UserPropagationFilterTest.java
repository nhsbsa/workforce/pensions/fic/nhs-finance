package com.nhsbsa.filters;

import com.nhsbsa.model.User;
import java.util.UUID;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


public class UserPropagationFilterTest {

  private static final String GET = HttpMethod.GET.name();
  private static final String POST = HttpMethod.POST.name();
  private static final String ENDPOINT = "/endpoint/";
  private static final String URL = "http://test" + ENDPOINT;

  private RestTemplate restTemplate;
  private HttpServletRequest request;
  private HttpServletResponse response;
  private FilterChain filterChain;

  @Before
  public void before() {
    restTemplate = Mockito.mock(RestTemplate.class);
    request      = Mockito.mock(HttpServletRequest.class);
    response     = Mockito.mock(HttpServletResponse.class);
    filterChain  = Mockito.mock(FilterChain.class);
  }

  @Test
  public void testGetRequest() throws Exception {

    UserPropagationFilter filter = new UserPropagationFilter(restTemplate, URL, new String[]{});
    when(request.getMethod()).thenReturn(GET);

    verifyNoMoreInteractions(restTemplate);
    filter.doFilter(request, response, filterChain);

    verify(filterChain, times(1)).doFilter(request, response);
  }

  @Test
  public void testExcludedRequest() throws Exception {
    UserPropagationFilter filter = new UserPropagationFilter(restTemplate, URL, new String[]{ENDPOINT});
    when(request.getMethod()).thenReturn(GET);
    when(request.getServletPath()).thenReturn(ENDPOINT);

    verifyNoMoreInteractions(restTemplate);
    filter.doFilter(request, response, filterChain);

    verify(filterChain, times(1)).doFilter(request, response);
  }

  @Test
  public void testExcludedRequestWithRegex() throws Exception {
    String endpoint = "/user/0ddacbf6-5e1c-440a-ba70-a5d228209604";
    String url = "http://test" + endpoint;
    String regex = "/user/.*";
    UserPropagationFilter filter = new UserPropagationFilter(restTemplate, url, new String[]{regex});
    when(request.getMethod()).thenReturn(GET);
    when(request.getServletPath()).thenReturn(endpoint);

    verifyNoMoreInteractions(restTemplate);
    filter.doFilter(request, response, filterChain);

    verify(filterChain, times(1)).doFilter(request, response);
  }

  @Test
  public void testFilterHappyPath() throws Exception {
    UserPropagationFilter filter = new UserPropagationFilter(restTemplate, URL, new String[]{});
    String name = "userName";
    String role = "ROLE_STANDARD";
    String uuid = UUID.randomUUID().toString();
    String userRequest = URL + uuid;

    ResponseEntity<User> userResponse = Mockito.mock(ResponseEntity.class);
    when(userResponse.getStatusCode()).thenReturn(HttpStatus.OK);
    when(userResponse.getBody()).thenReturn(
        User.builder()
            .name(name)
            .role(role)
            .uuid(uuid)
            .build()
    );
    when(request.getMethod()).thenReturn(POST);
    when(request.getHeader("PROPAGATED_UUID")).thenReturn(uuid);
    when(restTemplate.getForEntity(userRequest, User.class)).thenReturn(userResponse);

    filter.doFilter(request, response, filterChain);

    verify(filterChain, times(1)).doFilter(request, response);
    verify(restTemplate, times(1)).getForEntity(userRequest, User.class);
  }

  @Test
  public void testMissingPath() throws Exception {
    UserPropagationFilter filter = new UserPropagationFilter(restTemplate, URL, new String[]{});
    when(request.getServletPath()).thenReturn(ENDPOINT);
    when(request.getMethod()).thenReturn(POST);

    verifyNoMoreInteractions(restTemplate);
    verifyNoMoreInteractions(filterChain);
    filter.doFilter(request, response, filterChain);

    verify(response, times(1))
        .sendError(HttpStatus.BAD_REQUEST.value(),"Missing PROPAGATED_UUID Http header on path: "+ENDPOINT);
  }

  @Test
  public void testMissingUser() throws Exception {
    UserPropagationFilter filter = new UserPropagationFilter(restTemplate, URL, new String[]{});
    String uuid = UUID.randomUUID().toString();
    String userRequest = URL + uuid;

    when(request.getMethod()).thenReturn(POST);
    when(request.getHeader("PROPAGATED_UUID")).thenReturn(uuid);
    when(restTemplate.getForEntity(userRequest, User.class))
        .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

    verifyNoMoreInteractions(filterChain);
    filter.doFilter(request, response, filterChain);

    verify(restTemplate, times(1)).getForEntity(userRequest, User.class);
    verify(response, times(1))
        .sendError(HttpStatus.BAD_REQUEST.value(),"UUID does not exist");
  }

}