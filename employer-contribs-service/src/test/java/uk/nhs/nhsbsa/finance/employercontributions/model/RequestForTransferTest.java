package uk.nhs.nhsbsa.finance.employercontributions.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.model.validation.ContributionsValidationGroup;
import com.nhsbsa.model.validation.SchedulePaymentValidationGroup;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Collections;
import java.util.Locale;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RequestForTransferTest {

  private static Validator validator;
  private RequestForTransfer.RequestForTransferBuilder requestForTransferBuilder;

  @BeforeClass
  public static void setUpValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Before
  public void before() {
    requestForTransferBuilder = RequestForTransfer
        .builder()
        .transferDate(tomorrowValidDate())
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20"))
        .transferDate(TransferFormDate.builder().build())
        .adjustmentsRequired(false)
        .totalDebitAmount(new BigDecimal("30"))
        .payAsSoonAsPossible("Y");

  }

  @Test
  public void allDataIsValid() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);

    assertThat(constraintViolations, hasSize(0));
  }

  @Test
  public void invalidTransferDateValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .payAsSoonAsPossible("N")
        .transferDate(
            TransferFormDate
                .builder()
                .days("111")
                .month("12")
                .year("2016")
                .build()
        )
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{validation.validFormDate}"))));
  }

  @Test
  public void blankTransferDateValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .payAsSoonAsPossible("N")
        .transferDate(
            TransferFormDate
                .builder()
                .days("")
                .month("")
                .year("")
                .build()
        )
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, SchedulePaymentValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{formDate.notBlank}"))));
  }

  @Test
  public void totalPensionablePayNotEnteredValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(null)
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.notNull}"))));
  }

  @Test
  public void employeeContributionsNotEnteredValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .employeeContributions(null)
        .totalDebitAmount(new BigDecimal("20"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employeeContributions.notNull}"))));
  }

  @Test
  public void employerContributionsNotEnteredValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .employerContributions(null)
        .totalDebitAmount(new BigDecimal("10"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employerContributions.notNull}"))));
  }

  @Test
  public void employeeContributionBelowThresholdValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("4.99"))
        .totalDebitAmount(new BigDecimal("24.99"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employee.contribution.threshold}"))));
  }

  @Test
  public void employeeContributionAboveThresholdValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("100"))
        .employeeContributions(new BigDecimal("14.51"))
        .totalDebitAmount(new BigDecimal("34.51"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employee.contribution.threshold}"))));
  }

  @Test
  public void employerContributionBelowThresholdValidationError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("100"))
        .employerContributions(new BigDecimal("13.99"))
        .totalDebitAmount(new BigDecimal("23.99"))
        .adjustmentList(Collections.singletonList(Adjustment.builder()
            .adjustmentContributionDate(ContributionDate.builder().build())
            .build()
        ))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer, ContributionsValidationGroup.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{employer.contribution.threshold}"))));
  }

  // Total pensionable pay/Employee contributions/Employer contributions/Employee added years/Additional pension/ERRBO are the same style
  // as use the @Digit, @Min and @Max annotation validations, so just used one for this testing, a few examples below.
  @Test
  public void totalPensionablePayBelowMinimumValueError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("0.99"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }

  @Test
  public void totalPensionablePayAboveMaximumValueError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("100000000"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }
  
  @Test
  public void totalPensionablePayInvalidDecimalValueError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("99.991"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }

  @Test
  public void totalPensionablePayNegativeValueError() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("-3.99"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{totalPensionablePay.invalid}"))));
  }

  @Test
  public void totalPensionablePayValidAmountValue() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .totalPensionablePay(new BigDecimal("12.99"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));
  }

  @Test
  public void getAdjustmentReturnsNullIfListIsNull() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .build();

    Adjustment actualAdjustment = requestForTransfer.getAdjustment();

    assertThat(actualAdjustment, is(nullValue()));
  }

  @Test
  public void getAdjustmentReturnsNullIfListIsEmpty() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .adjustmentList(Collections.EMPTY_LIST)
        .build();

    Adjustment actualAdjustment = requestForTransfer.getAdjustment();

    assertThat(actualAdjustment, is(nullValue()));
  }

  @Test
  public void getAdjustmentReturnsFirstItem() {
    final Adjustment expectedAdjustment = Adjustment.builder().build();
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .adjustmentList(Collections.singletonList(expectedAdjustment))
        .build();

    Adjustment actualAdjustment = requestForTransfer.getAdjustment();

    assertThat(actualAdjustment, is(sameInstance(expectedAdjustment)));
  }

  @Test
  public void setAdjustmentWithNullRemovesList() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .build();

    requestForTransfer.setAdjustment(null);

    assertThat(requestForTransfer.getAdjustmentList(), is(nullValue()));
  }

  @Test
  public void setAdjustmentOverwritesListWithValue() {
    final Adjustment adjustment = Adjustment.builder().build();
    final RequestForTransfer requestForTransfer = requestForTransferBuilder
        .build();

    requestForTransfer.setAdjustment(adjustment);

    assertThat(requestForTransfer.getAdjustmentList(), hasSize(1));
    assertThat(requestForTransfer.getAdjustmentList().get(0), is(sameInstance(adjustment)));
  }
  
	@Test
	public void adjustmentsRequiredShouldDefaultToFalseWhenNotSpecified() {
		/*Use of the lombok builder will correctly initialise the adjustmentRequired Boolean to false
		but Jackson mapping would result in adjustmentRequired being null if no value provided and a 
		NPE in isTotalDebitAmountValid() if adjustmentRequired is null on object creation.*/
		
		final RequestForTransfer requestForTransfer = new RequestForTransfer();
		
		assertNotNull(requestForTransfer.getAdjustmentsRequired());
		assertThat(requestForTransfer.getAdjustmentsRequired(), is(false));
	}

	@Test
	public void adjustmentsRequiredShouldBeTrueWhenSetToTrue() {

		final RequestForTransfer requestForTransfer = new RequestForTransfer();
		requestForTransfer.setAdjustmentsRequired(true);
		
		assertNotNull(requestForTransfer.getAdjustmentsRequired());
		assertThat(requestForTransfer.getAdjustmentsRequired(), is(true));
	}

  @Test
  public void addedYearsAdditionalPensionErrboAllowZero() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .employeeAddedYears(new BigDecimal("0"))
        .additionalPension(new BigDecimal("0"))
        .errbo(new BigDecimal("0"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));
  }

  @Test
  public void addedYearsAdditionalPensionErrboAllowLessThan1() {
    RequestForTransfer requestForTransfer = requestForTransferBuilder
        .employeeAddedYears(new BigDecimal("0.1"))
        .additionalPension(new BigDecimal("0.5"))
        .errbo(new BigDecimal("0.9"))
        .build();

    Set<ConstraintViolation<RequestForTransfer>> constraintViolations = validator
        .validate(requestForTransfer);
    assertThat(constraintViolations, hasSize(0));
  }

  private TransferFormDate tomorrowValidDate() {

    TransferFormDate transferFormDate = new TransferFormDate();
    final LocalDate localDate = LocalDate.now().plusDays(1);
    transferFormDate.setDays(Integer.toString(localDate.getDayOfMonth()));
    transferFormDate.setMonth(localDate.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH));
    transferFormDate.setYear(Integer.toString(localDate.getYear()));

    return transferFormDate;
  }

}
