package uk.nhs.nhsbsa.finance.employercontributions.model.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.view.RequestForTransferView;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

@RunWith(SpringJUnit4ClassRunner.class)
public class TransferFormDateNotBlankValidatorTest {

  private TransferFormDateNotBlankValidator test;
  private TransferFormDate date = new TransferFormDate();
  private ConstraintValidatorContext context;

  @Before
  public void initializeObject() {
    test = new TransferFormDateNotBlankValidator();
    context = Mockito.mock(ConstraintValidatorContext.class);

    final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder =
        Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);

    when(context.buildConstraintViolationWithTemplate(Mockito.anyString()))
        .thenReturn(constraintViolationBuilder);

    NodeBuilderCustomizableContext customCtx = mock(NodeBuilderCustomizableContext.class);

    doReturn(customCtx).when(constraintViolationBuilder).addPropertyNode(any());
    doReturn(context).when(customCtx).addConstraintViolation();
  }

  @Test
  public void isValidIsEmptyTest() throws Exception {
    date.setDays("");
    date.setMonth("");
    date.setYear("");
    RequestForTransfer rft = RequestForTransfer.builder().payAsSoonAsPossible("N").transferDate(
        TransferFormDate.builder().days("").month("").year("").build())
        .build();
    final boolean value = test.isValid(rft, context);
    assertThat(value, is(false));
  }

  @Test
  public void isValidEmptyTest() throws Exception {
    date.setDays("");
    date.setMonth("");
    date.setYear("");
    RequestForTransfer rft = RequestForTransfer.builder().payAsSoonAsPossible("Y").transferDate(
        TransferFormDate.builder().days("").month("").year("").build()
    ).build();
    final boolean value = test.isValid(rft, context);
    assertThat(value, is(true));
  }

  @Test
  public void isValidDayTest() throws Exception {
    date.setDays("28");
    date.setMonth("");
    date.setYear("");
    RequestForTransfer rft = RequestForTransfer.builder().payAsSoonAsPossible("N").transferDate(
        TransferFormDate.builder().days("28").month("").year("").build()
    ).build();
    final boolean value = test.isValid(rft, context);
    assertThat(value, is(true));
  }


}
