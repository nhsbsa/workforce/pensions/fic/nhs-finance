package uk.nhs.nhsbsa.finance.employercontributions.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.nhsbsa.view.ListWrappingView;
import com.nhsbsa.view.TransferView;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Validator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import uk.nhs.nhsbsa.finance.employercontributions.repository.RequestForTransferRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RequestForTransferRepository.class, CsvGeneratingService.class,
    Validator.class})
public class CsvGeneratingServiceTests {

  @Autowired
  CsvGeneratingService service;

  @Test
  public void given_single_view_when_csv_generated_then_file_created() throws IOException {
    TransferView source = TransferView.builder()
        .employerType("S")
        .accountCode("EA1234")
        .transactionDate(LocalDate.of(2017 - 1900, 4 - 1, 15))
        .contributionYearMonth("0616")
        .contributions(new BigDecimal("10.00"))
        .employerContributions(new BigDecimal("14.00"))
        .build();
    ListWrappingView view = ListWrappingView.builder().set(Collections.singletonList(source))
        .build();

    Optional<byte[]> file = service.generate(view);

    assertTrue(file.isPresent());
  }

  @Test
  public void given_single_view_with_field_errors_when_csv_generated_then_file_is_not_created()
      throws IOException {

    String wrongEACode = "EAEA123";

    TransferView source = TransferView.builder()
        .employerType("S")
        .accountCode(wrongEACode)
        .transactionDate(LocalDate.of(2017 - 1900, 4 - 1, 15))
        .contributionYearMonth("0616")
        .contributions(new BigDecimal("10.00"))
        .employerContributions(new BigDecimal("14.00"))
        .build();
    ListWrappingView view = ListWrappingView.builder().set(Collections.singletonList(source))
        .build();

    Optional<byte[]> file = service.generate(view);

    assertFalse(file.isPresent());
  }

  @Test
  public void given_month_year_when_date_generated_date_is_first_of_correct_month_and_year()
      throws ParseException {
    TransferRecord tr = new TransferRecord();

    Date date = tr.absoluteYearMonthToDate("1703");

    assertThat(date.getDate()).isEqualTo(1);    // day in month
    assertThat(date.getMonth()).isEqualTo(2);   // month in range 0-11
    assertThat(date.getYear()).isEqualTo(117);  // year - 1900
  }

  @Test
  public void given_March_2017_when_converted_to_contribution_date_then_result_is_1712()
      throws ParseException {
    TransferRecord tr = new TransferRecord();
    Date date = tr.absoluteYearMonthToDate("1703");

    String contributionDate = tr.dateToFinancialYearMonth(date);

    assertThat(contributionDate).isEqualTo("1712");
  }

  @Test
  public void given_April_2017_when_converted_to_contribution_date_then_result_is_1801()
      throws ParseException {
    TransferRecord tr = new TransferRecord();
    Date date = tr.absoluteYearMonthToDate("1704");

    String contributionDate = tr.dateToFinancialYearMonth(date);

    assertThat(contributionDate).isEqualTo("1801");
  }

  @Test
  public void given_minus_12_34_amount_when_base_amount_requested_then_result_is_positive_with_same_magnitude() {
    TransferRecord tr = new TransferRecord();

    String baseAmount = tr.currencyAmountToBaseAmount(new BigDecimal("-12.34"));

    assertThat(baseAmount).isEqualTo("12.34");
  }

  @Test
  public void given_plus_12_34_amount_when_base_amount_requested_then_result_is_positive_with_same_magnitude() {
    TransferRecord tr = new TransferRecord();

    String baseAmount = tr.currencyAmountToBaseAmount(new BigDecimal("12.34"));

    assertThat(baseAmount).isEqualTo("12.34");
  }

  @Test
  public void given_minus_amount_when_payment_type_requested_then_result_is_CRN() {
    TransferRecord tr = new TransferRecord();

    String type = tr.currencyAmountToPaymentType(new BigDecimal("-12.34"));

    assertThat(type).isEqualTo("CRN");
  }

  @Test
  public void given_plus_amount_when_payment_type_requested_then_result_is_INV() {
    TransferRecord tr = new TransferRecord();

    String type = tr.currencyAmountToPaymentType(new BigDecimal("12.34"));

    assertThat(type).isEqualTo("INV");
  }

  @Test
  public void given_type_R_and_not_adjustment_when_sales_analysis_requested_then_result_is_XR7100() {
    TransferRecord tr = new TransferRecord();

    String type = tr.salesAnalysisFromTypeAndAdjustment(ContributionType.R, false,
        "S");

    assertThat(type).isEqualTo("XR7100");
  }

  @Test
  public void given_type_B_and_adjustment_when_sales_analysis_requested_then_result_is_XR7203Adj() {
    TransferRecord tr = new TransferRecord();

    String type = tr.salesAnalysisFromTypeAndAdjustment(ContributionType.B, true,
        "S");

    assertThat(type).isEqualTo("XR7203Adj");
  }

  @Test
  public void given_listWrapper_when_converted_to_csv_records_then_result_is_as_expected() {
    TransferView source = TransferView.builder()
        .employerType("S")
        .accountCode("ABC1234")
        .transactionDate(LocalDate.of(2017, 4, 15))
        .contributionYearMonth("1704")
        .contributions(new BigDecimal("10.00"))
        .employerContributions(new BigDecimal("14.00"))
        .adjustmentYearMonth("1605")
        .adjustmentErrbo(new BigDecimal("1.50"))
        .adjustmentAddedYears(new BigDecimal("-2.75"))
        .build();

    List<TransferRecord> records = service.transferViewToRecords(source);

    assertThat(records.size()).isEqualTo(4);

    // Note: the order of results is dependent on the order the different amount
    // fields are checked in CsvGeneratingService.

    TransferRecord record = records.get(0);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.E);
    assertThat(record.baseAmount).isEqualTo("10.00");
    assertThat(record.baseAmountPlusMinus).isEqualTo("10.00");
    assertThat(record.paymentType).isEqualTo("INV");
    assertThat(record.contributionYearMonth).isEqualTo("1801");
    assertThat(record.salesAnalysis).isEqualTo("XR7200");
    assertThat(record.staffOrGP).isEqualTo("S");

    record = records.get(1);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.R);
    assertThat(record.baseAmount).isEqualTo("14.00");
    assertThat(record.baseAmountPlusMinus).isEqualTo("14.00");
    assertThat(record.paymentType).isEqualTo("INV");
    assertThat(record.contributionYearMonth).isEqualTo("1801");
    assertThat(record.salesAnalysis).isEqualTo("XR7100");
    assertThat(record.staffOrGP).isEqualTo("S");

    record = records.get(2);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.A);
    assertThat(record.baseAmount).isEqualTo("2.75");
    assertThat(record.baseAmountPlusMinus).isEqualTo("-2.75");
    assertThat(record.paymentType).isEqualTo("CRN");
    assertThat(record.contributionYearMonth).isEqualTo("1702");
    assertThat(record.salesAnalysis).isEqualTo("XR7201Adj");
    assertThat(record.staffOrGP).isEqualTo("S");

    record = records.get(3);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.B);
    assertThat(record.baseAmount).isEqualTo("1.50");
    assertThat(record.baseAmountPlusMinus).isEqualTo("1.50");
    assertThat(record.paymentType).isEqualTo("INV");
    assertThat(record.contributionYearMonth).isEqualTo("1702");
    assertThat(record.salesAnalysis).isEqualTo("XR7203Adj");
    assertThat(record.staffOrGP).isEqualTo("S");
  }

  @Test
  public void given_listWrapper_with_employerType_GP_when_converted_to_csv_records_then_result_is_as_expected() {
    TransferView source = TransferView.builder()
        .employerType("G")
        .accountCode("ABC1234")
        .transactionDate(LocalDate.of(2017, 4, 15))
        .contributionYearMonth("1704")
        .contributions(new BigDecimal("10.00"))
        .employerContributions(new BigDecimal("14.00"))
        .adjustmentYearMonth("1605")
        .adjustmentErrbo(new BigDecimal("1.50"))
        .adjustmentAddedYears(new BigDecimal("-2.75"))
        .build();

    List<TransferRecord> records = service.transferViewToRecords(source);

    assertThat(records.size()).isEqualTo(4);

    // Note: the order of results is dependent on the order the different amount
    // fields are checked in CsvGeneratingService.

    TransferRecord record = records.get(0);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.E);
    assertThat(record.baseAmount).isEqualTo("10.00");
    assertThat(record.baseAmountPlusMinus).isEqualTo("10.00");
    assertThat(record.paymentType).isEqualTo("INV");
    assertThat(record.contributionYearMonth).isEqualTo("1801");
    assertThat(record.salesAnalysis).isEqualTo("XR7210");
    assertThat(record.staffOrGP).isEqualTo("G");

    record = records.get(1);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.R);
    assertThat(record.baseAmount).isEqualTo("14.00");
    assertThat(record.baseAmountPlusMinus).isEqualTo("14.00");
    assertThat(record.paymentType).isEqualTo("INV");
    assertThat(record.contributionYearMonth).isEqualTo("1801");
    assertThat(record.salesAnalysis).isEqualTo("XR7110");
    assertThat(record.staffOrGP).isEqualTo("G");

    record = records.get(2);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.A);
    assertThat(record.baseAmount).isEqualTo("2.75");
    assertThat(record.baseAmountPlusMinus).isEqualTo("-2.75");
    assertThat(record.paymentType).isEqualTo("CRN");
    assertThat(record.contributionYearMonth).isEqualTo("1702");
    assertThat(record.salesAnalysis).isEqualTo("XR7211Adj");
    assertThat(record.staffOrGP).isEqualTo("G");

    record = records.get(3);
    assertThat(record.transactionDate).isEqualTo("15/04/2017");
    assertThat(record.contributionType).isEqualTo(ContributionType.B);
    assertThat(record.baseAmount).isEqualTo("1.50");
    assertThat(record.baseAmountPlusMinus).isEqualTo("1.50");
    assertThat(record.paymentType).isEqualTo("INV");
    assertThat(record.contributionYearMonth).isEqualTo("1702");
    assertThat(record.salesAnalysis).isEqualTo("XR7213Adj");
    assertThat(record.staffOrGP).isEqualTo("G");
  }

  @Test
  public void given_single_view_when_csv_generated_then_file_created_with_expected_contents()
      throws IOException {
    TransferView source = TransferView.builder()
        .employerType("S")
        .accountCode("EA1234")
        .transactionDate(LocalDate.of(2015, 4, 15))
        .contributionYearMonth("0417")
        .contributions(new BigDecimal("10.00"))
        .employerContributions(new BigDecimal("14.00"))
        .adjustmentYearMonth("0516")
        .adjustmentErrbo(new BigDecimal("1.50"))
        .adjustmentAddedYears(new BigDecimal("-2.75"))
        .build();
    ListWrappingView view = ListWrappingView.builder().set(Collections.singletonList(source))
        .build();

    String result = new String(service.generate(view).get());

    String expectedResult =
        "Account Code,Transaction Date,Base Amount,Base Amount(+/-),Payment Type,Reference,Description,Sales Analysis,Journal Source,Contribution Month,Staff/GP,Contribution Type\n"
            + "EA1234,15/04/2015,10.00,10.00,INV,EA12340602,JS: RFTOL REF: EA12340602 DES: Online Monthly Cont'tions Staff/GP: S,XR7200,RFTOL,0602,S,E\n"
            + "EA1234,15/04/2015,14.00,14.00,INV,EA12340602,JS: RFTOL REF: EA12340602 DES: Online Monthly Cont'tions Staff/GP: S,XR7100,RFTOL,0602,S,R\n"
            + "EA1234,15/04/2015,2.75,-2.75,CRN,EA12340701,JS: RFTOL REF: EA12340701 DES: Online Monthly Cont'tions Staff/GP: S,XR7201Adj,RFTOL,0701,S,A\n"
            + "EA1234,15/04/2015,1.50,1.50,INV,EA12340701,JS: RFTOL REF: EA12340701 DES: Online Monthly Cont'tions Staff/GP: S,XR7203Adj,RFTOL,0701,S,B\n";

    assertEquals(expectedResult, result);
  }

}
