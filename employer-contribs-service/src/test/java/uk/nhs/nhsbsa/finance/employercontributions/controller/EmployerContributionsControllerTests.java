package uk.nhs.nhsbsa.finance.employercontributions.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.test.context.ActiveProfiles;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;
import java.math.BigDecimal;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import uk.nhs.nhsbsa.finance.employercontributions.service.FinanceService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class EmployerContributionsControllerTests {

  private RequestForTransfer.RequestForTransferBuilder requestForTransferBuilder;

  @Autowired
  private TestRestTemplate restTemplate;

  @MockBean
  private FinanceService financeService;

  @Autowired
  private ObjectMapper objectMapper;


  @Test
  public void when_get_requestForTransfer_invoked_then_405_response_is_received() {
    ResponseErrorHandler originalErrorHandler =
        this.restTemplate.getRestTemplate().getErrorHandler();
    try {
      this.restTemplate.getRestTemplate().setErrorHandler(new DefaultResponseErrorHandler());
      ResponseEntity<RequestForTransfer> response =
          this.restTemplate.getForEntity("/requestfortransfer", RequestForTransfer.class);
    } catch (HttpClientErrorException e) {
      assertThat(e.getStatusCode()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);
      return;
    } finally {
      this.restTemplate.getRestTemplate().setErrorHandler(originalErrorHandler);
    }
    fail("Expected exception from GET; didn't receive one");
  }

  @Test
  public void when_get_requestForTransfer_invoked_with_valid_id_requestForTransfer_is_returned() {

    requestForTransferBuilder = RequestForTransfer.builder().rftUuid(UUID.randomUUID().toString())
        .totalPensionablePay(new BigDecimal("100")).employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20")).adjustmentsRequired(false)
        .totalDebitAmount(new BigDecimal("30"));

    RequestForTransfer requestForTransfer = requestForTransferBuilder.build();

    given(this.financeService.getRequestForTransferByRftUuid(requestForTransfer.getRftUuid()))
        .willReturn(requestForTransfer);

    ResponseEntity<RequestForTransfer> response = this.restTemplate.getForEntity(
        "/requestfortransfer/" + requestForTransfer.getRftUuid(), RequestForTransfer.class);
    assertNotNull(response);
    RequestForTransfer entity = response.getBody();
    assertNotNull(entity);
    assertThat(entity.getRftUuid()).isEqualTo(requestForTransfer.getRftUuid());

  }

  @Test
  public void when_get_requestForTransfer_invoked_with_invalid_id_no_content_status_is_returned() {

    String unknownUuid = UUID.randomUUID().toString();

    given(this.financeService.getRequestForTransferByRftUuid(unknownUuid)).willReturn(null);

    ResponseEntity<RequestForTransfer> response = this.restTemplate
        .getForEntity("/requestfortransfer/{rftId}", RequestForTransfer.class, unknownUuid);
    // assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT); //TODO currently returns ok
  }

  @Test
  public void given_post_RequestForTransfer_invoked_when_body_is_valid_and_with_null_id_RequestForTransfer_created() {

    requestForTransferBuilder =
        RequestForTransfer.builder()
                            .rftUuid(null)
                            .totalPensionablePay(new BigDecimal("100"))
                            .employeeContributions(new BigDecimal("10"))
                            .employerContributions(new BigDecimal("20"))
                            .adjustmentsRequired(false)
                            .totalDebitAmount(new BigDecimal("30"));

    RequestForTransfer requestForTransfer = requestForTransferBuilder.build();

    given(this.financeService.saveRequestForTransfer(any(RequestForTransfer.class)))
        .willReturn(requestForTransfer);

    ResponseEntity<RequestForTransfer> response = this.restTemplate
        .postForEntity("/requestfortransfer", requestForTransfer, RequestForTransfer.class);
    // assertThat(response.getHeaders().getLocation()).isNotNull(); //TODO currently doesn't return resource location
    // assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED); //TODO currently returns just OK.

    RequestForTransfer entity = response.getBody();
    assertNotNull(entity);

  }

  @Test
  public void given_post_RequestForTransfer_invoked_when_body_is_valid_and_with_non_null_id_RequestForTransfer_created() {

    requestForTransferBuilder = RequestForTransfer.builder().rftUuid(UUID.randomUUID().toString())
        .totalPensionablePay(new BigDecimal("100")).employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("20")).adjustmentsRequired(false)
        .totalDebitAmount(new BigDecimal("30"));

    RequestForTransfer requestForTransfer = requestForTransferBuilder.build();

    given(this.financeService.saveRequestForTransfer(any(RequestForTransfer.class)))
        .willReturn(requestForTransfer);

    ResponseEntity<RequestForTransfer> response = this.restTemplate
        .postForEntity("/requestfortransfer", requestForTransfer, RequestForTransfer.class);
    // assertThat(response.getHeaders().getLocation()).isNotNull(); //TODO currently doesn't return resource location
    // assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED); //TODO currently returns just OK.

    RequestForTransfer entity = response.getBody();
    assertNotNull(entity);
    assertNotNull(entity.getRftUuid());
    assertThat(entity.getRftUuid()).isEqualTo(requestForTransfer.getRftUuid()); // Existing UUID should not have been replaced.

  }

  @Test
  public void when_get_submitDate_invoked_with_valid_eaCode_date_is_returned() {

    String eaCode = "EA1111";
    LocalDate expectedDate = LocalDate.of(2018,8,15);

    given(this.financeService.getSubmitDateByEaCode(eaCode))
        .willReturn(Optional.of(expectedDate));

    ResponseEntity<LocalDate> response = this.restTemplate.getForEntity(
        "/submitdate/{eaCode}", LocalDate.class, eaCode);
    assertNotNull(response);
    LocalDate entity = response.getBody();
    assertNotNull(entity);
    assertThat(entity).isEqualTo(expectedDate);

  }

  @Test
  public void when_get_submitDate_invoked_with_invalid_eacode_no_content_status_is_returned() {

    String eaCode = "EA2222";

    when(financeService.getSubmitDateByEaCode(eaCode)).thenReturn(Optional.empty());
    ResponseEntity<?> response = this.restTemplate.getForEntity("/submitdate/{eaCode}", LocalDate.class, eaCode);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }

  /*
   * TODO Validation not finished
   * 
   * @Test public void given_post_organisation_invoked_when_body_is_not_valid_then_error_400()
   * throws IOException {
   * 
   * requestForTransferBuilder = RequestForTransfer .builder()
   * .rftUuid(UUID.randomUUID().toString()) .totalPensionablePay(new BigDecimal("100"))
   * .employeeContributions(new BigDecimal("10")) .employerContributions(new BigDecimal("20"))
   * .adjustmentsRequired(false); //.totalDebitAmount(null); //cannot be null
   * 
   * RequestForTransfer requestForTransfer = requestForTransferBuilder.build();
   * 
   * ResponseErrorHandler originalErrorHandler =
   * this.restTemplate.getRestTemplate().getErrorHandler();
   * 
   * try { this.restTemplate.getRestTemplate().setErrorHandler(new DefaultResponseErrorHandler());
   * ResponseEntity<RequestForTransfer> response =
   * this.restTemplate.postForEntity("/requestfortransfer", requestForTransfer,
   * RequestForTransfer.class); } catch (HttpClientErrorException e) {
   * assertThat(e.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST); return; } finally {
   * this.restTemplate.getRestTemplate().setErrorHandler(originalErrorHandler); }
   * fail("Expected exception from POST; didn't receive one"); }
   */

}
