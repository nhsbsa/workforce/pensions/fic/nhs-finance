package uk.nhs.nhsbsa.finance.employercontributions.service;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.nhsbsa.view.ListWrappingView;
import com.nhsbsa.view.TransferView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.validation.Validation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CsvGeneratingService {

  private Validator validator;

  private CsvMapper csvMapper;
  private CsvSchema csvSchema;

  public CsvGeneratingService() {

    this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    this.csvMapper = new CsvMapper();
    // assumption: the order of the fields is important
    // assumption: nothing is quoted (as in the sample)
    this.csvSchema = csvMapper.schemaFor(TransferRecord.class)
        .sortedBy("Account Code",
            "Transaction Date",
            "Base Amount",
            "Base Amount(+/-)",
            "Payment Type",
            "Reference",
            "Description",
            "Sales Analysis",
            "Journal Source",
            "Contribution Month",
            "Staff/GP",
            "Contribution Type")
        .withoutQuoteChar()
        .withHeader();
  }

  public Optional<byte[]> generate(ListWrappingView set) throws IOException {

    if (!isValidSet(set)) {
      log.error("Set with invalid values");
      return Optional.empty();
    }

    List<TransferRecord> records = this.transform(set);
    return Optional.ofNullable(write(records).toByteArray());
  }

  private boolean isValidSet(ListWrappingView set) {

    return validator.validate(set).isEmpty();

  }

  private List<TransferRecord> transform(ListWrappingView set) {
    return set.getSet().stream()
        .flatMap(v -> transferViewToRecords(v).stream())
        .collect(Collectors.toList());
  }

  /**
   * Each contribution in {@code view} that is present is converted into a transfer record instance;
   * these instances are then written into the csv file.
   */
  public List<TransferRecord> transferViewToRecords(final TransferView view) {

    final List<TransferRecord> records = new ArrayList<>();

    /**
     * Create a record for the csv file from the data item from the view.
     * Closes over {@code records} and {@code view}.
     * <br>
     * Could be a functional interface + lambda
     *
     */
    @SuppressWarnings("squid:S1610")
    abstract class TransferViewToRecordAdder {

      abstract void addIfPresent(Predicate<TransferView> predicate,
          Function<TransferView, BigDecimal> function, boolean isAdjustment, ContributionType type);
    }

    TransferViewToRecordAdder recordAdder = new TransferViewToRecordAdder() {
      @Override
      void addIfPresent(Predicate<TransferView> predicate,
          Function<TransferView, BigDecimal> accessor,
          boolean isAdjustment,
          ContributionType type) {
        if (predicate.test(view)) {
          BigDecimal amount = accessor.apply(view);
          TransferRecord record = TransferRecord
              .makeTransferRecordFrom(view, amount, type, isAdjustment);
          records.add(record);
        }
      }
    };

    recordAdder
        .addIfPresent(TransferView::hasContributionsValue, TransferView::getContributions, false,
            ContributionType.E);
    recordAdder.addIfPresent(TransferView::hasEmployerContributions,
        TransferView::getEmployerContributions, false, ContributionType.R);
    recordAdder
        .addIfPresent(TransferView::hasAdditionalPension, TransferView::getAdditionalPension, false,
            ContributionType.D);
    recordAdder.addIfPresent(TransferView::hasAddedYears, TransferView::getAddedYears, false,
        ContributionType.A);
    recordAdder
        .addIfPresent(TransferView::hasErrbo, TransferView::getErrbo, false, ContributionType.B);

    recordAdder.addIfPresent(TransferView::hasAdjustmentContributionsValue,
        TransferView::getAdjustmentContributions, true, ContributionType.E);
    recordAdder.addIfPresent(TransferView::hasAdjustmentEmployerContributions,
        TransferView::getAdjustmentEmployerContributions, true, ContributionType.R);
    recordAdder.addIfPresent(TransferView::hasAdjustmentAdditionalPension,
        TransferView::getAdjustmentAdditionalPension, true, ContributionType.D);
    recordAdder
        .addIfPresent(TransferView::hasAdjustmentAddedYears, TransferView::getAdjustmentAddedYears,
            true, ContributionType.A);
    recordAdder
        .addIfPresent(TransferView::hasAdjustmentErrbo, TransferView::getAdjustmentErrbo, true,
            ContributionType.B);

    return records;
  }

  private ByteArrayOutputStream write(Collection<TransferRecord> records) throws IOException {

    log.info("Writing " + records.size() + " entries to RFT1 file ");
    final ByteArrayOutputStream stream = new ByteArrayOutputStream();
    ObjectWriter writer = csvMapper.writer(csvSchema);
    writer.writeValue(stream, records);
    log.info("Finished writing RTF1 file");
    return stream;
  }

}
