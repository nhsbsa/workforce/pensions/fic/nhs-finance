package uk.nhs.nhsbsa.finance.employercontributions.utility;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
//TODO Change Domain location i.e. to uk.nhs.nhsbsa... so can re-use ExampleDateGenerator instead of this.
@Component
@PropertySource("classpath:dates.properties")
public class FileDateGenerator {

  private List<String> excludedDates;

  private int deadlineHour;

  public FileDateGenerator(
      @Value("#{'${bankholiday.excluded.dates}'.split(',')}") List<String> excludedDates,
      @Value("${paymentdate.deadline.hour:13}")  int deadlineHour) {
    this.excludedDates = excludedDates;
    this.deadlineHour = deadlineHour;
  }

  private static final String DAY_MONTH_FORMAT = "dd/MM";

  private DateTimeFormatter dayMonthFormatter = DateTimeFormatter.ofPattern(DAY_MONTH_FORMAT);


  public LocalDate getNearestWorkingDay(LocalDate date, int minWorkingDaysOffset) {
    
    int direction = minWorkingDaysOffset >= 0 ? 1 : -1;
    
    while ((!isWorkingDay(date)) || (minWorkingDaysOffset != 0)) {
      date = date.plusDays(direction);

      if (isWorkingDay(date) && minWorkingDaysOffset != 0) minWorkingDaysOffset += -direction;
    }
      
    return date;
  }
  
  public boolean isWorkingDay(LocalDate date) {
    DayOfWeek dayOfWeek = date.getDayOfWeek();
    if (DayOfWeek.SUNDAY.equals(dayOfWeek) || DayOfWeek.SATURDAY.equals(dayOfWeek)) {
      return false;
    }
    return !isExcludedDate(date);
  }
  
  public boolean isExcludedDate(LocalDate date) {
	  return (excludedDates != null && excludedDates.contains(date.format(dayMonthFormatter)));
  }

  public boolean isAfterDeadlineHour() {
    return now().getHour() >= deadlineHour;
  }

  public LocalTime now() {
    return LocalTime.now();
  }


}