package uk.nhs.nhsbsa.finance.employercontributions.repository;

import java.time.Instant;
import java.util.Set;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

public interface RequestForTransferRepository extends PagingAndSortingRepository<RequestForTransfer, Long> {

    RequestForTransfer findByRftUuid (String rftUuid);

    Set<RequestForTransfer> findByCsvProcessedDateIsNullAndSubmitDateIsNotNull();

    //if todayHour > 1pm condition is (submitdate <= today 13pm), else (submitdate <= today -1day at 13pm)
    @Query(value = "SELECT DISTINCT CASE WHEN date_part('hour',submit_date) >= 13 THEN to_char((date_trunc('day',submit_date + interval '1 day')), 'YYYY-MM-dd') ELSE to_char(submit_date, 'YYYY-MM-dd') END as dates FROM request_for_transfer WHERE (date_part('hour',now()) >= '13' AND submit_date < (date_trunc('day',now()) + time '13:00')) OR (date_part('hour',now()) < '13' AND submit_date < (date_trunc('day',now()) - interval '1 day' + time '13:00')) AND submit_date IS NOT NULL ORDER BY dates DESC LIMIT 31", nativeQuery = true)
    String[] getCsvFileList();

    @Query(value = "SELECT * FROM request_for_transfer WHERE submit_date BETWEEN (date_trunc('day',to_date(:previousDate,'YYYY-MM-DD')) + time '13:00') AND (date_trunc('day',to_date(:fileName,'YYYY-MM-DD')) + time '13:00')", nativeQuery = true)
    Set<RequestForTransfer> findByFormattedCsvProcessedDate(@Param("fileName") String fileName, @Param("previousDate") String previousDate);

    @Query(value = "SELECT MAX(submit_date) FROM request_for_transfer WHERE ea_code = :eaCode", nativeQuery = true)
    Instant findSubmitDateByEaCode(@Param("eaCode") String eaCode);

}