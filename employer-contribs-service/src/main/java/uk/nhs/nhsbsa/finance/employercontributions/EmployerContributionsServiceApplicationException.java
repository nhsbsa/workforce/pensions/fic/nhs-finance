package uk.nhs.nhsbsa.finance.employercontributions;

/**
 * Custom exception thrown when any exceptional circumstance is encountered and
 * specific error handling of that event is required.
 */

public class EmployerContributionsServiceApplicationException extends RuntimeException {

    public EmployerContributionsServiceApplicationException() {
        super();
    }

    public EmployerContributionsServiceApplicationException(String message) {
        super(message);
    }
}
