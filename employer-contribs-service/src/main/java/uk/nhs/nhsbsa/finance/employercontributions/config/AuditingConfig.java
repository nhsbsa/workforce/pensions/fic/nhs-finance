package uk.nhs.nhsbsa.finance.employercontributions.config;

import com.nhsbsa.audit.FinanceAuditorProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
@Profile("!test")
public class AuditingConfig {
  @Bean
  public AuditorAware<String> auditorProvider() {
    return new FinanceAuditorProvider();
  }
}
