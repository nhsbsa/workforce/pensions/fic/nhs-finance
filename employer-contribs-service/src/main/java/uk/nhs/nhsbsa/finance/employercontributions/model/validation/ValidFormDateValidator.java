package uk.nhs.nhsbsa.finance.employercontributions.model.validation;

import com.nhsbsa.utility.TransferFormDateFormat;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

public class ValidFormDateValidator implements ConstraintValidator<ValidFormDate, RequestForTransfer> {

  private String dateNotBlank;

  private TransferFormDateFormat transferFormDateFormat = new TransferFormDateFormat();

  @Override
  public void initialize(ValidFormDate constraintAnnotation) {
    dateNotBlank = constraintAnnotation.message();
  }

  @Override
  public boolean isValid(RequestForTransfer rft, ConstraintValidatorContext context) {

    if ("Y".equals(rft.getPayAsSoonAsPossible()) || StringUtils.isEmpty(rft.getTransferDate().getDays()) &&
        StringUtils.isEmpty(rft.getTransferDate().getMonth()) &&
        StringUtils.isEmpty(rft.getTransferDate().getYear())) {
      return true;
    }

      if(!transferFormDateFormat.isValidDate(rft.getTransferDate())) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(dateNotBlank)
          .addPropertyNode("transferDate")
          .addConstraintViolation();
      return false;
    }
    return true;
  }

}
