package uk.nhs.nhsbsa.finance.employercontributions.model.validation;

import java.math.BigDecimal;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;

/**
 * Created by Mark Lishman on 18/11/2016.
 */
public class EmployerContributionThresholdValidator implements
    ConstraintValidator<EmployerContributionThreshold, RequestForTransfer> {

  private static final BigDecimal MINIMUM_PERCENT = new BigDecimal("0.1433");

  @Override
  public void initialize(EmployerContributionThreshold constraint) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public boolean isValid(RequestForTransfer rft, ConstraintValidatorContext context) {
    if (rft.getTotalPensionablePay() == null || rft.getEmployerContributions() == null) {
      return true;
    }
    final BigDecimal minimumValue = rft.getTotalPensionablePay().multiply(MINIMUM_PERCENT);


    boolean isValid = rft.getEmployerContributions().compareTo(minimumValue) >= 0;
    if (!isValid) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()).addPropertyNode("employerContributions").addConstraintViolation();
    }

    return isValid;
  }
}
