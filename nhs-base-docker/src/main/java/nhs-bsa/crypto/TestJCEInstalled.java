package nhsbsa.crypto;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;

public class TestJCEInstalled
{
    public static void main( String[] args )
    {
        System.out.println ("JCE Support Installed : " + testJCEInstalled());
    }

	private static boolean testJCEInstalled() {
		try {
			int length = Cipher.getMaxAllowedKeyLength("AES");
			boolean unlimited = (length==Integer.MAX_VALUE);
			return unlimited;
		} catch (NoSuchAlgorithmException e) {
		}
		return false;
	}
}

