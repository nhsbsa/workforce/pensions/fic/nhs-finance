#!/bin/bash

#set -x

DOCKER_MACHINE_NAME="default"

declare -a APPS=(
  "./authorization"
  "./employer-details-service"
  "./employer-contribs-service"
  "./nhs-finance/employer-service"
  "./nhs-finance/employer-website"
  "./finance-facade"
)

declare -a SHARED_REPO_URL=(
  "git@dps-gitlab.service.nhsbsa:pensions/NHS-Shared/authorization.git"
  "git@dps-gitlab.service.nhsbsa:pensions/FIC/nhs-base-docker.git"
)

declare -a SERVICES_REPO_URL=(
  "git@dps-gitlab.service.nhsbsa:pensions/FIC/employer-details-service.git"
  "git@dps-gitlab.service.nhsbsa:pensions/FIC/employer-contribs-service.git"
  "git@dps-gitlab.service.nhsbsa:pensions/FIC/finance-facade.git"
)

function git_branch_pull {
    git status | head -1
    git fetch --all --tags --prune
    git pull
}

function pull {
  echo "Updating..."

  echo ">>>>>>>> UPDATING nhs-finance <<<<<<<<<<<"
  git_branch_pull
}

function build  {
  echo "Building..."
  docker-machine start "$DOCKER_MACHINE_NAME"
  eval "$(docker-machine env $DOCKER_MACHINE_NAME)"

  echo "=========== Build database ==========="
  pushd . >> /dev/null
  cd nhs-finance/database
  mvn clean install docker:build || { echo "Maven tasks failed"; exit 1; }
  popd >> /dev/null


  echo "=========== Build dependencies ==========="
  mvn clean install || { echo "Maven tasks failed"; exit 1; }

}

function runall {
  echo "Running"
  docker-machine start "$DOCKER_MACHINE_NAME"
  eval "$(docker-machine env $DOCKER_MACHINE_NAME)"

  echo "Starting docker"
  docker run -d -p5432:5432 --name finance-databases nhs-bsa/finance-databases
  echo "Wait until database will be accessible on port 5432"
  while ! nc -z "$(docker-machine ip $DOCKER_MACHINE_NAME)" 5432; do
    sleep 1
  done
  pushd . >> /dev/null

  echo "Update FinanceDB"
  mvn -f "./employer-contribs-service/liquibase/pom.xml" -Dliquibase.url="jdbc:postgresql://$(docker-machine ip $DOCKER_MACHINE_NAME):5432/finance" liquibase:update

  echo "Update AuthDB"
  mvn -f "./authorization/authorization-liquibase/pom.xml" -Dliquibase.url="jdbc:postgresql://$(docker-machine ip $DOCKER_MACHINE_NAME):5432/authentication" liquibase:update

  echo "Update EmployerDB"
  mvn -f "./employer-details-service/liquibase/pom.xml" -Dliquibase.url="jdbc:postgresql://$(docker-machine ip $DOCKER_MACHINE_NAME):5432/employer" liquibase:update@employer-database

  for APP in "${APPS[@]}"
  do
    DIR_NAME=$(basename ${APP})
    echo ">>>>>>>> STARTING ${DIR_NAME} <<<<<<<<<<<"

    if [ -d "$APP" ]; then
      echo ">>>>>>>> ACCESSING $APP/target <<<<<<<<<<<"

      pushd . >> /dev/null
      cd "$APP"
      nohup java -jar $(ls target/*.jar) > app.log & echo $! > PID
      popd >> /dev/null
      sleep 5
    fi
  done

  echo ""
  echo "##########################################################"
  echo "Make sure you have this line on your hosts file: 127.0.0.1       authorization backend-api employer-details-service finance-facade employer-service employer-website employer-contributions-service"
  echo "##########################################################"

  echo "Wait until website will be accessible on port 8080"
  while ! nc -z "localhost" 8080; do
    sleep 2
  done

  if [[ "$OSTYPE" == "darwin"* ]]; then
      open "http://localhost:8080/employer-website/"
  fi
}

function stopall {
  echo "Stopping services..."

  for APP in "${APPS[@]}"
  do
    DIR_NAME=$(basename ${APP})
    echo ">>>>>>>> STOPPING ${DIR_NAME} <<<<<<<<<<<"

    if [ -d "$APP" ]; then
      pushd . >> /dev/null
      cd "$APP"
      kill -9 `cat PID`
      rm PID
      popd >> /dev/null
    fi
  done

  docker stop finance-databases
  echo "Services stopped. Did not stop docker machine. To stop machine run: docker-machine stop $DOCKER_MACHINE_NAME"
}

function checkout {
  echo "Checking out to $1 ..."

  echo ">>>>>>>> CHECKING OUT nhs-finance $1 <<<<<<<<<<<"
  git checkout "$1"
}

function checkoutTag {
  echo "Checking tag out to $1 ..."

  echo ">>>>>>>> CHECKING OUT TAG nhs-finance $1 <<<<<<<<<<<"
  git checkout "tags/$1"
}

function tag {

  DATE_TIME=`date '+%H:%M:%S %d-%m-%Y'`
  echo ">>>>>>>> Tagging nhs-finance $1 <<<<<<<<<<<"
  git tag "$1" -a -m "$DATE_TIME $1"
  git push --tags
}

function openLogs {
  open app.log

  for APP in "${APPS[@]}"
  do
    DIR_NAME=$(basename ${APP})
    if [ -d "$APP" ]; then
      pushd . >> /dev/null
      open "$APP/app.log"
      popd >> /dev/null
    fi
  done
}

function package {
  rm -rf ./build
  mkdir -p ./build
  cp dev.run.sh ./build/run.sh

  for APP in "${APPS[@]}"
  do
    APP_NAME=$(basename ${APP})
    if [ -d "$APP" ]; then
      pushd . >> /dev/null
      cd "$APP/target/"
      if [[ $(ls -A *.jar) ]]; then
        APP_DIR="$(pwd)"
        JAR_NAME="$(ls *.jar)"
        popd >> /dev/null
        mkdir "./build/$APP_NAME"
        cp "$APP_DIR/$JAR_NAME" "./build/$APP_NAME"
        cp "./dev.logback.xml" "./build/$APP_NAME/logback.xml"
        touch "./build/$APP_NAME/application.properties"
      else
        echo "Jar file for app $APP_NAME not found"
        exit 1
      fi

    fi
  done

  tar -zcvf "./fic.tar.gz" ../build

  rm -rf ./liquibase
  mkdir -p ./liquibase

  cp -ri ./authorization/authorization-liquibase/src/ ../liquibase/authentication/
  mkdir ./liquibase/authentication/src
  mv ./liquibase/authentication/main ../liquibase/authentication/src

  cp -ri ./nhs-finance/liquibase/src/ ./liquibase/finance/
  mkdir ./liquibase/finance/src
  mv ./liquibase/finance/main ../liquibase/finance/src

  cp -ri ./employer-details-service/liquibase/src/ ./liquibase/employer/
  mkdir ./liquibase/employer/src
  mv ./liquibase/employer/main ../liquibase/employer/src

  tar -zcvf "./db.tar.gz" ../liquibase

}

function usage {
  echo "Usage: $0 [-u] [-b] [-r] [-a] [-o <branch name>] [-c <tag name>] [-t <tag name>] [-s]" 1>&2;
  echo """
    -o  <branch name>   Checkout/change repo to branch <branch name>, i.e. git checkout <branch name>
    -c  <tag name>      Checkout/change repo to tag <tag name>
    -t  <tag name>      Tag all repositories with tag name
    -u                  Update repositories, i.e git pull
    -b                  Build with dependencies
    -r                  Run project
    -a                  Initialise, update, build and run
    -s                  Stop project
    -g                  Open the logs of all the apps
    -p                  Package services
"""
}

while getopts "hlubraso:c:t:gp" opt; do
  case $opt in
    l)
      listall >&2
      ;;
    \?)
      usage;
      exit
      ;;
    h)
      usage;
      exit
      ;;
    u)
      pull >&2
      ;;
    b)
      build >&2
      ;;
    r)
      runall >&2
      ;;
    s)
      stopall >&2
      ;;
    a)
      init >&2
      pull >&2
      build >&2
      runall
      ;;
    o)
      echo "!!!!! ${OPTARG}"
      checkout "${OPTARG}" >&2
      ;;
    c)
      echo "!!!!! ${OPTARG}"
      checkoutTag "${OPTARG}" >&2
      ;;
    t)
      echo "!!!!! ${OPTARG}"
      tag "${OPTARG}" >&2
      ;;
    g)
      openLogs >&2
      ;;
    p)
      package >&2
      ;;
  esac
done
if [ $OPTIND -eq 1 ]; 
	then usage; 
	exit;
fi
