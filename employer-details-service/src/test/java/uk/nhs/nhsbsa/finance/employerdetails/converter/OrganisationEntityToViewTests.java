package uk.nhs.nhsbsa.finance.employerdetails.converter;

import com.nhsbsa.model.EmployerTypes;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import org.mockito.Mock;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;
import com.nhsbsa.view.OrganisationView;

import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.ValidatorFactory;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Unit tests for organisation entity --> view converter
 *
 * Created by duncan on 17/03/2017.
 */
public class OrganisationEntityToViewTests {


    private OrganisationEntityToView converter;

    @Mock
    private EmployingAuthorityEntityToView eaConverter;

    public OrganisationEntityToViewTests() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.eaConverter = mock(EmployingAuthorityEntityToView.class);
        this.converter = new OrganisationEntityToView(vf.getValidator(), eaConverter);
    }

    @Test
    public void given_null_organisation_entity_then_return_null() {

        OrganisationView view = converter.convert(null);

        assertThat(view).isNull();
    }

    @Test
    public void given_valid_organisation_entity_when_converted_then_view_generated() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .name("Organisation: name")
                .eas(Collections.emptyList())
                .uuid(UUID.randomUUID().toString())
                .build();

        OrganisationView view = converter.convert(organisationEntity);

        assertThat(view).isNotNull();
    }

    /// Org name - not blank, max length 1000

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_name_missing_then_validation_exception() {
        final EmployingAuthorityEntity ea = EmployingAuthorityEntity.builder().eaCode("EA123456").name("Organisation: name").employerType(EmployerTypes.STAFF.name()).build();
        final List<EmployingAuthorityEntity> eaList = Collections.singletonList(ea);

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .eas(eaList)
                .uuid(UUID.randomUUID().toString())
                .build();

        OrganisationView view = converter.convert(organisationEntity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_name_empty_then_validation_exception() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .eas(Collections.emptyList())
                .name("")
                .uuid(UUID.randomUUID().toString())
                .build();

        OrganisationView view = converter.convert(organisationEntity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_org_entity_when_name_999_len_then_validation_ok() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .eas(Collections.emptyList())
                .name(makeStringOfLength(999))
                .uuid(UUID.randomUUID().toString())
                .build();

        OrganisationView view = converter.convert(organisationEntity);

        assertThat(view).isNotNull();
    }

    @Test
    public void given_org_entity_when_name_1000_len_then_validation_ok() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .eas(Collections.emptyList())
                .name(makeStringOfLength(1000))
                .uuid(UUID.randomUUID().toString())
                .build();

        OrganisationView view = converter.convert(organisationEntity);

        assertThat(view).isNotNull();
    }

    @Test(expected = ValidationException.class)
    public void given_org_entity_when_name_1001_len_then_validation_exception() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
                .eas(Collections.emptyList())
                .name(makeStringOfLength(1001))
                .uuid(UUID.randomUUID().toString())
                .build();

        OrganisationView view = converter.convert(organisationEntity);

        assertThat(view).isNotNull();
    }


    private String makeStringOfLength(int length) {
        String source = "abcdefghij";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i += 1) {
            builder.append(source.charAt(i % 10));
        }
        return builder.toString();
    }
}