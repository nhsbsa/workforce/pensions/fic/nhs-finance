package uk.nhs.nhsbsa.finance.employerdetails.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;

public interface OrganisationRepository extends JpaRepository<OrganisationEntity, Long> {

    OrganisationEntity findByUuid(String uuid);

    @Query("SELECT org FROM OrganisationEntity org join org.eas ea WHERE ea.eaCode = :eaCode")
    List<OrganisationEntity> findByEaCode(@Param("eaCode") String eaCode);

    OrganisationEntity findByNameIgnoreCaseContaining(String name);
    
}
