package uk.nhs.nhsbsa.finance.employerdetails.controller;

import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import uk.nhs.nhsbsa.finance.employerdetails.EmployerDetailsException;
import uk.nhs.nhsbsa.finance.employerdetails.service.EmployerDetailsService;
import com.nhsbsa.view.OrganisationView;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView;

/**
 * Employer Details service endpoints.
 *
 * Created by duncan on 08/03/2017.
 */
@RestController
@Slf4j
public class EmployerDetailsController {

    private EmployerDetailsService service;

    public EmployerDetailsController(EmployerDetailsService service) {
        this.service = service;
    }

    @GetMapping(value = "/organisation/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
            value = "Query for Organisation",
            notes = "Request the resource of the Organisation with the supplied id."
    )
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No organisation with id provided")
    })
    public ResponseEntity<OrganisationView> getOrganisationById(
            @ApiParam(name = "id", value = "Identifier of Organisation being queried") @PathVariable UUID id
    ) {

        log.info("Searching for organisation with id = {}", id.toString());
        Optional<OrganisationView> optionalView = service.findOrganisation(id);
        if (optionalView.isPresent()) {
            return ResponseEntity.ok(optionalView.get());
        }
        return ResponseEntity.noContent().build();
    }
    
    @GetMapping(value = "/organisation/ea-code/{eaCode}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
            value = "Query for Organisation",
            notes = "Request the resource of the Organisation with the supplied eaCode."
    )
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No organisation with eaCode found.")
    })
    public ResponseEntity<OrganisationView> getOrganisationByEaCode(
            @ApiParam(name = "eaCode", value = "Identifier of Organisation being queried") @PathVariable String eaCode
    ) {

        log.info("Searching for organisation with EA Code = {}", eaCode);
        Optional<OrganisationView> optionalView = service.findOrganisationByEaCode(eaCode);
        if (optionalView.isPresent()) {
            return ResponseEntity.ok(optionalView.get());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/user/{userUuid}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
            value = "Query for User",
            notes = "Request the resource of the Organisation with the supplied uuid."
    )
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No user in organisation with ids provided")
    })
    public ResponseEntity<UserView> getUserByUuId(
            @ApiParam(name = "userId", value = "Identifier of User being queried") @PathVariable UUID userUuid
    ) {

        log.info("Searching for user with uuid = {}", userUuid);
        Optional<UserView> optionalView = service.findUser(userUuid);
        if (optionalView.isPresent()) {
            return ResponseEntity.ok(optionalView.get());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/org-users/{orgUuid}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Query to find Users by Organisation Uuid",
        notes = "Request the resource of the Finance User by Organisation with supplied orgUuid."
    )
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "No users with organisation Uuid provided")
    })
    public ResponseEntity<List<UserView>> getUsesrByOrgUuId(
        @ApiParam(name = "orgUuId", value = "Identifier of Organisation for users being queried") @PathVariable UUID orgUuid
    ) {

        log.info("Searching for users with orgUuid = {}", orgUuid);
        Optional<List<UserView>> optionalView = service.findUserByOrgUuid(orgUuid);

        return optionalView.map(u -> ResponseEntity.ok(optionalView.get()))
            .orElse( ResponseEntity.noContent().build());
    }

    @GetMapping(value = "/employer-type/{eaCode}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Query to find EmployerType by EaCode",
        notes = "Request the resource of the EmployingAuthority by supplied eaCode."
    )
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "No employerType with eaCode provided")
    })
    public ResponseEntity<String> getempTypeByEaCode(
        @ApiParam(name = "eaCode", value = "Identifier of EmployingAuthority being queried") @PathVariable String eaCode
    ) {

        log.info("Searching for employerType with eaCode = {}", eaCode);
        Optional<String> optionalResult = service.findEmployerTypeByEaCode(eaCode);


        return optionalResult.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());
    }


    @PostMapping(value = "/organisation", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
            value = "Create an Organisation",
            notes = "Create a persistent Organisation record based off the prototype"
                  + " instance provided."
    )
    public ResponseEntity<OrganisationView> createOrganisation(
            @ApiParam(name = "organisationDetails", value = "Prototype for Organisation to create")
            @RequestBody
            @Valid CreateOrganisationRequest organisationRequest,
            BindingResult bindingResult,
            UriComponentsBuilder uriComponentsBuilder
    ) {

        log.info("Creating Organisation with EA code = {}", organisationRequest.getEas().get(0).getEaCode());
        OrganisationView organisation = service.storeOrganisation(organisationRequest);

        return ResponseEntity.ok(organisation);
    }

    @PostMapping(value = "/organisation/{organisationId}/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
            value = "Create a User within an Organisation",
            notes = "Create a persistent User record based off the prototype instance"
                    + " provided. The User will be related to the Organisation with"
                    + " the organisation id supplied."
    )
    public ResponseEntity<UserView> createUserWithinOrganisationById(
            @ApiParam(name = "organisationId", value = "Identifier of Organisation being queried")
            @PathVariable UUID organisationId,
            @ApiParam(name = "userDetails", value = "Prototype for User to create")
            @RequestBody
            @Valid UserView userDetails,
            BindingResult bindingResult,
            UriComponentsBuilder uriComponentsBuilder
    ) {


        log.info("Creating user within organisation with org uuid = {}", organisationId.toString());
        checkBindingResult(bindingResult, "UserView");
        UserView view = service.storeUser(userDetails);
        URI locationUri = uriComponentsBuilder.path("/organisation/")
                .path(organisationId.toString())
                .path("/user/")
                .path(view.getIdentifier().toString())
                .build()
                .toUri();
        return ResponseEntity.created(locationUri).body(view);
    }

    @PostMapping(value = "/organisation/{organisationId}/standard-user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Create a User within an Organisation",
        notes = "Create a persistent User record based off the prototype instance"
            + " provided. The User will be related to the Organisation with"
            + " the organisation id supplied."
    )

    public ResponseEntity<CreateUserResponse> createUserWithinOrganisationByEaCode(
        @ApiParam(name = "organisationId", value = "Identifier of Organisation being queried")
        @PathVariable UUID organisationId,
        @ApiParam(name = "userDetails", value = "Prototype for User to create")
        @RequestBody
        @Valid CreateUserRequest userDetails
    ) {

        //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
        log.info("Creating user {} within organisation Id = {}", userDetails.getUsername(), userDetails.getOrganisations().get(0).getIdentifier());
        CreateUserResponse response = service.storeUser(userDetails);
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "/organisation-no-ea", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Create an Organisation without ea",
        notes = "Create a persistent Organisation record based off the prototype"
            + " instance provided."
    )
    public ResponseEntity<CreateOrganisationResponse> createOrganisationWithoutEa(
        @ApiParam(name = "organisationDetails", value = "Prototype for Organisation to create")
        @RequestBody
        @Valid CreateOrganisationRequest organisationRequest,
        BindingResult bindingResult,
        UriComponentsBuilder uriComponentsBuilder
    ) {

        log.info("Creating Organisation");
        Optional<CreateOrganisationResponse> organisation = service.storeOrganisationWithoutEa(organisationRequest);

        return organisation.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());
    }

    @PostMapping(value = "/employing-authority", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Create an EA within an Organisation",
        notes = "Create a persistent EA record based off the prototype"
            + " instance provided."
    )
    public ResponseEntity<OrganisationView> createEa(
        @ApiParam(name = "eaDetails", value = "Prototype for EA to create")
        @RequestBody
        @Valid CreateEaRequest eaRequest,
        BindingResult bindingResult,
        UriComponentsBuilder uriComponentsBuilder
    ) {

        log.info("Creating EA");
        Optional<OrganisationView> organisation = service.storeEa(eaRequest);

        return organisation.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());
    }

    @PostMapping(value = "/organisation-name", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Query for Organisation",
        notes = "Request the resource of the Organisation with the supplied name."
    )
    public ResponseEntity<String> getOrganisationByName(
        @ApiParam(name = "orgDetails", value = "Prototype for Organisation")
        @RequestBody
        @Valid CreateOrganisationRequest orgRequest,
        BindingResult bindingResult,
        UriComponentsBuilder uriComponentsBuilder
    ) {

        log.info("Searching for organisation with name = {}", orgRequest.getName());
        Optional<String> optionalView = service.findOrganisationByName(orgRequest.getName());
        if (optionalView.isPresent()) {
            return ResponseEntity.ok(optionalView.get());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/employing-authority/{eaCode}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
        value = "Query to find ea by EaCode",
        notes = "Request the resource of the EmployingAuthority by supplied eaCode."
    )
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "No ea with eaCode provided")
    })
    public ResponseEntity<String> getEaByEaCode(
        @ApiParam(name = "eaCode", value = "Identifier of EmployingAuthority being queried") @PathVariable String eaCode
    ) {

        log.info("Searching for ea with eaCode = {}", eaCode);
        Optional<String> optionalResult = service.findEaByCode(eaCode);


        return optionalResult.map(u -> ResponseEntity.ok(u)).orElse( ResponseEntity.noContent().build());
    }

    private void checkBindingResult(BindingResult bindingResult, String view) {
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            String message = errors.stream()
                    .map(e -> String.format("%s %s", e.getObjectName(), e.getDefaultMessage()))
                    .collect(Collectors.joining(","));
            throw new EmployerDetailsException(String.format("Failed to create %s from invalid input - %s", view, message));
        }
    }

}
