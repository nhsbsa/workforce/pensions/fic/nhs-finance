package uk.nhs.nhsbsa.finance.employerdetails;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * Custom exception thrown when any exceptional circumstance is encountered and
 * specific error handling of that event is required.
 *
 * Created by duncan on 20/03/2017.
 */
public class EmployerDetailsException extends RuntimeException {

    public EmployerDetailsException() {
        super();
    }

    public EmployerDetailsException(String message) {
        super(message);
    }
}
