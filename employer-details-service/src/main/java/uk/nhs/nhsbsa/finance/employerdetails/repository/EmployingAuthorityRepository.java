package uk.nhs.nhsbsa.finance.employerdetails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;

public interface EmployingAuthorityRepository extends JpaRepository<EmployingAuthorityEntity, Long> {


    EmployingAuthorityEntity findByEaCode(String eaCode);

    @Query(value = "SELECT employer_type FROM employing_authority WHERE ea_code = :eaCode", nativeQuery = true)
    String findEmployerTypeByEaCode(@Param("eaCode") String eaCode);
    
}
