/**
 * Spring config classes.
 *
 * Created by duncan on 14/03/2017.
 */
package uk.nhs.nhsbsa.finance.employerdetails.config;